package zest.nio;

import com.google.common.base.Throwables;
import org.jetbrains.annotations.NotNull;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.io.TempDir;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import zest.data.FQN;
import zest.data.ZSet;
import zest.util.URL;

import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.Flow;
import java.util.concurrent.atomic.AtomicReference;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class OSFTest {

    private final static Logger LOG = LoggerFactory.getLogger(OSFTest.class);

    @TempDir
    public static java.io.File tempDir;

    @BeforeAll
    public static void setup() {
        assertTrue(tempDir.exists());
        assertTrue(tempDir.listFiles().length == 0);
    }

    @Test
    @Order(1)
    public void create() throws IOException, InterruptedException {
        final Service service = Service.at(URL.at(tempDir.toURI()));
        try (final Service.Out out = service.out()) {
            final Map<FQN, Collection<ZSet>> map = IOFixture.TEST_MMAP.asMap();
            for (final Map.Entry<FQN, Collection<ZSet>> entry : map.entrySet()) {
                // Directory
                {
                    final Subscriber subscriber = new Subscriber() {
                        @Override
                        public void onNext(@NotNull ZSet result) {
                            LOG.info("ACK {} {}.", op_id, result);
                            next();
                        }
                    };
                    out.create(ZSet.fqn(entry.getKey()).set(), subscriber);
                    subscriber.await();
                    if (subscriber.isFailed()) {
                        fail(subscriber.getError());
                    }
                }
                // Files
                {
                    final Subscriber subscriber = new Subscriber(entry.getValue().size()) {
                        @Override
                        public void onNext(@NotNull ZSet result) {
                            LOG.info("ACK OP {} {}.", op_id, result);
                            next();
                        }
                    };
                    for (ZSet operand : entry.getValue()) {
                        out.create(operand, subscriber);
                    }
                    subscriber.await();
                    if (subscriber.isFailed()) {
                        fail(subscriber.getError());
                    }
                }
            }
        }
    }


    @Test
    @Order(2)
    public void read() throws IOException, InterruptedException {
        final Service service = Service.at(URL.at(tempDir.toURI()));
        try (final Service.Out out = service.out()) {
            final AtomicReference<Throwable> catchable = new AtomicReference<>();
            for (final Map.Entry<FQN, Collection<ZSet>> entry : IOFixture.TEST_MMAP.asMap().entrySet()) {
                ZSet operand = ZSet.fqn(entry.getKey()).set();
                final CountDownLatch latch = new CountDownLatch(1);
                out.read(operand, new Flow.Subscriber<ZSet>() {

                    private Flow.Subscription subscription;

                    @Override
                    public void onSubscribe(Flow.Subscription subscription) {
                        this.subscription = subscription;
                        this.subscription.request(1);
                        LOG.debug("BEG {}", subscription);
                    }

                    @Override
                    public void onNext(ZSet zset) {
                        LOG.info("ACK {}", zset);
                        final File file = zset.fqn.toPath(zset.id).toFile();
                        if (file.isFile() && !file.isHidden()) {
                            final CountDownLatch latch = new CountDownLatch(1);
                            out.read(zset, new Flow.Subscriber<ZSet>() {

                                private Flow.Subscription subscription;

                                @Override
                                public void onSubscribe(Flow.Subscription subscription) {
                                    this.subscription = subscription;
                                    this.subscription.request(1);
                                    LOG.debug("BEG {}", subscription);
                                }

                                @Override
                                public void onNext(ZSet zset) {
                                    LOG.info("ACK {}", zset);
                                    this.subscription.request(1);
                                }

                                @Override
                                public void onError(Throwable throwable) {
                                    catchable.set(throwable);
                                    latch.countDown();

                                }

                                @Override
                                public void onComplete() {
                                    LOG.debug("END");
                                    latch.countDown();
                                }
                            });
                            try {
                                latch.await();
                            } catch (InterruptedException e) {
                                catchable.set(e);
                            }
                        }
                        subscription.request(1);
                    }

                    @Override
                    public void onError(Throwable throwable) {
                        catchable.set(throwable);
                        latch.countDown();
                    }

                    @Override
                    public void onComplete() {
                        LOG.debug("END");
                        latch.countDown();
                    }
                });
                latch.await();
                final Throwable throwable = catchable.get();
                if (throwable != null) {
                    fail(throwable);
                }
            }
        }
    }

    @Test
    @Order(3)
    public void update() throws IOException, InterruptedException {
        final Service service = Service.at(URL.at(tempDir.toURI()));
        try (final Service.Out out = service.out()) {
            final AtomicReference<Throwable> catchable = new AtomicReference<>();
            for (final Map.Entry<FQN, Collection<ZSet>> entry : IOFixture.TEST_MMAP.asMap().entrySet()) {
                {
                    ZSet operand = ZSet.fqn(entry.getKey()).set();
                    final CountDownLatch latch = new CountDownLatch(1);
                    out.update(operand, new Flow.Subscriber<>() {

                        private Flow.Subscription subscription;

                        @Override
                        public void onSubscribe(Flow.Subscription subscription) {
                            this.subscription = subscription;
                            this.subscription.request(1);
                            LOG.debug("BEG {}.", subscription);
                        }

                        @Override
                        public void onNext(ZSet zSet) {
                            LOG.error("ERR {}.", zSet);
                            subscription.request(1);
                            catchable.set(new UnsupportedOperationException("Container update should raise UnsupportedOperationException"));
                        }

                        @Override
                        public void onError(Throwable throwable) {
                            LOG.error("ERR is expected for {}.", Throwables.getStackTraceAsString(throwable));
                            latch.countDown();
                        }

                        @Override
                        public void onComplete() {
                            LOG.debug("END.");
                            latch.countDown();
                        }
                    });
                    latch.await();
                    final Throwable throwable = catchable.get();
                    if (throwable != null) {
                        fail(throwable);
                    }
                }
                for (ZSet operand : entry.getValue()) {
                    final CountDownLatch latch = new CountDownLatch(1);

                    out.update(operand, new Flow.Subscriber<>() {

                        private Flow.Subscription subscription;

                        @Override
                        public void onSubscribe(Flow.Subscription subscription) {
                            this.subscription = subscription;
                            this.subscription.request(1);
                            LOG.debug("BEG {}", subscription);
                        }

                        @Override
                        public void onNext(ZSet zSet) {
                            LOG.info("ACK {}", zSet);
                            subscription.request(1);
                        }

                        @Override
                        public void onError(Throwable throwable) {
                            catchable.set(throwable);
                            latch.countDown();
                        }

                        @Override
                        public void onComplete() {
                            LOG.debug("END");
                            latch.countDown();
                        }
                    });
                    latch.await();
                    final Throwable throwable = catchable.get();
                    if (throwable != null) {
                        fail(throwable);
                    }
                }
            }
        }
    }

    @Test
    @Order(4)
    public void delete() throws IOException, InterruptedException {
        final Service service = Service.at(URL.at(tempDir.toURI()));
        try (final Service.Out out = service.out()) {
            final AtomicReference<Throwable> catchable = new AtomicReference<>();
            for (final Map.Entry<FQN, Collection<ZSet>> entry : IOFixture.TEST_MMAP.asMap().entrySet()) {
                for (ZSet operand : entry.getValue()) {
                    final CountDownLatch latch = new CountDownLatch(1);
                    out.delete(operand, new Flow.Subscriber<ZSet>() {

                        private Flow.Subscription subscription;

                        @Override
                        public void onSubscribe(Flow.Subscription subscription) {
                            this.subscription = subscription;
                            this.subscription.request(1);
                            LOG.debug("BEG {}", subscription);
                        }

                        @Override
                        public void onNext(ZSet zSet) {
                            LOG.info("ACK {}", zSet);
                            subscription.request(1);
                        }

                        @Override
                        public void onError(Throwable throwable) {
                            catchable.set(throwable);
                            latch.countDown();
                        }

                        @Override
                        public void onComplete() {
                            LOG.debug("END.");
                            latch.countDown();
                        }
                    });
                    latch.await();
                    final Throwable throwable = catchable.get();
                    if (throwable != null) {
                        fail(throwable);
                    }
                }
            }

            TreeSet<FQN> fqnSet = new TreeSet(new Comparator<FQN>() {
                @Override
                public int compare(FQN fqn1, FQN fqn2) {
                    return fqn1.compareTo(fqn2) * -1;
                }
            });
            fqnSet.addAll(IOFixture.TEST_MMAP.asMap().keySet());
            for(final FQN fqn : fqnSet) {
                final CountDownLatch latch = new CountDownLatch(1);
                final ZSet operand = ZSet.fqn(fqn).set();
                out.delete(operand, new Flow.Subscriber<ZSet>() {

                    private Flow.Subscription subscription;

                    @Override
                    public void onSubscribe(Flow.Subscription subscription) {
                        this.subscription = subscription;
                        this.subscription.request(1);
                        LOG.debug("BEG {}.", subscription);
                    }

                    @Override
                    public void onNext(ZSet zSet) {
                        LOG.info("ACK {}.", zSet);
                        this.subscription.request(1);
                    }

                    @Override
                    public void onError(Throwable throwable) {
                        catchable.set(throwable);
                        latch.countDown();
                    }

                    @Override
                    public void onComplete() {
                        LOG.debug("END.");
                        latch.countDown();
                    }
                });
                LOG.info("{}", fqn);
            }
        }
    }



}