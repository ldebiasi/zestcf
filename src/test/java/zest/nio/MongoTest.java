package zest.nio;

import com.google.common.collect.Sets;
import org.jetbrains.annotations.NotNull;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import zest.data.Codec;
import zest.data.Element;
import zest.data.FQN;
import zest.data.ZSet;
import zest.util.URL;

import java.io.IOException;
import java.util.Collection;
import java.util.Map;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.fail;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class MongoTest {

    private final static Logger LOG = LoggerFactory.getLogger(MongoTest.class);

    private final URL url = URL.at("mongo://localhost:27017");

    @Test
    @Order(1)
    public void create() throws IOException, InterruptedException {
        final Service service = Service.at(url);
        try (final Service.Out out = service.out()) {
            final Map<FQN, Collection<ZSet>> map = IOFixture.TEST_MMAP.asMap();
            final Set<FQN> fqnSet = Sets.newTreeSet();
            fqnSet.addAll(map.keySet());
            {
                final Subscriber subscriber = new Subscriber(fqnSet.size()) {
                    @Override
                    public void onNext(@NotNull ZSet result) {
                        LOG.info("ACK {} {}.", op_id, result);
                        next();
                    }
                };
                for (final FQN fqn : fqnSet) {
                    out.create(ZSet.fqn(fqn).set(), subscriber);
                }
                subscriber.await();
                if (subscriber.isFailed()) {
                    fail(subscriber.getError());
                }
            }
            for (final FQN fqn : fqnSet) {
                final Collection<ZSet> collection = map.get(fqn);
                final Subscriber subscriber = new Subscriber(collection.size()) {
                    @Override
                    public void onNext(@NotNull ZSet result) {
                        LOG.info("ACK {} {}.", op_id, result);
                        next();
                    }
                };
                for (final ZSet operand : collection) {
                    out.create(operand, subscriber);
                }
                subscriber.await();
                if(subscriber.isFailed()) {
                    fail(subscriber.getError());
                }
            }
        }
    }

    @Test
    @Order(4)
    public void delete() throws IOException, InterruptedException {
        final Service service = Service.at(url);
        try (final Service.Out out = service.out()) {
            final Map<FQN, Collection<ZSet>> map = IOFixture.TEST_MMAP.asMap();
            final Set<FQN> fqnSet = Sets.newTreeSet();
            fqnSet.addAll(map.keySet());
            {
                final Subscriber subscriber = new Subscriber() {
                    @Override
                    public void onNext(@NotNull ZSet result) {
                        LOG.info("ACK {} {}.", op_id, result);
                        next();
                    }
                };
                for (final FQN fqn : fqnSet) {
                    out.delete(ZSet.fqn(fqn).set(), subscriber);
                }
                subscriber.await();
                if (subscriber.isFailed()) {
                    fail(subscriber.getError());
                }
            }
            for (final FQN fqn : fqnSet) {
                final Collection<ZSet> collection = map.get(fqn);
                final Subscriber subscriber = new Subscriber(collection.size()) {
                    @Override
                    public void onNext(@NotNull ZSet result) {
                        LOG.info("ACK {} {}.", op_id, result);
                        next();
                    }
                };
                for (final ZSet operand : collection) {

                }
            }
        }
    }

}
