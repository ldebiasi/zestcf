package zest.nio;

import com.google.common.collect.ImmutableMultimap;
import com.google.common.collect.Multimap;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import zest.data.Codec;
import zest.data.Element;
import zest.data.FQN;
import zest.data.ZSet;
import zest.nio.meta.OSF;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.Iterator;

public class IOFixture {

    protected static final Logger LOG = LoggerFactory.getLogger(IOFixture.class);

    public final static java.io.File JAVA_HOME = new java.io.File(System.getProperty("java.home"));

    public final static String TAG_BFA = "basic_file_attributes";

    public volatile static Multimap<FQN, ZSet> TEST_MMAP;

    static {
        LOG.debug("Setup...");
        try {
            TEST_MMAP = ls(JAVA_HOME);
        } catch (IOException e) {
            TEST_MMAP = ImmutableMultimap.of();
            e.printStackTrace();
        }
        LOG.debug("Setup test set of {} items.", TEST_MMAP.size());
    }

    protected static Multimap<FQN, ZSet> ls(
            @NotNull final java.io.File root)
            throws IOException {
        final ImmutableMultimap.Builder<FQN, ZSet> mmap = ImmutableMultimap.builder();
        for (final Iterator<Path> iter = Files.walk(root.toPath()).iterator(); iter.hasNext(); ) {
            final Path path = iter.next();
            final java.io.File file = path.toFile();
            if (file.isFile() && !file.isHidden()) {
                final BasicFileAttributes bfa = Files.readAttributes(path, BasicFileAttributes.class);
                final zest.nio.meta.OSF data = new OSF(bfa);
                final Element element = Codec.DEFAULT.encode(data).set();
                final ZSet zset = ZSet.fqn(FQN.of(path.getParent())).put(TAG_BFA, element).set(file.getName());
                mmap.put(zset.fqn, zset);
            }
        }
        return mmap.build();
    }

}
