package zest.io;

import org.jetbrains.annotations.NotNull;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.io.TempDir;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import zest.data.Op;
import zest.data.ZSet;
import zest.util.URL;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.*;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class FileTest {

    private final static Logger LOG = LoggerFactory.getLogger(FileTest.class);

    @TempDir
    public static java.io.File tempDir;

    @BeforeAll
    public static void setup() {
        assertTrue(tempDir.exists());
        assertTrue(tempDir.listFiles().length == 0);
    }

    @Test
    @Order(1)
    public void create() throws IOException {
        final Op op = Op.C;
        final Service service = Service.at(URL.at(tempDir.toURI()));
        try (final Service.Out out = service.out(new Subscriber() {
            @Override
            public void onBeg(@NotNull String op_id) {
                LOG.debug("BEG {}.", op_id);
            }

            @Override
            public void onEnd(@NotNull String op_id) {
                LOG.debug("END {}.", op_id);
            }

            @Override
            public void onAck(@NotNull String op_id, @NotNull ZSet ZSet, @NotNull int status) {
                LOG.info("ACK {} {} {}.", op_id, ZSet, status);
            }

            @Override
            public void onNak(@NotNull String op_id, @NotNull Throwable throwable, @NotNull int status) {
                LOG.error("NAK {} {} {}.", op_id, throwable, status);
                fail(throwable);
            }
        })) {
            for (final ZSet zset : IOFixture.TEST_MMAP.values()) {
                final String op_id = op.id(service, zset.fqn, zset.id).toURI();
                out.write(op_id, zset, op);
            }
        }
    }


    @Test
    @Order(2)
    public void read() throws IOException {
        final Op op = Op.R;
        final Service service = Service.at(URL.at(tempDir.toURI()));
        try (final Service.Out out = service.out(new Subscriber() {
            @Override
            public void onBeg(@NotNull String op_id) {
                LOG.debug("BEG {}.", op_id);
            }

            @Override
            public void onEnd(@NotNull String op_id) {
                LOG.debug("END {}.", op_id);
            }


            @Override
            public void onAck(@NotNull String op_id, @NotNull ZSet zset, @NotNull int status) {
                LOG.info("ACK {} {} {}.", op_id, zset, status);
            }

            @Override
            public void onNak(@NotNull String op_id, @NotNull Throwable throwable, @NotNull int status) {
                LOG.error("NAK {} {} {}.", op_id, throwable, status);
                fail(throwable);
            }
        })) {
            for (final ZSet zset : IOFixture.TEST_MMAP.values()) {
                final String op_id = op.id(service, zset.fqn, zset.id).toURI();
                out.write(op_id, zset, op);
            }
        }
    }

    @Test
    @Order(3)
    public void update() throws IOException {
        final Op op = Op.U;
        final Service service = Service.at(URL.at(tempDir.toURI()));
        try (final Service.Out out = service.out(new Subscriber() {
            @Override
            public void onBeg(@NotNull String op_id) {
                LOG.debug("BEG {}.", op_id);
            }

            @Override
            public void onEnd(@NotNull String op_id) {
                LOG.debug("END {}.", op_id);
            }


            @Override
            public void onAck(@NotNull String op_id, @NotNull ZSet ZSet, @NotNull int status) {
                LOG.info("ACK {} {} {}.", op_id, ZSet, status);
            }

            @Override
            public void onNak(@NotNull String op_id, @NotNull Throwable throwable, @NotNull int status) {
                LOG.error("NAK {} {} {}.", op_id, throwable, status);
                fail(throwable);
            }
        })) {
            for (final ZSet zset : IOFixture.TEST_MMAP.values()) {
                final String op_id = op.id(service, zset.fqn, zset.id).toURI();
                out.write(op_id, zset, op);
            }
        }
    }

    @Test
    @Order(4)
    public void delete() throws IOException {
        final Op op = Op.D;
        final Service service = Service.at(URL.at(tempDir.toURI()));
        try (final Service.Out out = service.out(new Subscriber() {
            @Override
            public void onBeg(@NotNull String op_id) {
                LOG.debug("BEG {}.", op_id);
            }

            @Override
            public void onEnd(@NotNull String op_id) {
                LOG.debug("END {}.", op_id);
            }


            @Override
            public void onAck(@NotNull String op_id, @NotNull ZSet ZSet, @NotNull int status) {
                LOG.info("ACK {} {} {}.", op_id, ZSet, status);
            }

            @Override
            public void onNak(@NotNull String op_id, @NotNull Throwable throwable, @NotNull int status) {
                LOG.error("NAK {} {} {}.", op_id, throwable, status);
                fail(throwable);
            }
        })) {
            for (final ZSet zset : IOFixture.TEST_MMAP.values()) {
                final String op_id = op.id(service, zset.fqn, zset.id).toURI();
                out.write(op_id, zset, op);
            }
        }
    }

}