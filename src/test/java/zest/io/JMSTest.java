package zest.io;

import org.jetbrains.annotations.NotNull;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import zest.data.FQN;
import zest.data.Op;
import zest.data.ZSet;
import zest.util.HttpStatus;
import zest.util.URL;

import java.io.IOException;
import java.util.Collection;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.atomic.AtomicReference;

import static org.junit.jupiter.api.Assertions.*;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class JMSTest {

    private final static Logger LOG = LoggerFactory.getLogger(JMSTest.class);

    private final static String DESTINATION = "queue";

    public static final int LIMIT = Integer.MAX_VALUE;

    public static final URL _URL = URL.at("jms://ldebiasi:ayanam1!@localhost:61616");


    @Test
    @Order(1)
    public void out_create_message() throws IOException, InterruptedException {
        final Service service = Service.at(_URL);
        final Collection<ZSet> tests = IOFixture.TEST_MMAP.values();
        final CountDownLatch latch = new CountDownLatch(Math.min(LIMIT, tests.size()));
        final AtomicReference<Throwable> catchable = new AtomicReference<>();
        try (final Service.Out out = service.out(new Subscriber() {
            @Override
            public void onBeg(@NotNull String op_id) {
                LOG.debug("BEG {}.", op_id);
            }

            @Override
            public void onEnd(@NotNull String op_id) {
                LOG.debug("END {}.", op_id);
                latch.countDown();
            }

            @Override
            public void onAck(@NotNull String op_id, @NotNull ZSet zset, @NotNull int status) {
                LOG.info("ACK {} {} {}.", op_id, zset, status);
            }

            @Override
            public void onNak(@NotNull String op_id, @NotNull Throwable throwable, @NotNull int status) {
                LOG.error("NAK {} {} {}.", op_id, throwable, status);
                catchable.set(throwable);
            }
        })) {
            final Op op = Op.C;
            int i = 0;
            for (final ZSet zset : tests) {
                final ZSet ask = ZSet
                        .fqn(FQN.of(DESTINATION, zset.fqn))
                        .put(zset)
                        .set(zset.id);
                final String op_id = op.id(service, ask.fqn, ask.id).toURI();
                out.write(op_id, ask, op);
                if (++i >= LIMIT) {
                    break;
                }
            }
            latch.await();
            final Throwable throwable = catchable.get();
            if (throwable != null) {
                fail(throwable);
            }
        }
    }

    @Test
    @Order(2)
    public void out_read_message() throws IOException, InterruptedException {
        final Service service = Service.at(_URL);
        final Collection<FQN> tests = IOFixture.TEST_MMAP.keySet();
        final CountDownLatch latch = new CountDownLatch(Math.min(LIMIT, tests.size()));
        final AtomicReference<Throwable> catchable = new AtomicReference<>();
        try (final Service.Out out = service.out(new Subscriber() {
            @Override
            public void onBeg(@NotNull String op_id) {
                LOG.debug("BEG {}.", op_id);
            }

            @Override
            public void onEnd(@NotNull String op_id) {
                LOG.debug("END {}.", op_id);
                latch.countDown();
            }

            @Override
            public void onAck(@NotNull String op_id, @NotNull ZSet zset, @NotNull int status) {
                LOG.info("ACK {} {} {}.", op_id, zset, status);
            }

            @Override
            public void onNak(@NotNull String op_id, @NotNull Throwable throwable, @NotNull int status) {
                LOG.error("NAK {} {} {}.", op_id, throwable, status);
                catchable.set(throwable);
            }
        })) {
            final Op op = Op.R;
            int i = 0;
            for (final FQN fqn : tests) {
                final ZSet ask = ZSet
                        .fqn(FQN.of(DESTINATION, fqn))
                        .set();
                final String op_id = op.id(service, ask.fqn, ask.id).toURI();
                out.write(op_id, ask, op);
                if (++i >= LIMIT) {
                    break;
                }
            }
            latch.await();
            final Throwable throwable = catchable.get();
            if (throwable != null) {
                fail(throwable);
            }
        }
    }


    @Test
    @Order(4)
    public void out_update_message() throws IOException, InterruptedException {
        final Service service = Service.at(_URL);
        final Collection<ZSet> tests = IOFixture.TEST_MMAP.values();
        final CountDownLatch latch = new CountDownLatch(Math.min(LIMIT, tests.size()));
        final AtomicReference<Throwable> catchable = new AtomicReference<>();
        try (final Service.Out out = service.out(new Subscriber() {
            @Override
            public void onBeg(@NotNull String op_id) {
                LOG.debug("BEG {}.", op_id);
            }

            @Override
            public void onEnd(@NotNull String op_id) {
                LOG.debug("END {}.", op_id);
                latch.countDown();
            }

            @Override
            public void onAck(@NotNull String op_id, @NotNull ZSet zset, @NotNull int status) {
                LOG.info("ACK {} {} {}.", op_id, zset, status);
                catchable.set(new UnsupportedOperationException());
            }

            @Override
            public void onNak(@NotNull String op_id, @NotNull Throwable throwable, @NotNull int status) {
                assertTrue(throwable instanceof UnsupportedOperationException);
                assertEquals(HttpStatus.SC_METHOD_NOT_ALLOWED, status);
            }
        })) {
            final Op op = Op.U;
            int i = 0;
            for (final ZSet zset : tests) {
                final ZSet ask = ZSet
                        .fqn(FQN.of(DESTINATION, zset.fqn))
                        .put(zset)
                        .set(zset.id);
                final String op_id = op.id(service, ask.fqn, ask.id).toURI();
                out.write(op_id, ask, op);
                if (++i >= LIMIT) {
                    break;
                }
            }
            latch.await();
            final Throwable throwable = catchable.get();
            if (throwable != null) {
                fail(throwable);
            }
        }
    }

//    @Test
//    @Order(5)
//    public void in_read() throws IOException, InterruptedException {
//        final URL url = new URL(
//                _URL.scheme,
//                _URL.user,
//                _URL.password,
//                _URL.host,
//                _URL.port,
//                _URL.path,
//                ImmutableMap.of(
//                        ConsumerConfig.GROUP_ID_CONFIG, UUID.randomUUID().toString(),
//                        ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest",
//                        ConsumerConfig.ENABLE_AUTO_COMMIT_CONFIG, "false"
//                ),
//                _URL.fragment);
//        final Collection<ZSet> values = IOFixture.TEST_MMAP.values();
//        final CountDownLatch latch = new CountDownLatch(Math.min(LIMIT, values.size()));
//        final Service service = Service.at(url);
//        try (Service.In in = service.in(new Subscriber() {
//            @Override
//            public void onBeg(@NotNull String op_id) {
//                LOG.debug("BEG {}.", op_id);
//            }
//
//            @Override
//            public void onEnd(@NotNull String op_id) {
//                LOG.debug("END {}.", op_id);
//            }
//
//            @Override
//            public void onAck(@NotNull String op_id, @NotNull ZSet zset, @NotNull int status) {
//                LOG.info("ACK {} {} {}.", op_id, zset, status);
//                latch.countDown();
//            }
//
//            @Override
//            public void onNak(@NotNull String op_id, @NotNull Throwable throwable, @NotNull int status) {
//                LOG.error("NAK {} {} {}.", op_id, throwable, status);
//                fail(throwable);
//            }
//        })) {
//            final ZSet ignore = ZSet.fqn(TOPIC).set();
//            in.read(ignore);
//            latch.await();
//        }
//    }

    @Test
    @Order(5)
    public void out_delete_message() throws IOException, InterruptedException {
        final Service service = Service.at(_URL);
        final Collection<ZSet> tests = IOFixture.TEST_MMAP.values();
        final CountDownLatch latch = new CountDownLatch(Math.min(LIMIT, tests.size()));
        final AtomicReference<Throwable> catchable = new AtomicReference<>();
        try (final Service.Out out = service.out(new Subscriber() {
            @Override
            public void onBeg(@NotNull String op_id) {
                LOG.debug("BEG {}.", op_id);
            }

            @Override
            public void onEnd(@NotNull String op_id) {
                LOG.debug("END {}.", op_id);
                latch.countDown();
            }

            @Override
            public void onAck(@NotNull String op_id, @NotNull ZSet zset, @NotNull int status) {
                LOG.info("ACK {} {} {}.", op_id, zset, status);
                catchable.set(new UnsupportedOperationException());
            }

            @Override
            public void onNak(@NotNull String op_id, @NotNull Throwable throwable, @NotNull int status) {
                LOG.debug("NAK {} {} {}.", op_id, throwable, status);
                assertTrue(throwable instanceof UnsupportedOperationException);
                assertEquals(HttpStatus.SC_METHOD_NOT_ALLOWED, status);
            }
        })) {
            final Op op = Op.D;
            int i = 0;
            for (final ZSet zset : tests) {
                final ZSet ask = ZSet
                        .fqn(FQN.of(DESTINATION, zset.fqn))
                        .put(zset)
                        .set(zset.id);
                final String op_id = op.id(service, ask.fqn, ask.id).toURI();
                out.write(op_id, ask, op);
                if (++i >= LIMIT) {
                    break;
                }
            }
            latch.await();
            final Throwable throwable = catchable.get();
            if (throwable != null) {
                fail(throwable);
            }
        }
    }

    @Test
    @Order(6)
    public void in_read_message() throws IOException, InterruptedException {
        final Service service = Service.at(_URL);
        final Collection<ZSet> tests = IOFixture.TEST_MMAP.values();
        final CountDownLatch writeLatch = new CountDownLatch(Math.min(LIMIT, tests.size()));
        final AtomicReference<Throwable> catchable = new AtomicReference<>();
        // All messages in a single target
        final FQN targetFQN = FQN.of(DESTINATION, "in_read_message");
        try (final Service.Out out = service.out(new Subscriber() {
            @Override
            public void onBeg(@NotNull String op_id) {
                LOG.debug("BEG {}.", op_id);
            }

            @Override
            public void onEnd(@NotNull String op_id) {
                LOG.debug("END {}.", op_id);
                writeLatch.countDown();
            }

            @Override
            public void onAck(@NotNull String op_id, @NotNull ZSet zset, @NotNull int status) {
                LOG.trace("ACK {} {} {}.", op_id, zset, status);
            }

            @Override
            public void onNak(@NotNull String op_id, @NotNull Throwable throwable, @NotNull int status) {
                LOG.error("NAK {} {} {}.", op_id, throwable, status);
                catchable.set(throwable);
            }
        })) {
            final Op op = Op.C;
            int i = 0;
            for (final ZSet zset : tests) {
                final ZSet ask = ZSet
                        .fqn(targetFQN)
                        .put(zset)
                        .set(zset.id);
                final String op_id = op.id(service, ask.fqn, ask.id).toURI();
                out.write(op_id, ask, op);
                if (++i >= LIMIT) {
                    break;
                }
            }
            writeLatch.await();
            final Throwable throwable = catchable.get();
            if (throwable != null) {
                fail(throwable);
            }
        }
        final CountDownLatch readLatch = new CountDownLatch(1);
        try (final Service.In in = service.in(new Subscriber() {
            @Override
            public void onBeg(@NotNull String op_id) {
                LOG.debug("BEG {}.", op_id);
            }

            @Override
            public void onEnd(@NotNull String op_id) {
                LOG.debug("END {}.", op_id);
                readLatch.countDown();
            }

            @Override
            public void onAck(@NotNull String op_id, @NotNull ZSet zset, @NotNull int status) {
                LOG.info("ACK {} {} {}.", op_id, zset, status);
            }

            @Override
            public void onNak(@NotNull String op_id, @NotNull Throwable throwable, @NotNull int status) {
                LOG.error("NAK {} {} {}.", op_id, throwable, status);
                catchable.set(throwable);
            }
        } )) {
            final ZSet ask = ZSet
                    .fqn(targetFQN)
                    .set();
            in.read(ask);
            readLatch.await();
            final Throwable throwable = catchable.get();
            if (throwable != null) {
                throw throwable;
            }
        } catch (Throwable throwable) {
            fail(throwable);
        }
    }

}
