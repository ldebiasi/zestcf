package zest.io;

import com.google.common.base.MoreObjects;
import com.google.common.collect.*;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import zest.data.Codec;
import zest.data.Element;
import zest.data.FQN;
import zest.data.ZSet;
import zest.util.Text;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

public class IOFixture {

    protected static final Logger LOG = LoggerFactory.getLogger(IOFixture.class);

    public final static java.io.File JAVA_HOME = new java.io.File(System.getProperty("java.home"));

    public volatile static Multimap<FQN, ZSet> TEST_MMAP;

    static {
        LOG.debug("Setup...");
        try {
            TEST_MMAP = ls(JAVA_HOME);
        } catch (IOException e) {
            TEST_MMAP = ImmutableMultimap.of();
            e.printStackTrace();
        }
        LOG.debug("Setup test set of {} items.", TEST_MMAP.size());
    }

    /**
     * Return a multimap of file descriptors ordered by FQN.
     * FQN and paths are
     * @param root
     * @return
     * @throws IOException
     */
    protected static Multimap<FQN, ZSet> ls(
            @NotNull final java.io.File root)
            throws IOException {
        final ImmutableMultimap.Builder<FQN, ZSet> mmap = ImmutableMultimap.builder();
        for (final Iterator<Path> iter = Files.walk(root.toPath()).iterator(); iter.hasNext(); ) {
            final Path path = iter.next();
            final java.io.File file = path.toFile();
            if (file.isFile() && !file.isHidden()) {
                final String id = path.getFileName().toString();
                final BasicFileAttributes bfa = Files.readAttributes(path, BasicFileAttributes.class);
                final Element element = Codec.DEFAULT.encode(bfa).set();
                final ZSet zset = ZSet.fqn(FQN.of(path)).put("bfa", element).set(id);
                mmap.put(zset.fqn, zset);
            }
        }
        return mmap.build();
    }

    protected static Map<FQN, FQN> simplify(Set<FQN> fqnSet) {
        final Map<FQN, FQN> map = Maps.newTreeMap();
        int i = 0;
        for (FQN fqn : fqnSet) {
            map.put(fqn, FQN.of(String.format("test%d", i++)));
        }
        return map;
    }


}
