package zest.io;

import com.google.common.collect.ImmutableMap;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.jetbrains.annotations.NotNull;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import zest.data.Codec;
import zest.data.FQN;
import zest.data.Op;
import zest.data.ZSet;
import zest.util.URL;

import java.io.IOException;
import java.util.Collection;
import java.util.UUID;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.atomic.AtomicReference;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class KafkaTest {

    public static final FQN TOPIC = FQN.of("junit");
    public static final int LIMIT = Integer.MAX_VALUE;
    public static final URL _URL = URL.at("kafka://localhost:9092");
    private final static Logger LOG = LoggerFactory.getLogger(KafkaTest.class);

    @Test
    @Order(0)
    public void out_create_topic() throws IOException, InterruptedException {
        final Service service = Service.at(_URL);
        final CountDownLatch latch = new CountDownLatch(1);
        final AtomicReference<Throwable> catchable = new AtomicReference<>();
        try (final Service.Out out = service.out(new Subscriber() {
            @Override
            public void onBeg(@NotNull String op_id) {
                LOG.debug("BEG {}.", op_id);
            }

            @Override
            public void onEnd(@NotNull String op_id) {
                LOG.debug("END {}.", op_id);
                latch.countDown();
            }

            @Override
            public void onAck(@NotNull String op_id, @NotNull ZSet zset, @NotNull int status) {
                LOG.info("ACK {} {} {}.", op_id, zset, status);
            }

            @Override
            public void onNak(@NotNull String op_id, @NotNull Throwable throwable, @NotNull int status) {
                LOG.error("NAK {} {} {}.", op_id, throwable, status);
                catchable.set(throwable);
            }
        })) {
            final Op op = Op.C;
            final ZSet ask = ZSet.fqn(TOPIC).set();
            final String op_id = op.id(service, ask.fqn, ask.id).toURI();
            out.write(op_id, ask, op);
            latch.await();
            final Throwable throwable = catchable.get();
            if (throwable != null) {
                if (throwable.getClass().equals(org.apache.kafka.common.errors.TopicExistsException.class)) {
                    assertTrue(throwable.getClass().equals(org.apache.kafka.common.errors.TopicExistsException.class));
                } else {
                    fail(throwable);
                }
            }
        }
    }

    @Test
    @Order(1)
    public void out_create_record() throws IOException, InterruptedException {
        final Service service = Service.at(_URL);
        final Collection<ZSet> tests = IOFixture.TEST_MMAP.values();
        final CountDownLatch latch = new CountDownLatch(Math.min(LIMIT, tests.size()));
        final AtomicReference<Throwable> catchable = new AtomicReference<>();
        try (final Service.Out out = service.out(new Subscriber() {
            @Override
            public void onBeg(@NotNull String op_id) {
                LOG.debug("BEG {}.", op_id);
            }

            @Override
            public void onEnd(@NotNull String op_id) {
                LOG.debug("END {}.", op_id);
                latch.countDown();
            }

            @Override
            public void onAck(@NotNull String op_id, @NotNull ZSet zset, @NotNull int status) {
                LOG.info("ACK {} {} {}.", op_id, zset, status);
            }

            @Override
            public void onNak(@NotNull String op_id, @NotNull Throwable throwable, @NotNull int status) {
                LOG.error("NAK {} {} {}.", op_id, throwable, status);
                catchable.set(throwable);
            }
        })) {
            final Op op = Op.C;
            int i = 0;
            for (final ZSet zset : tests) {
                final ZSet ask = ZSet.fqn(TOPIC).
                        put(zset)
                        .set(zset.id);
                final String op_id = op.id(service, ask.fqn, ask.id).toURI();
                out.write(op_id, ask, op);
                if (++i >= LIMIT) {
                    break;
                }
            }
            latch.await();
            final Throwable throwable = catchable.get();
            if (throwable != null) {
                fail(throwable);
            }
        }
    }

    @Test
    @Order(2)
    public void out_read() throws IOException, InterruptedException {
        final CountDownLatch latch = new CountDownLatch(1);
        final Service service = Service.at(_URL);
        try (final Service.Out out = service.out(new Subscriber() {
            @Override
            public void onBeg(@NotNull String op_id) {
                LOG.debug("BEG {}.", op_id);
            }

            @Override
            public void onEnd(@NotNull String op_id) {
                LOG.debug("END {}.", op_id);
                latch.countDown();
            }

            @Override
            public void onAck(@NotNull String op_id, @NotNull ZSet zset, @NotNull int status) {
                LOG.info("ACK {} {} {}.", op_id, zset, status);
            }

            @Override
            public void onNak(@NotNull String op_id, @NotNull Throwable throwable, @NotNull int status) {
                LOG.error("NAK {} {} {}.", op_id, throwable, status);
                fail(throwable);
            }
        })) {
            final Op op = Op.R;
            final ZSet ask = ZSet.fqn(TOPIC)
                    .put(ConsumerConfig.GROUP_ID_CONFIG, Codec.DEFAULT.encode(UUID.randomUUID().toString()).set())
                    .put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, Codec.DEFAULT.encode("earliest").set())
                    .put(ConsumerConfig.ENABLE_AUTO_COMMIT_CONFIG, Codec.DEFAULT.encode("false").set())
                    .set();
            final String op_id = op.id(service, ask.fqn, ask.id).toURI();
            out.write(op_id, ask, op);
            latch.await();
        }
    }

    @Test
    @Order(3)
    public void out_delete_records() throws IOException, InterruptedException {
        final Service service = Service.at(_URL);
        final CountDownLatch latch = new CountDownLatch(1);
        try (final Service.Out out = service.out(new Subscriber() {
            @Override
            public void onBeg(@NotNull String op_id) {
                LOG.debug("BEG {}.", op_id);
            }

            @Override
            public void onEnd(@NotNull String op_id) {
                LOG.debug("END {}.", op_id);
                latch.countDown();
            }

            @Override
            public void onAck(@NotNull String op_id, @NotNull ZSet zset, @NotNull int status) {
                LOG.info("ACK {} {} {}.", op_id, zset, status);
            }

            @Override
            public void onNak(@NotNull String op_id, @NotNull Throwable throwable, @NotNull int status) {
                LOG.error("NAK {} {} {}.", op_id, throwable, status);
                fail(throwable);
            }
        })) {
            final Op op = Op.D;
            final ZSet ask = ZSet.fqn(TOPIC)
                    .put(Kafka.Tag.kafka_partition.name(), Codec.DEFAULT.encode(Long.MAX_VALUE).set())
                    .set("ignore");
            final String op_id = op.id(service, ask.fqn, ask.id).toURI();
            out.write(op_id, ask, op);
            latch.await();
        }
    }

    @Test
    @Order(4)
    public void out_update() throws IOException, InterruptedException {
        final Service service = Service.at(_URL);
        final Collection<ZSet> values = IOFixture.TEST_MMAP.values();
        final CountDownLatch latch = new CountDownLatch(Math.min(LIMIT, values.size()));
        try (final Service.Out out = service.out(
                new Subscriber() {
                    @Override
                    public void onBeg(@NotNull String op_id) {
                        LOG.debug("BEG {}.", op_id);
                    }

                    @Override
                    public void onEnd(@NotNull String op_id) {
                        LOG.debug("END {}.", op_id);
                    }

                    @Override
                    public void onAck(@NotNull String op_id, @NotNull ZSet zset, @NotNull int status) {
                        LOG.info("ACK {} {} {}.", op_id, zset, status);
                        latch.countDown();
                    }

                    @Override
                    public void onNak(@NotNull String op_id, @NotNull Throwable throwable, @NotNull int status) {
                        LOG.error("NAK {} {} {}.", op_id, throwable, status);
                        fail(throwable);
                    }
                }
        )) {
            final Op op = Op.U;
            int i = 0;
            for (final ZSet zset : values) {
                final ZSet ask = ZSet.fqn(TOPIC)
                        .put(zset)
                        .set(zset.id);
                final String op_id = op.id(service, ask.fqn, ask.id).toURI();
                out.write(op_id, ask, op);
                if (++i >= LIMIT) {
                    break;
                }
            }
            latch.await();
        }
    }

    @Test
    @Order(5)
    public void in_read() throws IOException, InterruptedException {
        final URL url = new URL(
                _URL.scheme,
                _URL.user,
                _URL.password,
                _URL.host,
                _URL.port,
                _URL.path,
                ImmutableMap.of(
                        ConsumerConfig.GROUP_ID_CONFIG, UUID.randomUUID().toString(),
                        ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest",
                        ConsumerConfig.ENABLE_AUTO_COMMIT_CONFIG, "false"
                ),
                _URL.fragment);
        final Collection<ZSet> values = IOFixture.TEST_MMAP.values();
        final CountDownLatch latch = new CountDownLatch(Math.min(LIMIT, values.size()));
        final Service service = Service.at(url);
        try (Service.In in = service.in(new Subscriber() {
            @Override
            public void onBeg(@NotNull String op_id) {
                LOG.debug("BEG {}.", op_id);
            }

            @Override
            public void onEnd(@NotNull String op_id) {
                LOG.debug("END {}.", op_id);
            }

            @Override
            public void onAck(@NotNull String op_id, @NotNull ZSet zset, @NotNull int status) {
                LOG.info("ACK {} {} {}.", op_id, zset, status);
                latch.countDown();
            }

            @Override
            public void onNak(@NotNull String op_id, @NotNull Throwable throwable, @NotNull int status) {
                LOG.error("NAK {} {} {}.", op_id, throwable, status);
                fail(throwable);
            }
        })) {
            final ZSet ignore = ZSet.fqn(TOPIC).set();
            in.read(ignore);
            latch.await();
        }
    }

    @Test
    @Order(6)
    public void out_delete() throws IOException, InterruptedException {
        final CountDownLatch latch = new CountDownLatch(1);
        final Service service = Service.at(_URL);
        try (Service.Out out = service.out(new Subscriber() {
            @Override
            public void onBeg(@NotNull String op_id) {
                LOG.debug("BEG {}.", op_id);
            }

            @Override
            public void onEnd(@NotNull String op_id) {
                LOG.debug("END {}.", op_id);
                latch.countDown();
            }

            @Override
            public void onAck(@NotNull String op_id, @NotNull ZSet zset, @NotNull int status) {
                LOG.info("ACK {} {} {}.", op_id, zset, status);
            }

            @Override
            public void onNak(@NotNull String op_id, @NotNull Throwable throwable, @NotNull int status) {
                LOG.error("NAK {} {} {}.", op_id, throwable, status);
                fail(throwable);
            }
        })) {
            final Op op = Op.D;
            final ZSet ask = ZSet.fqn(TOPIC).set();
            final String op_id = op.id(service, ask.fqn, ask.id).toURI();
            out.write(op_id, ask, op);
            latch.await();
        }
    }

}
