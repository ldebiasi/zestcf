package zest.io;

import org.jetbrains.annotations.NotNull;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import zest.data.FQN;
import zest.data.Op;
import zest.data.ZSet;
import zest.util.URL;

import java.io.IOException;
import java.util.Collection;
import java.util.Map;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;

import static org.junit.jupiter.api.Assertions.fail;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class SQSTest {

    private final static Logger LOG = LoggerFactory.getLogger(SQSTest.class);

    public static final String URI = "sqs:http://localhost:4576?aws_access_key_id=AKIAY4DSN4OZ4LYXXABW;aws_secret_access_key=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";

    @Test
    @Order(0)
    public void out_create() throws IOException {
        final Service service = Service.at(URL.at(URI));
        try (final Service.Out out = service.out(new Subscriber() {
            @Override
            public void onBeg(@NotNull String op_id) {
                LOG.debug("BEG {}.", op_id);
            }

            @Override
            public void onEnd(@NotNull String op_id) {
                LOG.debug("END {}.", op_id);
            }

            @Override
            public void onAck(@NotNull String op_id, @NotNull ZSet zset, @NotNull int status) {
                LOG.info("ACK {} {} {}.", op_id, zset, status);
            }

            @Override
            public void onNak(@NotNull String op_id, @NotNull Throwable throwable, @NotNull int status) {
                LOG.error("NAK {} {} {}.", op_id, throwable, status);
                fail(throwable);
            }
        })) {
            final Op op = Op.C;
            final Map<FQN, FQN> simpleFQNMap = IOFixture.simplify(IOFixture.TEST_MMAP.keySet());
            for (final ZSet zset : IOFixture.TEST_MMAP.values()) {
                final ZSet ask = ZSet.fqn(simpleFQNMap.get(zset.fqn)).put(zset).set(zset.id);
                final String op_id = op.id(service, ask.fqn, ask.id).toURI();
                out.write(op_id, ask, op);
            }
        }
    }

    @Test
    @Order(1)
    public void out_read() throws IOException {
        final Service service = Service.at(URL.at(URI));
        try (final Service.Out out = service.out(new Subscriber() {
            @Override
            public void onBeg(@NotNull String op_id) {
                LOG.debug("BEG {}.", op_id);
            }

            @Override
            public void onEnd(@NotNull String op_id) {
                LOG.debug("END {}.", op_id);
            }

            @Override
            public void onAck(@NotNull String op_id, @NotNull ZSet zset, @NotNull int status) {
                LOG.info("ACK {} {} {}.", op_id, zset, status);
            }

            @Override
            public void onNak(@NotNull String op_id, @NotNull Throwable throwable, @NotNull int status) {
                LOG.error("NAK {} {} {}.", op_id, throwable, status);
                fail(throwable);
            }
        })) {
            final Op op = Op.R;
            final Map<FQN, FQN> simpleFQNMap = IOFixture.simplify(IOFixture.TEST_MMAP.keySet());
            for (final ZSet zset : IOFixture.TEST_MMAP.values()) {
                final ZSet ask = ZSet.fqn(simpleFQNMap.get(zset.fqn)).put(zset).set(zset.id);
                final String op_id = op.id(service, ask.fqn, ask.id).toURI();
                out.write(op_id, ask, op);
            }
        }
    }

    @Test
    @Order(2)
    public void out_delete_messages() throws IOException {
        final Service service = Service.at(URL.at(URI));
        try (final Service.Out out = service.out(new Subscriber() {
            @Override
            public void onBeg(@NotNull String op_id) {
                LOG.debug("BEG {}.", op_id);
            }

            @Override
            public void onEnd(@NotNull String op_id) {
                LOG.debug("END {}.", op_id);
            }


            @Override
            public void onAck(@NotNull String op_id, @NotNull ZSet zset, @NotNull int status) {
                LOG.info("ACK {} {} {}.", op_id, zset, status);
                if (zset.isMember(SQS.TAG_RECEIPT_HANDLE)) {
                    service.out(this).write(Op.D.id(service, zset.fqn, zset.id).toURI(), zset, Op.D);
                }
            }

            @Override
            public void onNak(@NotNull String op_id, @NotNull Throwable throwable, @NotNull int status) {
                LOG.error("NAK {} {} {}.", op_id, throwable, status);
                fail(throwable);
            }
        })) {
            final Op op = Op.D;
            final Map<FQN, FQN> simpleFQNMap = IOFixture.simplify(IOFixture.TEST_MMAP.keySet());
            for (final ZSet zset : IOFixture.TEST_MMAP.values()) {
                if (zset.isIdentifiable()) {
                    final ZSet ask = ZSet.fqn(simpleFQNMap.get(zset.fqn)).put(zset).set(zset.id);
                    final String op_id = op.id(service, ask.fqn, ask.id).toURI();
                    out.write(op_id, ask, op);
                }
            }
        }
    }

    @Test
    @Order(3)
    public void out_update() throws IOException {
        final Service service = Service.at(URL.at(URI));
        try (final Service.Out out = service.out(new Subscriber() {
            @Override
            public void onBeg(@NotNull String op_id) {
                LOG.debug("BEG {}.", op_id);
            }

            @Override
            public void onEnd(@NotNull String op_id) {
                LOG.debug("END {}.", op_id);
            }


            @Override
            public void onAck(@NotNull String op_id, @NotNull ZSet zset, @NotNull int status) {
                LOG.info("ACK {} {} {}.", op_id, zset, status);
            }

            @Override
            public void onNak(@NotNull String op_id, @NotNull Throwable throwable, @NotNull int status) {
                LOG.error("NAK {} {} {}.", op_id, throwable, status);
                fail(throwable);
            }
        })) {
            final Op op = Op.U;
            final Map<FQN, FQN> simpleFQNMap = IOFixture.simplify(IOFixture.TEST_MMAP.keySet());
            for (final ZSet zset : IOFixture.TEST_MMAP.values()) {
                if (zset.isIdentifiable()) {
                    final ZSet ask = ZSet.fqn(simpleFQNMap.get(zset.fqn)).put(zset).set(zset.id);
                    final String op_id = op.id(service, ask.fqn, ask.id).toURI();
                    out.write(op_id, ask, op);
                }
            }
        }
    }

    @Test
    @Order(4)
    public void in_read() throws IOException {
        final Service service = Service.at(URL.at(URI));
        final AtomicReference<CountDownLatch> latch = new AtomicReference<>(new CountDownLatch(0));
        try (Service.In in = service.in(new Subscriber() {
            @Override
            public void onBeg(@NotNull String op_id) {
                LOG.debug("BEG {}.", op_id);
            }

            @Override
            public void onEnd(@NotNull String op_id) {
                LOG.debug("END {}.", op_id);
            }


            @Override
            public void onAck(@NotNull String op_id, @NotNull ZSet zset, @NotNull int status) {
                LOG.info("ACK {} {} {}.", op_id, zset, status);
                latch.get().countDown();
            }

            @Override
            public void onNak(@NotNull String op_id, @NotNull Throwable throwable, @NotNull int status) {
                LOG.error("NAK {} {} {}.", op_id, throwable, status);
                fail(throwable);
            }
        })) {
            final Map<FQN, FQN> simpleFQNMap = IOFixture.simplify(IOFixture.TEST_MMAP.keySet());
            for (final Map.Entry<FQN, Collection<ZSet>> entry : IOFixture.TEST_MMAP.asMap().entrySet()) {
                final ZSet ask = ZSet.fqn(simpleFQNMap.get(entry.getKey())).set();
                final int size = entry.getValue().size();
                latch.set(new CountDownLatch(size));
                in.read(ask);
                try {
                    if (!latch.get().await(size * in.getPeriod(), TimeUnit.SECONDS)) {
                        fail("Timeout!");
                    }
                } catch (InterruptedException e) {
                    fail("Timeout!");
                }
            }
        }
    }

    @Test
    @Order(5)
    public void delete_queues() throws IOException {
        final Service service = Service.at(URL.at(URI));
        try (final Service.Out out = service.out(new Subscriber() {
            @Override
            public void onBeg(@NotNull String op_id) {
                LOG.debug("BEG {}.", op_id);
            }

            @Override
            public void onEnd(@NotNull String op_id) {
                LOG.debug("END {}.", op_id);
            }

            @Override
            public void onAck(@NotNull String op_id, @NotNull ZSet zset, @NotNull int status) {
                LOG.info("ACK {} {} {}.", op_id, zset, status);
                if (zset.isMember(SQS.TAG_RECEIPT_HANDLE)) {
                    service.out(this).write(Op.D.id(service, zset.fqn, zset.id).toURI(), zset, Op.D);
                }
            }

            @Override
            public void onNak(@NotNull String op_id, @NotNull Throwable throwable, @NotNull int status) {
                LOG.error("NAK {} {} {}.", op_id, throwable, status);
                fail(throwable);
            }
        })) {
            final Op op = Op.D;
            final Map<FQN, FQN> simpleFQNMap = IOFixture.simplify(IOFixture.TEST_MMAP.keySet());
            for (final FQN fqn : IOFixture.TEST_MMAP.keySet()) {
                final ZSet ask = ZSet.fqn(simpleFQNMap.get(fqn)).set();
                final String op_id = op.id(service, fqn, ask.id).toURI();
                out.write(op_id, ask, op);
            }
        }
    }

}
