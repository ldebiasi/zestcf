package zest.io;

import org.jetbrains.annotations.NotNull;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import zest.data.FQN;
import zest.data.Op;
import zest.data.ZSet;
import zest.util.URL;

import java.io.IOException;
import java.util.concurrent.CountDownLatch;

import static org.junit.jupiter.api.Assertions.fail;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class KinesisTest {

    private final static Logger LOG = LoggerFactory.getLogger(KinesisTest.class);

    //public static final String URI = "kinesis:http//localhost:4568?aws_access_key_id=AKIAY4DSN4OZ4LYXXABW;aws_secret_access_key=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";

    public static final String URI = "kinesis:https://kinesis.eu-west-1.amazonaws.com?aws_region=eu-west-1;aws_access_key_id=AKIAIAKB6TYIQJDD6O7Q;aws_secret_access_key=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";

    public static final FQN STREAM = FQN.of("junit");

    public static final int LIMIT = 10;

    @Test
    @Order(0)
    public void out_create() throws IOException {
        final Service service = Service.at(URL.at(URI));
        try (final Service.Out out = service.out(new Subscriber() {
            @Override
            public void onBeg(@NotNull String op_id) {
                LOG.debug("BEG {}.", op_id);
            }

            @Override
            public void onEnd(@NotNull String op_id) {
                LOG.debug("END {}.", op_id);
            }

            @Override
            public void onAck(@NotNull String op_id, @NotNull ZSet zset, @NotNull int status) {
                LOG.info("ACK {} {} {}.", op_id, zset, status);
            }

            @Override
            public void onNak(@NotNull String op_id, @NotNull Throwable throwable, @NotNull int status) {
                LOG.error("NAK {} {} {}.", op_id, throwable, status);
                fail(throwable);
            }
        })) {
            final Op op = Op.C;
            int i = 0;
            for (final ZSet zset : IOFixture.TEST_MMAP.values()) {
                final ZSet ask = ZSet.fqn(STREAM).put(zset).set(zset.id);
                final String op_id = op.id(service, ask.fqn, ask.id).toURI();
                out.write(op_id, ask, op);
                if (++ i >= LIMIT) {
                    break;
                }
            }
        }
    }

    @Test
    @Order(1)
    public void out_read() throws IOException, InterruptedException {
        final CountDownLatch latch = new CountDownLatch(1);
        final Service service = Service.at(URL.at(URI));
        try (final Service.Out out = service.out(new Subscriber() {
            @Override
            public void onBeg(@NotNull String op_id) {
                LOG.debug("BEG {}.", op_id);
            }

            @Override
            public void onEnd(@NotNull String op_id) {
                LOG.debug("END {}.", op_id);
                latch.countDown();
            }

            @Override
            public void onAck(@NotNull String op_id, @NotNull ZSet zset, @NotNull int status) {
                LOG.info("ACK {} {} {}.", op_id, zset, status);
            }

            @Override
            public void onNak(@NotNull String op_id, @NotNull Throwable throwable, @NotNull int status) {
                LOG.error("NAK {} {} {}.", op_id, throwable, status);
                fail(throwable);
            }
        })) {
            final Op op = Op.R;
            final ZSet ask = ZSet.fqn(STREAM).set();
            final String op_id = op.id(service, ask.fqn, ask.id).toURI();
            out.write(op_id, ask, op);
            latch.await();
        }
    }

    @Test
    @Order(2)
    public void out_update() throws IOException {
        final Service service = Service.at(URL.at(URI));
        try (final Service.Out out = service.out(new Subscriber() {
            @Override
            public void onBeg(@NotNull String op_id) {
                LOG.debug("BEG {}.", op_id);
            }

            @Override
            public void onEnd(@NotNull String op_id) {
                LOG.debug("END {}.", op_id);
            }

            @Override
            public void onAck(@NotNull String op_id, @NotNull ZSet zset, @NotNull int status) {
                LOG.info("ACK {} {} {}.", op_id, zset, status);
            }

            @Override
            public void onNak(@NotNull String op_id, @NotNull Throwable throwable, @NotNull int status) {
                LOG.error("NAK {} {} {}.", op_id, throwable, status);
                fail(throwable);
            }
        })) {
            final Op op = Op.U;
            int i = 0;
            for (final ZSet zset : IOFixture.TEST_MMAP.values()) {
                if (zset.isIdentifiable()) {
                    final ZSet ask = ZSet.fqn(STREAM).put(zset).set(zset.id);
                    final String op_id = op.id(service, ask.fqn, ask.id).toURI();
                    out.write(op_id, ask, op);
                    if (++i >= LIMIT) {
                        break;
                    }
                }
            }
        }
    }

    @Test
    @Order(3)
    public void in_read() throws IOException, InterruptedException {
        final CountDownLatch latch = new CountDownLatch(1);
        final Service service = Service.at(URL.at(URI));
        try (Service.In in = service.in(new Subscriber() {
            @Override
            public void onBeg(@NotNull String op_id) {
                LOG.debug("BEG {}.", op_id);
            }

            @Override
            public void onEnd(@NotNull String op_id) {
                LOG.debug("END {}.", op_id);
            }

            @Override
            public void onAck(@NotNull String op_id, @NotNull ZSet zset, @NotNull int status) {
                LOG.info("ACK {} {} {}.", op_id, zset, status);
                latch.countDown();
            }

            @Override
            public void onNak(@NotNull String op_id, @NotNull Throwable throwable, @NotNull int status) {
                LOG.error("NAK {} {} {}.", op_id, throwable, status);
                fail(throwable);
            }
        })) {
            final Op op = Op.R;
            final ZSet ask = ZSet.fqn(STREAM).set();
            final String op_id = op.id(service, ask.fqn, ask.id).toURI();
            in.read(ask);
            latch.await();
        }
    }


    @Test
    @Order(4)
    public void out_delete() throws IOException, InterruptedException {
        final Service service = Service.at(URL.at(URI));
        final CountDownLatch latch = new CountDownLatch(1);
        try (final Service.Out out = service.out(new Subscriber() {
            @Override
            public void onBeg(@NotNull String op_id) {
                LOG.debug("BEG {}.", op_id);
            }

            @Override
            public void onEnd(@NotNull String op_id) {
                LOG.debug("END {}.", op_id);
                latch.countDown();
            }

            @Override
            public void onAck(@NotNull String op_id, @NotNull ZSet zset, @NotNull int status) {
                LOG.info("ACK {} {} {}.", op_id, zset, status);
                if (zset.isMember(SQS.TAG_RECEIPT_HANDLE)) {
                    service.out(this).write(Op.D.id(service, zset.fqn, zset.id).toURI(), zset, Op.D);
                }
            }

            @Override
            public void onNak(@NotNull String op_id, @NotNull Throwable throwable, @NotNull int status) {
                LOG.error("NAK {} {} {}.", op_id, throwable, status);
                fail(throwable);
            }
        })) {
            final Op op = Op.D;
            final ZSet ask = ZSet.fqn(STREAM).set();
            final String op_id = op.id(service, ask.fqn, ask.id).toURI();
            out.write(op_id, ask, op);
            latch.await();
        }
    }

}
