package zest.util;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class URITest {

    @Test
    void schemeOf() {
        assertEquals("foo", URI.schemeOf("foo://example.com:8042/over/there?name=ferret#nose"));
        assertEquals("boo", URI.schemeOf("boo:foo://example.com:8042/over/there?name=ferret#nose"));
        assertEquals("foo", URI.schemeOf("foo:/over/there"));
        assertEquals("foo", URI.schemeOf("foo:down/here"));
        assertEquals("urn", URI.schemeOf("urn:example:animal:ferret:nose"));
    }

    @Test
    void schemeAndSubsOf() {
        assertEquals("foo", URI.schemeAndSubsOf("foo://example.com:8042/over/there?name=ferret#nose"));
        assertEquals("boo:foo", URI.schemeAndSubsOf("boo:foo://example.com:8042/over/there?name=ferret#nose"));
        assertEquals("boo:foo", URI.schemeAndSubsOf("boo:foo:/over/there"));
        assertEquals("boo:foo", URI.schemeAndSubsOf("boo:foo:down/here"));
        assertEquals("urn:example:animal:ferret", URI.schemeAndSubsOf("urn:example:animal:ferret:nose"));
    }

    @Test
    void innerSchemeSpecificPartOf() {
        assertEquals("example.com:8042/over/there?name=ferret#nose", URI.innerSSPOf("foo://example.com:8042/over/there?name=ferret#nose"));
        assertEquals("example.com:8042/over/there?name=ferret#nose", URI.innerSSPOf("boo:foo://example.com:8042/over/there?name=ferret#nose"));
        assertEquals("/over/there", URI.innerSSPOf("boo:foo:/over/there"));
        assertEquals("down/here", URI.innerSSPOf("boo:foo:down/here"));
        assertEquals("/over/there", URI.innerSSPOf("foo:/over/there"));
        assertEquals("down/here", URI.innerSSPOf("foo:down/here"));
        assertEquals("nose", URI.innerSSPOf("urn:example:animal:ferret:nose"));
    }

    @Test
    void outerSSPOf() {
        assertEquals("example.com:8042/over/there?name=ferret#nose", URI.outerSSPOf("foo://example.com:8042/over/there?name=ferret#nose"));
        assertEquals("foo://example.com:8042/over/there?name=ferret#nose", URI.outerSSPOf("boo:foo://example.com:8042/over/there?name=ferret#nose"));
        assertEquals("foo:/over/there", URI.outerSSPOf("boo:foo:/over/there"));
        assertEquals("foo:down/here", URI.outerSSPOf("boo:foo:down/here"));
        assertEquals("/over/there", URI.outerSSPOf("foo:/over/there"));
        assertEquals("down/here", URI.outerSSPOf("foo:down/here"));
        assertEquals("example:animal:ferret:nose", URI.outerSSPOf("urn:example:animal:ferret:nose"));
    }

    @Test
    void getScheme() {
        assertEquals("foo", URL.at("foo://example.com:8042/over/there?name=ferret#nose").getScheme());
        assertEquals("boo", URL.at("boo:foo://example.com:8042/over/there?name=ferret#nose").getScheme());
        assertEquals("foo", URL.at("foo:/over/there").getScheme());
        assertEquals("foo", URL.at("foo:down/here").getScheme());
        //assertEquals("urn", URN.at("urn:example:animal:ferret:nose"));
    }

    @Test
    void getSchemeAndSubs() {
        assertEquals("foo", URL.at("foo://example.com:8042/over/there?name=ferret#nose").getSchemeAndSubs());
        assertEquals("boo:foo", URL.at("boo:foo://example.com:8042/over/there?name=ferret#nose").getSchemeAndSubs());
        assertEquals("boo:foo", URL.at("boo:foo:/over/there").getSchemeAndSubs());
        assertEquals("boo:foo", URL.at("boo:foo:down/here").getSchemeAndSubs());
        //assertEquals("urn:example:animal:ferret", URL.at("urn:example:animal:ferret:nose").getSchemeAndSubs());
    }

    @Test
    void getInnerSSP() {
        assertEquals("example.com:8042/over/there?name=ferret#nose", URL.at("foo://example.com:8042/over/there?name=ferret#nose").getInnerSSP());
        assertEquals("example.com:8042/over/there?name=ferret#nose", URL.at("boo:foo://example.com:8042/over/there?name=ferret#nose").getInnerSSP());
        assertEquals("/over/there", URL.at("boo:foo:/over/there").getInnerSSP());
        assertEquals("down/here", URL.at("boo:foo:down/here").getInnerSSP());
        assertEquals("/over/there", URL.at("foo:/over/there").getInnerSSP());
        assertEquals("down/here", URL.at("foo:down/here").getInnerSSP());
        //assertEquals("nose", URN.at("urn:example:animal:ferret:nose").getInnerSSP());
    }

    @Test
    void getOuterSSP() {
        assertEquals("example.com:8042/over/there?name=ferret#nose", URL.at("foo://example.com:8042/over/there?name=ferret#nose").getOuterSSP());
        assertEquals("foo://example.com:8042/over/there?name=ferret#nose", URL.at("boo:foo://example.com:8042/over/there?name=ferret#nose").getOuterSSP());
        assertEquals("foo:/over/there", URL.at("boo:foo:/over/there").getOuterSSP());
        assertEquals("foo:down/here", URL.at("boo:foo:down/here").getOuterSSP());
        assertEquals("/over/there", URL.at("foo:/over/there").getOuterSSP());
        assertEquals("down/here", URL.at("foo:down/here").getOuterSSP());
        //assertEquals("example:animal:ferret:nose", URN.at("urn:example:animal:ferret:nose").getOuterSSP());
    }

    @Test
    void toURI() {
        final URL url = URL.at("foo://user:password@example.com:8042/over/there?name=ferret#nose");
        assertTrue(url.toURI().indexOf("password") == -1);
    }

}