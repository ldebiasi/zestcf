package zest.util;

import com.google.common.collect.ImmutableMap;
import org.junit.jupiter.api.Test;

import java.nio.file.Paths;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class URLTest {

    @Test
    void at() {
        final URL url = URL.at("boo:foo://user:password@example.com:8042/over/there?name=ferret#nose");
        assertEquals("boo:foo", url.scheme);
        assertEquals("user", url.user);
        assertEquals("password", url.password);
        assertEquals("example.com", url.host);
        assertEquals(8042, url.port);
        assertEquals("/over/there", url.path);
        assertEquals("name=ferret", url.query);
        assertEquals("nose", url.fragment);
    }

    @Test
    void authorityOf() {
        assertEquals("user:password@example.com:8042", URL.authorityOf("boo:foo://user:password@example.com:8042/over/there?name=ferret#nose"));
        assertEquals("user@example.com:8042", URL.authorityOf("foo://user@example.com:8042/over/there?name=ferret"));
        assertEquals("example.com:8042", URL.authorityOf("foo://example.com:8042/over/there#nose"));
        assertEquals("example.com", URL.authorityOf("foo://example.com/over/there"));
        assertEquals(null, URL.authorityOf("foo:/over/there"));
    }

    @Test
    void fragmentOf() {
        assertEquals("nose", URL.fragmentOf("boo:foo://user:password@example.com:8042/over/there?name=ferret#nose"));
        assertEquals(null, URL.fragmentOf("foo://user@example.com:8042/over/there?name=ferret"));
        assertEquals("nose", URL.fragmentOf("foo://example.com:8042/over/there#nose"));
        assertEquals(null, URL.fragmentOf("foo://example.com/over/there"));
        assertEquals("nose", URL.fragmentOf("foo:/over/there#nose"));
    }

    @Test
    void hostOf() {
        assertEquals("example.com", URL.hostOf("boo:foo://user:password@example.com:8042/over/there?name=ferret#nose"));
        assertEquals("example.com", URL.hostOf("foo://user@example.com:8042/over/there?name=ferret"));
        assertEquals("example.com", URL.hostOf("foo://example.com:8042/over/there#nose"));
        assertEquals("example.com", URL.hostOf("foo://example.com/over/there"));
        assertEquals(null, URL.hostOf("foo:/over/there"));
    }

    @Test
    void passwordOf() {
        assertEquals("password", URL.passwordOf("boo:foo://user:password@example.com:8042/over/there?name=ferret#nose"));
        assertEquals("password", URL.passwordOf("foo://user:password@example.com:8042/over/there?name=ferret"));
        assertEquals(null, URL.passwordOf("boo:foo://user@example.com:8042/over/there?name=ferret#nose"));
        assertEquals(null, URL.passwordOf("foo://user@example.com:8042/over/there?name=ferret"));
        assertEquals(null, URL.passwordOf("foo:/over/there"));
    }

    @Test
    void pathOf() {
        assertEquals("/over/there", URL.pathOf("boo:foo://user:password@example.com:8042/over/there?name=ferret#nose"));
        assertEquals("/over/there", URL.pathOf("foo://user@example.com:8042/over/there?name=ferret"));
        assertEquals("/over/there", URL.pathOf("foo://example.com:8042/over/there#nose"));
        assertEquals("/over/there", URL.pathOf("foo://example.com/over/there"));
        assertEquals("down/here", URL.pathOf("foo:down/here"));
    }

    @Test
    void portOf() {
        assertEquals(8042, URL.portOf("boo:foo://user:password@example.com:8042/over/there?name=ferret#nose"));
        assertEquals(8042, URL.portOf("foo://user@example.com:8042/over/there?name=ferret"));
        assertEquals(8042, URL.portOf("foo://example.com:8042/over/there#nose"));
        assertEquals(URL.NO_PORT, URL.portOf("foo://example.com/over/there"));
    }

    @Test
    void queryOf() {
        assertEquals("name=ferret", URL.queryOf("boo:foo://user:password@example.com:8042/over/there?name=ferret#nose"));
        assertEquals("name=ferret", URL.queryOf("foo://user@example.com:8042/over/there?name=ferret"));
        assertEquals("name=ferret", URL.queryOf("boo:foo:/over/there?name=ferret"));
        assertEquals("name=ferret", URL.queryOf("foo:/over/there?name=ferret"));
        assertEquals("name=ferret", URL.queryOf("boo:foo:under/here?name=ferret"));
        assertEquals("name=ferret", URL.queryOf("foo:under/here?name=ferret"));
        assertEquals(null, URL.queryOf("boo:foo:under/here"));
        assertEquals(null, URL.queryOf("foo:under/here"));
    }

    @Test
    void userinfoOf() {
        assertEquals("user:password", URL.userinfoOf("boo:foo://user:password@example.com:8042/over/there?name=ferret#nose"));
        assertEquals("user:password", URL.userinfoOf("foo://user:password@example.com:8042/over/there?name=ferret"));
        assertEquals("user", URL.userinfoOf("boo:foo://user@example.com:8042/over/there?name=ferret#nose"));
        assertEquals("user", URL.userinfoOf("foo://user@example.com:8042/over/there?name=ferret"));
        assertEquals(null, URL.userinfoOf("foo:/over/there"));
    }

    @Test
    void userOf() {
        assertEquals("user", URL.userOf("boo:foo://user:password@example.com:8042/over/there?name=ferret#nose"));
        assertEquals("user", URL.userOf("foo://user:password@example.com:8042/over/there?name=ferret"));
        assertEquals("user", URL.userOf("boo:foo://user@example.com:8042/over/there?name=ferret#nose"));
        assertEquals("user", URL.userOf("foo://user@example.com:8042/over/there?name=ferret"));
        assertEquals(null, URL.userOf("foo:/over/there"));
    }

    @Test
    void testCompareTo() {
        URL expected = URL.at("boo:foo://user@example.com:8042/over/there?name=ferret#nose");
        URL actual = URL.at("boo:foo://user:password@example.com:8042/over/there?name=ferret#nose");
        assertTrue(expected.compareTo(actual) == 0);
    }



    @Test
    void testEquals() {
        URL expected = URL.at("boo:foo://user@example.com:8042/over/there?name=ferret#nose");
        URL actual = URL.at("boo:foo://user:password@example.com:8042/over/there?name=ferret#nose");
        assertTrue(expected.equals(actual));
    }

    @Test
    void getQueryAsMap() {
        assertEquals(ImmutableMap.of("name", "ferret", "answer", "42", "catch", "22"), URL.at("boo:foo://user@example.com:8042/over/there?name=ferret&answer=42&catch=22#nose").getQueryAsMap());
        assertEquals(ImmutableMap.of("name", "ferret", "answer", "42", "catch", "22"), URL.at("foo:too/low/for/zero?name=ferret;answer=42;catch=22#nose").getQueryAsMap());
    }

    @Test
    void toPath() {
        assertEquals(Paths.get("over", "there"), URL.at("boo:foo://user@example.com:8042/over/there?name=ferret#nose").toPath());
        assertEquals(Paths.get("too", "low", "for", "zero"), URL.at("boo:foo:too/low/for/zero").toPath());
    }

    @Test
    void toURI() {
        URL expected = URL.at("boo:foo://user@example.com:8042/over/there?name=ferret#nose");
        URL actual = URL.at("boo:foo://user:password@example.com:8042/over/there?name=ferret#nose");
        assertEquals(expected.toURI(), actual.toURI());
        assertTrue(expected.toURI().indexOf("password") < 0);
        assertTrue(actual.toURI().indexOf("password") < 0);
    }

    @Test
    void testToString() {
        URL expected = URL.at("boo:foo://user@example.com:8042/over/there?name=ferret#nose");
        URL actual = URL.at("boo:foo://user:password@example.com:8042/over/there?name=ferret#nose");
        assertEquals(expected.toString(), actual.toString());
        assertTrue(expected.toString().indexOf("password") < 0);
        assertTrue(actual.toString().indexOf("password") < 0);
    }
}