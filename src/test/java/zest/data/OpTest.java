package zest.data;

import org.junit.jupiter.api.Test;
import zest.io.Service;
import zest.util.URL;

import java.nio.file.Paths;
import java.time.Instant;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class OpTest {

    private final Service service = Service.at(URL.at("file:."));

    private final FQN fqn = FQN.of("catalog", "schema", "name");

    @Test
    void idWithId() {
        final Op op = Op.C;
        final String id = "id";
        final URL op_id = op.id(service, fqn, id);
        assertEquals(op_id.path, Paths.get(fqn.toPath().toString(), id).toString());
        assertEquals(op_id.getQueryAsMap().get(Op.OP), op.name());
        assertTrue(Instant.parse(op_id.fragment).isBefore(Instant.now()));
    }

    @Test
    void idWithoutId() {
        final Op op = Op.R;
        final String id = null;
        final URL op_id = op.id(service, fqn, id);
        assertEquals(op_id.path, Paths.get(fqn.toPath().toString()).toString());
        assertEquals(op_id.getQueryAsMap().get(Op.OP), op.name());
        assertTrue(Instant.parse(op_id.fragment).isBefore(Instant.now()));
    }

}