package zest.io;

import com.google.common.base.Throwables;
import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.RemovalListener;
import com.google.common.util.concurrent.ExecutionError;
import com.google.common.util.concurrent.UncheckedExecutionException;
import org.apache.activemq.artemis.jms.client.ActiveMQJMSConnectionFactory;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import zest.data.*;
import zest.util.HttpStatus;
import zest.util.Text;
import zest.util.URL;

import javax.jms.*;
import java.lang.IllegalStateException;
import java.util.Deque;
import java.util.Random;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Serve the <a href="https://www.oracle.com/technical-resources/articles/java/intro-java-message-service.html">Java Message Service</a>
 * using the <a href="https://activemq.apache.org/components/artemis/documentation/1.0.0/using-jms.html">Apache ActiveMQ Artemis</a> as client.
 */
public class JMS extends Service {

    /**
     * Create a new JMS connected to the data service at {@code url} address.
     *
     * @param url address of the data service connected to this service.
     */
    protected JMS(@NotNull URL url) {
        super(url);
    }

    protected static Destination aim(
            @NotNull final FQN fqn,
            @NotNull final Session session)
            throws IllegalArgumentException, JMSException {
        switch (Target.valueOf(fqn.getRoot())) {
            case queue:
                return session.createQueue(fqn.getName());
            case topic:
                return session.createTopic(fqn.getName());
        }
        throw new IllegalArgumentException(String.format("fqn = %s doesn't express a queue or topic", fqn));
    }

    /**
     * {@inheritDoc}
     *
     * @param subscriber to notify the operations on this new input channel.
     * @return {@inheritDoc}
     */
    @Override
    @NotNull
    public In in(
            @NotNull final Subscriber subscriber) {
        return new In(this, subscriber);
    }

    /**
     * {@inheritDoc}
     *
     * @param subscriber to notify the operations on this new output channel.
     * @return {@inheritDoc}
     */
    @Override
    @NotNull
    public Out out(
            @NotNull final Subscriber subscriber) {
        return new Out(this, subscriber);
    }

    /**
     * {@inheritDoc}
     */
    public static class In extends Service.In {

        /**
         * Logger.
         */
        protected final static transient Logger LOG = LoggerFactory.getLogger(In.class);

        /**
         * {@link #connection} is shared for the channels having the same URL.
         */
        protected final static transient Cache<String, Connection> CONNECTION_CACHE = CacheBuilder
                .newBuilder()
                .removalListener((RemovalListener<String, Connection>) notification -> {
                    // Wrap it on URL to print as URI without user and password in case of exception.
                    final URL url = URL.at(notification.getKey());
                    final Connection connection = notification.getValue();
                    try {
                        connection.stop();
                        connection.close();
                    } catch (JMSException e) {
                        LOG.error("{} exception {}!", url.toURI(), Throwables.getStackTraceAsString(e));
                    }
                })
                .build();

        /**
         * {@link #session} is shared for out channels having the same URL.
         */
        protected final static transient Cache<String, Session> SESSION_CACHE = CacheBuilder
                .newBuilder()
                .removalListener((RemovalListener<String, Session>) notification -> {
                    // Wrap it on URL to print as URI without user and password in case of exception.
                    final URL url = URL.at(notification.getKey());
                    final Session session = notification.getValue();
                    try {
                        session.close();
                    } catch (JMSException e) {
                        LOG.error("{} exception {}!", url.toURI(), Throwables.getStackTraceAsString(e));
                    }
                })
                .build();

        /**
         * Codec used to serialize and deserialize {@link ZSet} instances to and from this channel.
         */
        protected final transient Codec codec = Codec.DEFAULT;

        /**
         * Thread safe.
         */
        private final transient Connection connection;

        /**
         * Polling period.
         */
        private final long period;

        /**
         * Random generator to distribute polling window.
         */
        private final transient Random random = new Random();

        /**
         * JMS session is thread safe.
         */
        protected final transient Session session;

        /**
         * Flag stopping the reading threads.
         */
        private final transient CountDownLatch shutdownLatch = new CountDownLatch(1);

        /**
         * {@inheritDoc}
         *
         * @param service    on this channel.
         * @param subscriber of operations through this channel.
         */
        public In(
                @NotNull final Service service,
                @NotNull final Subscriber subscriber) {
            super(service, subscriber);
            try {
                connection = CONNECTION_CACHE.get(service.url.toURI(), () -> {
                    final String url = service.url.port == URL.NO_PORT
                            ? String.format("tcp://%s", service.url.host)
                            : String.format("tcp://%s:%d", service.url.host, service.url.port);
                    final ConnectionFactory connectionFactory;
                    if (Text.isFat(service.url.user)) {
                        if (Text.isNil(service.url.password)) {
                            throw new IllegalArgumentException("Password missing");
                        }
                        connectionFactory = new ActiveMQJMSConnectionFactory(url, service.url.user, service.url.password);
                    } else {
                        connectionFactory = new ActiveMQJMSConnectionFactory(url);
                    }
                    final Connection connection = connectionFactory.createConnection();
                    connection.start();
                    return connection;
                });
                session = SESSION_CACHE.get(
                        service.url.toURI(),
                        () -> connection.createSession(false, Session.AUTO_ACKNOWLEDGE));
                period = Long.parseUnsignedLong(service.url.getQueryAsMap().getOrDefault(
                        Arg.period.name(),
                        Arg.period.byDefault)
                );
                LOG.debug("{} open.", service.url.toURI());
            } catch (ExecutionException | IllegalArgumentException e) {
                throw new IllegalStateException(String.format("url = %s can't start", service.url.toURI()), e);
            }
        }

        @Override
        public synchronized void close() {
            LOG.debug("{} closed.", service.url.toURI());
        }

        /**
         * {@inheritDoc}
         *
         * @return {@inheritDoc}
         */
        @NotNull
        @Override
        public JMS getService() {
            return (JMS) service;
        }

        /**
         * {@inheritDoc}
         *
         * @return {@inheritDoc}
         */
        @Override
        public long getPeriod() {
            return period;
        }

        /**
         * {@inheritDoc}
         *
         * @param zset {@inheritDoc}
         * @return this.
         */
        @NotNull
        @Override
        public In read(
                @NotNull final ZSet zset) {
            final Op op = Op.R;
            final long timeout = 1000 * period;
            final ExecutorService executorService = Executors.newSingleThreadExecutor();
            executorService.submit(() -> {
                String op_id = op.id(service).toURI();
                onBeg(op_id);
                try (MessageConsumer messageConsumer = session.createConsumer(aim(zset.fqn, session))) {
                    while (shutdownLatch.getCount() > 0) {
                        op_id = op.id(service).toURI();
                        onBeg(op_id);
                        try {
                            Message message = messageConsumer.receive(timeout);
                            while (message != null) {
                                final Metadata metadata = new Metadata(message);
                                final String body = message.getBody(String.class);
                                final ZSet load = codec.decode(body);
                                final ZSet ack = ZSet.fqn(load.fqn)
                                        .put(load)
                                        .put(TAG_ACK, codec.encode(metadata).set())
                                        .set(load.id);
                                onAck(op_id, ack, HttpStatus.SC_OK);
                                message = messageConsumer.receive(timeout);
                            }
                        } catch (JMSException e) {
                            onNak(op_id, e, HttpStatus.SC_METHOD_FAILURE);
                        } finally {
                            onEnd(op_id);
                        }
                    }
                } catch (IllegalArgumentException | JMSException e) {
                    onNak(op_id, e, HttpStatus.SC_METHOD_FAILURE);
                } finally {
                    onEnd(op_id);
                }
            });
            return this;
        }

    } //~ In

    /**
     * {@inheritDoc}
     * <p></p>
     * JMS connections and sessions are cached using service's URL as key.
     */
    public static class Out extends Service.Out {

        /**
         * Logger.
         */
        protected final static transient Logger LOG = LoggerFactory.getLogger(Out.class);

        /**
         * {@link #connection} is shared for out channels having the same URL.
         */
        protected final static transient Cache<String, Connection> CONNECTION_CACHE = CacheBuilder
                .newBuilder()
                .removalListener((RemovalListener<String, Connection>) notification -> {
                    // Wrap it on URL to print as URI without user and password in case of exception.
                    final URL url = URL.at(notification.getKey());
                    final Connection connection = notification.getValue();
                    try {
                        connection.stop();
                        connection.close();
                    } catch (JMSException e) {
                        LOG.error("{} exception {}!", url.toURI(), Throwables.getStackTraceAsString(e));
                    }
                })
                .build();

        /**
         * {@link #session} is shared for the channels having the same URL.
         */
        protected final static transient Cache<String, Session> SESSION_CACHE = CacheBuilder
                .newBuilder()
                .removalListener((RemovalListener<String, Session>) notification -> {
                    // Wrap it on URL to print as URI without user and password in case of exception.
                    final URL url = URL.at(notification.getKey());
                    final Session session = notification.getValue();
                    try {
                        session.close();
                    } catch (JMSException e) {
                        LOG.error("{} exception {}!", url.toURI(), Throwables.getStackTraceAsString(e));
                    }
                })
                .build();

        /**
         * Codec used to wrap datum in {@link Element}.
         */
        protected final transient Codec codec = Codec.DEFAULT;

        /**
         * JMS connection is thread safe.
         */
        protected final transient Connection connection;

        /**
         * JMS session is thread safe.
         */
        protected final transient Session session;

        /**
         * Return a new output channel for this {@code service} with at least one {@code subscriber}.
         *
         * @param service    of this output channel.
         * @param subscriber of operations sent to this channel.
         * @throws IllegalStateException if the service can't start.
         */
        public Out(
                @NotNull final Service service,
                @NotNull final Subscriber subscriber)
                throws IllegalStateException {
            super(service, subscriber);
            try {
                connection = CONNECTION_CACHE.get(service.url.toURI(), () -> {
                    final String url = service.url.port == URL.NO_PORT
                            ? String.format("tcp://%s", service.url.host)
                            : String.format("tcp://%s:%d", service.url.host, service.url.port);
                    final ConnectionFactory connectionFactory;
                    if (Text.isFat(service.url.user)) {
                        if (Text.isNil(service.url.password)) {
                            throw new IllegalArgumentException("Password missing");
                        }
                        connectionFactory = new ActiveMQJMSConnectionFactory(url, service.url.user, service.url.password);
                    } else {
                        connectionFactory = new ActiveMQJMSConnectionFactory(url);
                    }
                    final Connection connection = connectionFactory.createConnection();
                    connection.start();
                    return connection;
                });
                session = SESSION_CACHE.get(
                        service.url.toURI(),
                        () -> connection.createSession(false, Session.AUTO_ACKNOWLEDGE));
                LOG.debug("{} open.", service.url.toURI());
            } catch (ExecutionException | IllegalArgumentException e) {
                throw new IllegalStateException(String.format("url = %s can't start", service.url.toURI()), e);
            }
        }


        /**
         * {@inheritDoc}
         */
        @Override
        public synchronized void close() {
            LOG.debug("{} closed.", service.url.toURI());
        }

        /**
         * JMS specifications do not allow to manage queue/topics directly.
         *
         * @param op_id Operation identifier.
         * @param stack Operands' stack.
         */
        private void create(
                @NotNull final String op_id,
                @NotNull final Deque<ZSet> stack) {
            onBeg(op_id);
            final ZSet zset = stack.removeLast();
            try (MessageProducer messageProducer = session.createProducer(aim(zset.fqn, session))) {
                final String body = zset.toJSON().toString();
                final TextMessage textMessage = session.createTextMessage(body);
                if (zset.isIdentifiable()) {
                    final String messageID = String.format("ID:%s", zset.id);
                    textMessage.setJMSMessageID(messageID);
                }
                messageProducer.send(textMessage, new CompletionListener() {
                    @Override
                    public void onCompletion(Message message) {
                        try {
                            final Metadata metadata = new Metadata(message);
                            final ZSet ack = ZSet.fqn(zset.fqn)
                                    .put(Kafka.Tag.ack.name(), codec.encode(metadata).set())
                                    .set(zset.id);
                            onAck(op_id, ack, HttpStatus.SC_CREATED);
                            onEnd(op_id);
                        } catch (JMSException e) {
                            onNak(op_id, e, HttpStatus.SC_METHOD_FAILURE);
                            onEnd(op_id);
                        }
                    }

                    @Override
                    public void onException(Message message, Exception e) {
                        onNak(op_id, e, HttpStatus.SC_METHOD_FAILURE);
                        onEnd(op_id);
                    }

                });
            } catch (IllegalArgumentException e) {
                onNak(op_id, e, HttpStatus.SC_BAD_REQUEST);
                onEnd(op_id);
            } catch (JMSException e) {
                onNak(op_id, e, HttpStatus.SC_METHOD_FAILURE);
                onEnd(op_id);
            }
        }

        /**
         * JMS specifications do not allow to delete a submitted message.
         * This method calls {@link #onNak(String, Throwable, int).}
         *
         * @param op_id operation identifier.
         * @param stack operands' stack is ignored.
         */
        private void delete(
                @NotNull final String op_id,
                @NotNull final Deque<ZSet> stack) {
            onBeg(op_id);
            final Throwable e = new UnsupportedOperationException(
                    String.format("url = %s delete not implemented", service.url));
            onNak(op_id, e, HttpStatus.SC_METHOD_NOT_ALLOWED);
            onEnd(op_id);
        }

        /**
         * {@inheritDoc}
         *
         * @return {@inheritDoc}
         */
        @NotNull
        @Override
        public JMS getService() {
            return (JMS) service;
        }

        /**
         * Read all the messages available.
         *
         * @param op_id operation id.
         * @param stack operands' stack.
         */
        private void read(
                @NotNull final String op_id,
                @NotNull final Deque<ZSet> stack) {
            onBeg(op_id);
            final ZSet zset = stack.removeLast();
            try (MessageConsumer messageConsumer = session.createConsumer(aim(zset.fqn, session))) {
                Message message = messageConsumer.receiveNoWait();
                while (message != null) {
                    final Metadata metadata = new Metadata(message);
                    final String body = message.getBody(String.class);
                    final ZSet load = codec.decode(body);
                    final ZSet ack = ZSet.fqn(load.fqn)
                            .put(load)
                            .put(TAG_ACK, codec.encode(metadata).set())
                            .set(load.id);
                    onAck(op_id, ack, HttpStatus.SC_OK);
                    message = messageConsumer.receiveNoWait();
                }
            } catch (IllegalArgumentException e) {
                onNak(op_id, e, HttpStatus.SC_BAD_REQUEST);
            } catch (JMSException e) {
                onNak(op_id, e, HttpStatus.SC_METHOD_FAILURE);
            }
            onEnd(op_id);
        }

        /**
         * JMS specifications don't allow to update messages, this method creates new messages.
         * This method calls {@link #onNak(String, Throwable, int).}
         *
         * @param op_id operation id.
         * @param stack operands' stack.
         */
        private void update(
                @NotNull final String op_id,
                @NotNull final Deque<ZSet> stack) {
            onBeg(op_id);
            final Throwable e = new UnsupportedOperationException(
                    String.format("url = %s delete not implemented", service.url));
            onNak(op_id, e, HttpStatus.SC_METHOD_NOT_ALLOWED);
            onEnd(op_id);
        }

        /**
         * {@inheritDoc}
         *
         * @param op_id   operation identifier.
         * @param operand {@inheritDoc}
         * @param op      op.
         * @return {@inheritDoc}
         */
        @NotNull
        @Override
        public Out write(
                @NotNull final String op_id,
                @NotNull final ZSet operand,
                @NotNull final Op op) {
            try {
                push(operand);
                switch (op) {
                    case C:
                        create(op_id, reset());
                        break;
                    case R:
                        read(op_id, reset());
                        break;
                    case U:
                        update(op_id, reset());
                        break;
                    case D:
                        delete(op_id, reset());
                        break;
                }
            } catch (ExecutionError | ExecutionException | UncheckedExecutionException e) {
                onBeg(op_id);
                onNak(op_id, e, HttpStatus.SC_METHOD_FAILURE);
                onEnd(op_id);
            }
            return this;
        }

    } //~ Out

    public static class Metadata {

        public final String correlation_id;

        public final int delivery_mode;

        public final long delivery_time;

        public final String destination;

        public final long expiration;

        public final String jms_type;

        public final String message_id;

        public final int priority;

        public final boolean redelivered;

        public final String reply_to;

        public final long timestamp;

        public Metadata(
                @NotNull final Message message
        ) throws JMSException {
            correlation_id = message.getJMSCorrelationID();
            delivery_mode = message.getJMSDeliveryMode();
            delivery_time = message.getJMSDeliveryTime();
            destination = message.getJMSDestination() == null ? null : message.getJMSDestination().toString();
            expiration = message.getJMSExpiration();
            jms_type = message.getJMSType();
            message_id = message.getJMSMessageID();
            priority = message.getJMSPriority();
            reply_to = message.getJMSReplyTo() == null ? null : message.getJMSReplyTo().toString();
            redelivered = message.getJMSRedelivered();
            timestamp = message.getJMSTimestamp();
        }

    }

    public enum Target {

        queue,
        topic

    } //~ Target

}
