package zest.io;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.RemovalListener;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Sets;
import com.google.common.util.concurrent.ExecutionError;
import com.google.common.util.concurrent.UncheckedExecutionException;
import com.google.gson.JsonArray;
import org.apache.kafka.clients.CommonClientConfigs;
import org.apache.kafka.clients.admin.*;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.KafkaFuture;
import org.apache.kafka.common.TopicPartition;
import org.apache.kafka.common.serialization.ByteBufferDeserializer;
import org.apache.kafka.common.serialization.ByteBufferSerializer;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.apache.kafka.common.serialization.StringSerializer;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import zest.data.*;
import zest.util.Host;
import zest.util.HttpStatus;
import zest.util.Text;
import zest.util.URL;

import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;
import java.time.Duration;
import java.util.*;
import java.util.concurrent.*;

import static org.apache.kafka.clients.CommonClientConfigs.BOOTSTRAP_SERVERS_CONFIG;

public class Kafka extends Service {

    /**
     * Create a new service connected to the data service at {@code url} address.
     *
     * @param url address of the data service conncted to this service.
     */
    protected Kafka(
            @NotNull final URL url) {
        super(url);
    }

    /**
     * Return the expressions of {@code bootstrap.servers} composed by the {@code url} host and port and
     * additional brokers' addresses in the query parameter {@link CommonClientConfigs#BOOTSTRAP_SERVERS_CONFIG}.
     *
     * @param url Service address.
     * @return The comma separated addresses of _Kafka brokers.
     */
    @NotNull
    protected static String bootstrapServersOf(
            @NotNull final URL url) {
        final StringBuilder servers = new StringBuilder();
        if (Text.isFat(url.host)) {
            servers.append(url.host);
            if (url.port != URL.NO_PORT) {
                servers.append(':').append(url.port);
            }
        }
        final String bootstrap_servers = url.getQueryAsMap().get(BOOTSTRAP_SERVERS_CONFIG);
        if (Text.isFat(bootstrap_servers)) {
            servers.append(",").append(bootstrap_servers);
        }
        return servers.toString();
    }


    /**
     * {@inheritDoc}
     *
     * @param subscriber to notify the operations on this new input channel.
     * @return {@inheritDoc}
     */
    @Override
    @NotNull
    public In in(
            @NotNull final Subscriber subscriber) {
        return new In(this, subscriber);
    }

    /**
     * {@inheritDoc}
     *
     * @param subscriber to notify the operations on this new output channel.
     * @return {@inheritDoc}
     */
    @Override
    @NotNull
    public Out out(
            @NotNull final Subscriber subscriber) {
        return new Out(this, subscriber);
    }

    /**
     * {@inheritDoc}
     */
    public static class In extends Service.In {

        /**
         * Logger.
         */
        protected final static transient Logger LOG = LoggerFactory.getLogger(In.class);

        /**
         * Wait until the reader thread is running.
         */
        private final transient CountDownLatch bootstrapLatch = new CountDownLatch(1);

        /**
         * Comma separated list of URL addresses of Kafka servers.
         */
        private final transient String bootstrap_servers;

        /**
         * Codec used to serialize and deserialize {@link ZSet} instances to and from this channel.
         */
        protected final transient Codec codec = Codec.DEFAULT;

        /**
         * Executor service.
         */
        private final transient ExecutorService executorService = Executors.newSingleThreadExecutor();

        /**
         * Polling period.
         */
        private final long period;

        /**
         * {@link KafkaConsumer} in {@link #read(ZSet)} configuration.
         */
        private final transient Properties properties = new Properties();

        /**
         * Random generator to distribute polling window.
         */
        private final transient Random random = new Random();

        /**
         * Flag stopping the reading threads.
         */
        private final transient CountDownLatch shutdownLatch = new CountDownLatch(1);


        /**
         * {@inheritDoc}
         *
         * @param service    on this channel.
         * @param subscriber of operations through this channel.
         */
        public In(
                @NotNull final Service service,
                @NotNull final Subscriber subscriber) {
            super(service, subscriber);
            final Map<String, String> map = service.url.getQueryAsMap();
            try {
                bootstrap_servers = bootstrapServersOf(service.url);
                final String client_id = map.containsKey(ConsumerConfig.CLIENT_ID_CONFIG)
                        ? map.get(ConsumerConfig.CLIENT_ID_CONFIG)
                        : Host.name();
                final String group_id = map.containsKey(ConsumerConfig.GROUP_ID_CONFIG)
                        ? map.get(ConsumerConfig.GROUP_ID_CONFIG)
                        : Host.name();
                period = Long.parseUnsignedLong(service.url.getQueryAsMap().getOrDefault(
                        Arg.period.name(),
                        Arg.period.byDefault)
                );
                for (final Field field : ConsumerConfig.class.getFields()) {
                    final int modifiers = field.getModifiers();
                    if (Modifier.isStatic(modifiers) && Modifier.isPublic(modifiers)) {
                        final String key = field.get(ConsumerConfig.class).toString();
                        if (map.containsKey(key)) {
                            properties.put(key, map.get(key));
                        }
                    }
                }
                properties.put(BOOTSTRAP_SERVERS_CONFIG, bootstrap_servers);
                properties.put(ConsumerConfig.CLIENT_ID_CONFIG, client_id);
                properties.put(ConsumerConfig.GROUP_ID_CONFIG, group_id);
                properties.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName());
                properties.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, ByteBufferDeserializer.class.getName());
                LOG.debug("{} open.", service.url.toURI());
            } catch (IllegalAccessException | IllegalArgumentException | UnknownHostException e) {
                throw new IllegalStateException(String.format("url = %s can't start", service.url.toURI()), e);
            }
        }

        /**
         * {@inheritDoc}
         * <p></p>
         * Stop the polling {@link #executorService}.
         *
         * @throws IOException {@inheritDoc}
         */
        @Override
        public synchronized void close() throws IOException {
            try {
                shutdownLatch.countDown();
                executorService.shutdown();
            } catch (Exception e) {
                throw new IOException(e);
            } finally {
                LOG.debug("{} closed.", service.url.toURI());
            }
        }

        /**
         * {@inheritDoc}
         *
         * @return {@inheritDoc}
         */
        @Override
        public long getPeriod() {
            return period;
        }

        /**
         * {@inheritDoc}
         *
         * @return {@inheritDoc}
         */
        @Override
        @NotNull
        public Kafka getService() {
            return (Kafka) service;
        }

        /**
         * {@inheritDoc}
         * <p></p>
         * The method runs a single polling thread, multiple call just modify the channel stack of operand to
         * express conditional queries.
         *
         * @param zset operand ignored at the moment.
         *             Properties allows the configuration of the Kafka consumer.
         * @return {@inheritDoc}
         */
        @Override
        @NotNull
        public In read(@NotNull ZSet zset) {
            final Op op = Op.R;
            final long semiperiod = period / 2;
            executorService.submit(() -> {
                try (final KafkaConsumer<String, ByteBuffer> kafkaConsumer = new KafkaConsumer<>(properties)) {
                    final Set<String> topicSet = Sets.newTreeSet();
                    topicSet.add(zset.fqn.toAWSName());
                    if (zset.isMember(Tag.kafka_topics.name())) {
                        final Element element = zset.getElement(Tag.kafka_topics.name());
                        if (element.isText()) {
                            final JsonArray jsonArray = element.getJsonArray();
                            for (int i = 0; i < jsonArray.size(); i++) {
                                topicSet.add(jsonArray.get(i).getAsString());
                            }
                        }
                    }
                    kafkaConsumer.subscribe(topicSet);
                    kafkaConsumer.poll(Duration.ZERO);
                    while (shutdownLatch.getCount() > 0) {
                        final String op_id = op.id(service).toURI();
                        onBeg(op_id);
                        final Duration timeout = Duration.ofSeconds(semiperiod + random.nextLong() % semiperiod);
                        try {
                            final ConsumerRecords<String, ByteBuffer> consumerRecords = kafkaConsumer.poll(timeout);
                            if (!consumerRecords.isEmpty()) {
                                consumerRecords.forEach((ConsumerRecord<String, ByteBuffer> consumerRecord) -> {
                                    final Metadata metadata = new Metadata(
                                            consumerRecord.topic(),
                                            consumerRecord.key(),
                                            consumerRecord.offset(),
                                            consumerRecord.partition(),
                                            consumerRecord.timestamp());
                                    final ByteBuffer buffer = consumerRecord.value();
                                    final String body = new String(buffer.array());
                                    final ZSet load = codec.decode(body);
                                    final ZSet ack = ZSet.fqn(load.fqn)
                                            .put(load)
                                            .put(TAG_ACK, codec.encode(metadata).set())
                                            .set(load.id);
                                    onAck(op_id, ack, HttpStatus.SC_OK);
                                });
                                kafkaConsumer.commitSync();
                            }
                            Thread.sleep(1000 * period - timeout.toMillis());
                        } catch (Exception e) {
                            onNak(op_id, e, HttpStatus.SC_METHOD_FAILURE);
                        }
                        onEnd(op_id);
                    }
                }
            });
            return this;
        }

    } //~ In

    /**
     * {@inheritDoc}
     */
    public static class Out extends Service.Out {

        /**
         * Logger.
         */
        protected final static transient Logger LOG = LoggerFactory.getLogger(Out.class);

        /**
         * {@link #kafkaProducer} is shared for out channels having the same URL.
         */
        protected final static transient Cache<String, KafkaProducer<String, ByteBuffer>> KAFKA_PRODUCER_CACHE = CacheBuilder
                .newBuilder()
                .removalListener((RemovalListener<String, KafkaProducer<String, ByteBuffer>>) notification -> {
                    final KafkaProducer<String, ByteBuffer> kafkaProducer = notification.getValue();
                    kafkaProducer.close();
                })
                .build();

        /**
         * Kafka administration client is thread safe? Suppose not yet. Use synchronized.
         */
        private final transient AdminClient adminClient;

        /**
         * Addresses of the kafka servers.
         */
        private final transient String bootstrap_servers;

        /**
         * Kafka client id.
         */
        private final transient String client_id;

        /**
         * Codec used to wrap datum in {@link Element}.
         */
        protected final transient Codec codec = Codec.DEFAULT;

        /**
         * Kafka producer is thread safe.
         */
        private final transient KafkaProducer kafkaProducer;

        /**
         * Return a new output channel for this {@code service} with at least one {@code subscriber}.
         *
         * @param service    of this output channel.
         * @param subscriber of operations sent to this channel.
         * @throws IllegalStateException is the service can't start.
         */
        public Out(
                @NotNull final Service service,
                @NotNull final Subscriber subscriber)
                throws IllegalStateException {
            super(service, subscriber);
            final Map<String, String> map = service.url.getQueryAsMap();
            try {
                bootstrap_servers = bootstrapServersOf(service.url);
                client_id = map.containsKey(ConsumerConfig.CLIENT_ID_CONFIG)
                        ? map.get(ConsumerConfig.CLIENT_ID_CONFIG)
                        : Host.name();
                // Admin configuration
                final Properties adminProperties = new Properties();
                for (final Field field : ProducerConfig.class.getFields()) {
                    final int modifiers = field.getModifiers();
                    if (Modifier.isStatic(modifiers) && Modifier.isPublic(modifiers)) {
                        final String key = field.get(AdminClientConfig.class).toString();
                        if (map.containsKey(key)) {
                            adminProperties.put(key, map.get(key));
                        }
                    }
                }
                adminProperties.put(BOOTSTRAP_SERVERS_CONFIG, bootstrap_servers);
                adminClient = AdminClient.create(adminProperties);
                // Producer configuration
                final Properties producerProperties = new Properties();
                for (final Field field : ProducerConfig.class.getFields()) {
                    final int modifiers = field.getModifiers();
                    if (Modifier.isStatic(modifiers) && Modifier.isPublic(modifiers)) {
                        final Object key = field.get(ProducerConfig.class);
                        if (map.containsKey(key)) {
                            producerProperties.put(key, map.get(key));
                        }
                    }
                }
                producerProperties.put(BOOTSTRAP_SERVERS_CONFIG, bootstrap_servers);
                producerProperties.put(ConsumerConfig.CLIENT_ID_CONFIG, client_id);
                producerProperties.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
                producerProperties.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, ByteBufferSerializer.class.getName());
                kafkaProducer = KAFKA_PRODUCER_CACHE.get(service.url.toURI(), () -> new KafkaProducer<>(producerProperties));
                LOG.debug("{} open.", service.url.toURI());
            } catch (ExecutionException | IllegalAccessException | IllegalArgumentException | UnknownHostException e) {
                throw new IllegalStateException(String.format("url = %s can't start", service.url.toURI()), e);
            }
        }

        /**
         * {@inheritDoc}
         *
         * @throws IOException {@inheritDoc}
         */
        @Override
        public synchronized void close() {
            adminClient.close();
            LOG.debug("{} closed.", service.url.toURI());
        }

        private void create(
                @NotNull final String op_id,
                @NotNull final Deque<ZSet> stack) {
            // todo add predicate processor
            final ZSet zset = stack.removeLast();
            if (!zset.isIdentifiable()) {
                createTopic(op_id, zset);
            } else {
                createRecord(op_id, zset);
            }
        }

        private void createRecord(
                @NotNull final String op_id,
                @NotNull final ZSet zset) {
            onBeg(op_id);
            final String topic = zset.fqn.toAWSName();
            final String body = zset.toJSON().toString();
            final ByteBuffer buffer = ByteBuffer.wrap(body.getBytes());
            final ProducerRecord<String, ByteBuffer> record = new ProducerRecord<>(topic, zset.id, buffer);
            kafkaProducer.send(record, (metadata, exception) -> {
                if (exception != null) {
                    onNak(op_id, exception, HttpStatus.SC_METHOD_FAILURE);
                } else {
                    final ZSet ack = ZSet.fqn(zset.fqn)
                            .put(Tag.ack.name(), codec.encode(metadata).set())
                            .set(zset.id);
                    onAck(op_id, ack, HttpStatus.SC_CREATED);
                }
                onEnd(op_id);
            });
        }

        private synchronized void createTopic(
                @NotNull final String op_id,
                @NotNull final ZSet zset) {
            onBeg(op_id);
            final String topic = zset.fqn.toAWSName();
            final int numPartitions = zset.json.has(Tag.kafka_partitions.name())
                    ? zset.json.get(Tag.kafka_partitions.name()).getAsInt()
                    : 1;
            final short replicationFactor = zset.json.has(Tag.kafka_replicas.name())
                    ? zset.json.get(Tag.kafka_replicas.name()).getAsShort()
                    : 1;
            final Set<NewTopic> topicSet = ImmutableSet.of(new NewTopic(topic, numPartitions, replicationFactor));
            final CreateTopicsResult res = adminClient.createTopics(topicSet);
            for (final Map.Entry<String, KafkaFuture<Void>> entry : res.values().entrySet()) {
                final KafkaFuture<Void> promise = entry.getValue();
                promise.whenComplete((ignored, t) -> {
                    if (t != null) {
                        onNak(op_id, t, HttpStatus.SC_METHOD_FAILURE);
                    } else {
                        onAck(op_id, zset, HttpStatus.SC_CREATED);
                    }
                });
            }
            res.all().whenComplete((ignored, t) -> onEnd(op_id));
        }

        private void delete(
                @NotNull final String op_id,
                @NotNull final Deque<ZSet> stack) {
            final ZSet zset = stack.removeLast();
            if (!zset.isIdentifiable()) {
                deleteRecord(op_id, zset);
            } else {
                deleteTopic(op_id, zset);
            }
        }

        private void deleteRecord(
                @NotNull final String op_id,
                @NotNull final ZSet zset) {
            final String topic = zset.fqn.toAWSName();
            final int partition = zset.json.has(Tag.kafka_partition.name())
                    ? zset.json.get(Tag.kafka_partition.name()).getAsInt()
                    : 0;
            final long offset = zset.json.has(Tag.kafka_offset.name())
                    ? zset.json.get(Tag.kafka_offset.name()).getAsInt()
                    : 0;
            if (offset > 0) {
                final TopicPartition topicPartition = new TopicPartition(topic, partition);
                final RecordsToDelete recordsToDelete = RecordsToDelete.beforeOffset(offset);
                final Map<TopicPartition, RecordsToDelete> map = ImmutableMap.of(topicPartition, recordsToDelete);
                final DeleteRecordsResult res = adminClient.deleteRecords(map);
                for (Map.Entry<TopicPartition, KafkaFuture<DeletedRecords>> entry : res.lowWatermarks().entrySet()) {
                    final KafkaFuture<DeletedRecords> promise = entry.getValue();
                    promise.whenComplete((deletedRecords, t) -> {
                        if (t != null) {
                            onNak(op_id, t, HttpStatus.SC_METHOD_FAILURE);
                        } else {
                            final FQN fqn = FQN.of(topic, Integer.toString(entry.getKey().partition()));
                            final ZSet ack = ZSet.fqn(fqn)
                                    .put(Tag.kafka_offset.name(), codec.encode(deletedRecords.lowWatermark()).set())
                                    .set(zset.id);
                            onAck(op_id, ack, HttpStatus.SC_OK);
                        }
                    });
                }
                res.all().whenComplete((ignored, t) -> onEnd(op_id));
            } else {
                onAck(op_id, zset, HttpStatus.SC_NO_CONTENT);
                onEnd(op_id);
            }
        }

        private void deleteTopic(
                @NotNull final String op_id,
                @NotNull final ZSet zset) {
            final String topic = zset.fqn.toAWSName();
            final Set<String> topics = ImmutableSet.of(topic);
            final DeleteTopicsResult res = adminClient.deleteTopics(topics);
            for (final Map.Entry<String, KafkaFuture<Void>> entry : res.values().entrySet()) {
                final KafkaFuture<Void> kafkaFuture = entry.getValue();
                kafkaFuture.whenComplete((ignored, t) -> {
                    if (t != null) {
                        onNak(op_id, t, HttpStatus.SC_METHOD_FAILURE);
                    } else {
                        onAck(op_id, zset, HttpStatus.SC_OK);
                    }
                });
            }
            res.all().whenComplete((ignored, t) -> onEnd(op_id));
        }

        /**
         * {@inheritDoc}
         *
         * @return {@inheritDoc}
         */
        @Override
        @NotNull
        public Kafka getService() {
            return (Kafka) service;
        }

        private void read(
                @NotNull final String op_id,
                @NotNull final  Deque<ZSet> stack) {
            final ZSet zset = stack.removeLast();
            onBeg(op_id);
            try {
                final String group_id = zset.json.has(ConsumerConfig.GROUP_ID_CONFIG)
                        ? (String) codec.decode(zset.getElement(ConsumerConfig.GROUP_ID_CONFIG))
                        : Host.name();
                final long period = zset.json.has(Tag.kafka_timeout.name())
                        ? (Long) codec.decode(zset.getElement(Tag.kafka_timeout.name()))
                        : Long.parseUnsignedLong(Arg.period.byDefault);
                final Set<String> topicSet = Sets.newTreeSet();
                topicSet.add(zset.fqn.toAWSName());
                if (zset.isMember(Tag.kafka_topics.name())) {
                    final Element element = zset.getElement(Tag.kafka_topics.name());
                    if (element.isText()) {
                        final JsonArray jsonArray = element.getJsonArray();
                        for (int i = 0; i < jsonArray.size(); i++) {
                            topicSet.add(jsonArray.get(i).getAsString());
                        }
                    }
                }
                final Properties properties = new Properties();
                for (final Field field : ConsumerConfig.class.getFields()) {
                    final int modifiers = field.getModifiers();
                    if (Modifier.isStatic(modifiers) && Modifier.isPublic(modifiers)) {
                        final Object key = field.get(ConsumerConfig.class);
                        if (zset.json.has(key.toString())) {
                            properties.put(key, zset.getElement(key.toString()).json.getAsString());
                        }
                    }
                }
                properties.put(BOOTSTRAP_SERVERS_CONFIG, bootstrap_servers);
                properties.put(ConsumerConfig.CLIENT_ID_CONFIG, client_id);
                properties.put(ConsumerConfig.GROUP_ID_CONFIG, group_id);
                properties.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName());
                properties.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, ByteBufferDeserializer.class.getName());
                try (final KafkaConsumer kafkaConsumer = new KafkaConsumer<String, ByteBuffer>(properties)) {
                    kafkaConsumer.subscribe(topicSet);
                    kafkaConsumer.poll(Duration.ZERO);
                    final Duration timeout = Duration.ofSeconds(period);
                    final ConsumerRecords<String, ByteBuffer> consumerRecords = kafkaConsumer.poll(timeout);
                    if (!consumerRecords.isEmpty()) {
                        consumerRecords.forEach(consumerRecord -> {
                            final Metadata metadata = new Metadata(
                                    consumerRecord.topic(),
                                    consumerRecord.key(),
                                    consumerRecord.offset(),
                                    consumerRecord.partition(),
                                    consumerRecord.timestamp());
                            final ByteBuffer buffer = consumerRecord.value();
                            final String body = new String(buffer.array());
                            final ZSet load = codec.decode(body);
                            final ZSet ack = ZSet.fqn(load.fqn)
                                    .put(load)
                                    .put(TAG_ACK, codec.encode(metadata).set())
                                    .set(load.id);
                            onAck(op_id, ack, HttpStatus.SC_OK);
                        });
                        kafkaConsumer.commitSync();
                    }
                } catch (Exception e) {
                    onNak(op_id, e, HttpStatus.SC_METHOD_FAILURE);
                }
            } catch (IllegalArgumentException | IllegalAccessException | UnknownHostException e) {
                onNak(op_id, e, HttpStatus.SC_BAD_REQUEST);
            }
            onEnd(op_id);
        }

        private void update(
                @NotNull final String op_id,
                @NotNull final Deque<ZSet> stack) {
            final ZSet zset = stack.removeLast();
            createRecord(op_id, zset);
        }

        @Override
        @NotNull
        public Out write(
                @NotNull final String op_id,
                @NotNull final ZSet operand,
                @NotNull final Op op) {
            try {
                push(operand);
                switch (op) {
                    case C:
                        create(op_id, reset());
                        break;
                    case R:
                        read(op_id, reset());
                        break;
                    case U:
                        update(op_id, reset());
                        break;
                    case D:
                        delete(op_id, reset());
                        break;
                }
            } catch (ExecutionError | ExecutionException | UncheckedExecutionException e) {
                onBeg(op_id);
                onNak(op_id, e, HttpStatus.SC_METHOD_FAILURE);
                onEnd(op_id);
            }
            return this;
        }

    } //~ Out

    public enum Tag {

        ack,
        kafka_partition,
        kafka_partitions,
        kafka_offset,
        kafka_replicas,
        kafka_timeout,
        kafka_topics

    }

    public static class Metadata {

        public final String key;

        public final long offset;

        public final int partition;

        public final long timestamp;

        public final String topic;

        public Metadata(
                @NotNull final String topic,
                @NotNull final String key,
                final long offset,
                final int partition,
                final long timestamp) {
            this.key = key;
            this.offset = offset;
            this.partition = partition;
            this.timestamp = timestamp;
            this.topic = topic;
        }
    }

}
