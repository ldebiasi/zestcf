package zest.io;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.RemovalListener;
import com.google.common.util.concurrent.ExecutionError;
import com.google.common.util.concurrent.UncheckedExecutionException;
import com.mongodb.reactivestreams.client.*;
import org.bson.Document;
import org.jetbrains.annotations.NotNull;
import org.reactivestreams.Subscription;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import zest.data.Op;
import zest.data.ZSet;
import zest.util.HttpStatus;
import zest.util.URL;

import java.io.IOException;
import java.util.Deque;
import java.util.List;
import java.util.concurrent.ExecutionException;

public class Mongo extends Service {

    /**
     * Create a new MongoDb service connected to database at {@code url} address.
     *
     * @param url address of the MongoDb server.
     */
    public Mongo(
            @NotNull final URL url) {
        super(url);
    }

    /**
     * {@inheritDoc}
     *
     * @param subscriber to notify the operations on this new input channel.
     * @return {@inheritDoc}
     */
    @Override
    @NotNull
    public In in(
            @NotNull final Subscriber subscriber) {
        return null;
    }

    /**
     * {@inheritDoc}
     *
     * @param subscriber to notify the operations on this new output channel.
     * @return {@inheritDoc}
     */
    @Override
    @NotNull
    public Out out(
            @NotNull final Subscriber subscriber) {
        return null;
    }

    /**
     * {@inheritDoc}
     */
    public static class In extends Service.In {

        /**
         * Logger.
         */
        protected final static transient Logger LOG = LoggerFactory.getLogger(In.class);


        /**
         * {@link #mongoClient} is shared for the channels having the same URL.
         */
        protected final static transient Cache<String, MongoClient> MONGO_CLIENT_CACHE = CacheBuilder
                .newBuilder()
                .removalListener((RemovalListener<String, MongoClient>) notification -> {
                    final MongoClient mongoClient = notification.getValue();
                    mongoClient.close();
                })
                .build();

        /**
         * Thread safe.
         */
        protected final transient MongoClient mongoClient;

        /**
         * {@inheritDoc}
         *
         * @param service    on this channel.
         * @param subscriber of operations through this channel.
         */
        public In(
                @NotNull final Service service,
                @NotNull final Subscriber subscriber) {
            super(service, subscriber);
            try {
                mongoClient = MONGO_CLIENT_CACHE.get(service.url.toURI(), () -> {
                    final String url = service.url.port == URL.NO_PORT
                            ? String.format("mongodb://%s", service.url.host)
                            : String.format("mongodb://%s:%d", service.url.host, service.url.port);
                    final MongoClient mongoClient = MongoClients.create(url);
                    // todo start?
                    return mongoClient;
                });
            } catch (ExecutionException e) {
                throw new IllegalStateException(String.format("url = %s can't start", service.url.toURI()), e);
            }
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public void close() throws IOException {

        }

        @Override
        public long getPeriod() {
            return 0;
        }

        @Override
        @NotNull
        public Mongo getService() {
            return (Mongo) service;
        }

        @Override
        @NotNull
        public In read(
                @NotNull final ZSet zset) throws IOException {
            return this;
        }


    } //~ In

    /**
     * {@inheritDoc}
     */
    public static class Out extends Service.Out {

        /**
         * Logger.
         */
        protected final static transient Logger LOG = LoggerFactory.getLogger(Out.class);

        /**
         * {@link #mongoClient} is shared for the channels having the same URL.
         */
        protected final static transient Cache<String, MongoClient> MONGO_CLIENT_CACHE = CacheBuilder
                .newBuilder()
                .removalListener((RemovalListener<String, MongoClient>) notification -> {
                    final MongoClient mongoClient = notification.getValue();
                    mongoClient.close();
                })
                .build();

        /**
         * Thread safe.
         */
        protected final transient MongoClient mongoClient;

        /**
         * Return a new output channel for this {@code service} with at least one {@code subscriber}.
         *
         * @param service    of this output channel.
         * @param subscriber of operations sent to this channel.
         * @throws IllegalStateException if the service can't start.
         * @see <a href="https://mongodb.github.io/mongo-java-driver/4.0/driver-reactive/tutorials/connect-to-mongodb/">Connect to MongoDB</a>
         */
        public Out(
                @NotNull final Service service,
                @NotNull final Subscriber subscriber)
                throws IllegalStateException {
            super(service, subscriber);
            try {
                mongoClient = MONGO_CLIENT_CACHE.get(service.url.toURI(), () -> {
                    final String url = service.url.port == URL.NO_PORT
                            ? String.format("mongodb://%s", service.url.host)
                            : String.format("mongodb://%s:%d", service.url.host, service.url.port);
                    final MongoClient mongoClient = MongoClients.create(url);
                    // todo start?
                    return mongoClient;
                });
            } catch (ExecutionException e) {
                throw new IllegalStateException(String.format("url = %s can't start", service.url.toURI()), e);
            }
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public synchronized void close() {
            LOG.debug("{} closed.", service.url.toURI());
        }

        private void create(
                @NotNull final String op_id,
                @NotNull final Deque<ZSet> stack) {
            onBeg(op_id);
            final ZSet zset = stack.removeLast();
            final List<String> fqnList = zset.fqn.getPartList();
            if (fqnList.size() > 1) {
                final MongoDatabase database = mongoClient.getDatabase(fqnList.get(0));
                final String collectionName = fqnList.get(1);
                final MongoCollection<Document> collection = database.getCollection(collectionName);
                if (!zset.isIdentifiable()) {
                    database.createCollection(collectionName).subscribe(new org.reactivestreams.Subscriber<Success>() {
                        @Override
                        public void onSubscribe(Subscription s) {
                            // Ignore.
                        }

                        @Override
                        public void onNext(Success success) {
                            onAck(op_id, zset, HttpStatus.SC_CREATED);
                        }

                        @Override
                        public void onError(Throwable t) {
                            onNak(op_id, t, HttpStatus.SC_METHOD_FAILURE);
                        }

                        @Override
                        public void onComplete() {
                            onEnd(op_id);
                        }
                    });
                } else {
                    
                }
            } else {
                final Exception e = new IllegalArgumentException(
                        String.format("fqn = %s doesn't express <db>/<collection>", zset.fqn));
                onNak(op_id, e, HttpStatus.SC_BAD_REQUEST);
                onEnd(op_id);
            }
        }

        private void delete(
                @NotNull final String op_id,
                @NotNull final Deque<ZSet> stack) {
            onBeg(op_id);
            final ZSet zset = stack.removeLast();
            final List<String> fqnList = zset.fqn.getPartList();
            if (fqnList.size() > 1) {
                final MongoDatabase database = mongoClient.getDatabase(fqnList.get(0));
                final MongoCollection<Document> collection = database.getCollection(fqnList.get(1));
            } else {
                final Exception e = new IllegalArgumentException(
                        String.format("fqn = %s doesn't express <db>/<collection>", zset.fqn));
                onNak(op_id, e, HttpStatus.SC_BAD_REQUEST);
                onEnd(op_id);
            }
        }

        /**
         * {@inheritDoc}
         *
         * @return {@inheritDoc}
         */
        @NotNull
        @Override
        public Mongo getService() {
            return (Mongo) service;
        }

        private void read(
                @NotNull final String op_id,
                @NotNull final Deque<ZSet> stack) {
            onBeg(op_id);
            final ZSet zset = stack.removeLast();
            final List<String> fqnList = zset.fqn.getPartList();
            if (fqnList.size() > 1) {
                final MongoDatabase database = mongoClient.getDatabase(fqnList.get(0));
                final MongoCollection<Document> collection = database.getCollection(fqnList.get(1));
            } else {
                final Exception e = new IllegalArgumentException(
                        String.format("fqn = %s doesn't express <db>/<collection>", zset.fqn));
                onNak(op_id, e, HttpStatus.SC_BAD_REQUEST);
                onEnd(op_id);
            }

        }

        private void update(
                @NotNull final String op_id,
                @NotNull final Deque<ZSet> stack) {
            onBeg(op_id);
            final ZSet zset = stack.removeLast();
            final List<String> fqnList = zset.fqn.getPartList();
            if (fqnList.size() > 1) {
                final MongoDatabase database = mongoClient.getDatabase(fqnList.get(0));
                final MongoCollection<Document> collection = database.getCollection(fqnList.get(1));
            } else {
                final Exception e = new IllegalArgumentException(
                        String.format("fqn = %s doesn't express <db>/<collection>", zset.fqn));
                onNak(op_id, e, HttpStatus.SC_BAD_REQUEST);
                onEnd(op_id);
            }
        }

        /**
         * {@inheritDoc}
         *
         * @param op_id   operation identifier.
         * @param operand {@inheritDoc}
         * @param op      op.
         * @return {@inheritDoc}
         */
        @NotNull
        @Override
        public Out write(
                @NotNull final String op_id,
                @NotNull final ZSet operand,
                @NotNull final Op op) {
            try {
                push(operand);
                switch (op) {
                    case C:
                        create(op_id, reset());
                        break;
                    case R:
                        read(op_id, reset());
                        break;
                    case U:
                        update(op_id, reset());
                        break;
                    case D:
                        delete(op_id, reset());
                        break;
                }
            } catch (ExecutionError | ExecutionException | UncheckedExecutionException e) {
                onBeg(op_id);
                onNak(op_id, e, HttpStatus.SC_METHOD_FAILURE);
                onEnd(op_id);
            }

            return this;
        }

    } //~ Out

}
