package zest.io;

import com.google.common.util.concurrent.ExecutionError;
import com.google.common.util.concurrent.UncheckedExecutionException;
import com.google.gson.JsonIOException;
import com.google.gson.JsonSyntaxException;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import zest.data.Codec;
import zest.data.FQN;
import zest.data.Op;
import zest.data.ZSet;
import zest.util.HttpStatus;
import zest.util.Text;
import zest.util.URL;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.sql.SQLException;
import java.util.Deque;
import java.util.concurrent.ExecutionException;

/**
 * Abstraction of the File System.
 * <p></p>
 * Immutable.
 *
 */
public class File extends Service {

    /**
     * Create a new service to interface the file system.
     *
     * @param url where is the parent directory for operations on channels.
     */
    protected File(
            @NotNull final URL url) {
        super(url);
    }

    @Override
    @NotNull
    public In in(
            @NotNull final Subscriber subscriber) {
        return new In(this, subscriber);
    }

    /**
     * {@inheritDoc}
     *
     * @param subscriber {@inheritDoc}
     * @return {@inheritDoc}
     */
    @NotNull
    @Override
    public Out out(
            @NotNull final Subscriber subscriber) {
        return new Out(this, subscriber);
    }

    public static class In extends Service.In {

        /**
         * {@inheritDoc}
         *
         * @param service  on this channel.
         * @param subscriber of operations through this channel.
         */
        public In(
                @NotNull Service service,
                @NotNull Subscriber subscriber) {
            super(service, subscriber);
        }

        @Override
        public Service.@NotNull In read(@NotNull ZSet zset) {
            return null;
        }

        @Override
        public void close() throws IOException {

        }

        @Override
        public long getPeriod() {
            return 0;
        }

        @Override
        public @NotNull Service getService() {
            return null;
        }

    } //~ In


    /**
     * {@inheritDoc}
     */
    public static class Out extends Service.Out {

        /**
         * Logger.
         */
        protected static final Logger LOG = LoggerFactory.getLogger(Out.class);

        /**
         * {@inheritDoc}
         *
         * @param service  {@inheritDoc}
         * @param subscriber {@inheritDoc}
         */
        protected Out(
                @NotNull final Service service,
                @NotNull final Subscriber subscriber) {
            super(service, subscriber);
            LOG.debug("{} open.", service.url);
        }

        /**
         * Return the target directory for {@code fqn} resolved against the {@link #url} of this service.
         *
         * @param fqn      of the target directory.
         * @param <Target> {@link Path} for File service.
         * @return the target directory for {@code fqn} resolved against the {@link #url} of this service.
         * @throws URISyntaxException
         */
        @NotNull
        private <Target> Target aim(
                @NotNull final FQN fqn)
                throws URISyntaxException {
            final java.net.URI uri = new java.net.URI(service.url.toURI());
            return (Target) Paths.get(uri).resolve(fqn.toPath());
        }

        /**
         * Return the target file for {@code fqn} resolved against the {@link #url} of this service.
         *
         * @param fqn      of the target file.
         * @param <Target> {@link Path} for File service.
         * @return the target file for {@code fqn} resolved against the {@link #url} of this service.
         * @throws URISyntaxException
         */
        @NotNull
        private <Target> Target aim(
                @NotNull final FQN fqn,
                @NotNull final String id
        ) throws URISyntaxException {
            final java.net.URI uri = new java.net.URI(service.url.toURI());
            return (Target) Paths.get(
                    Paths.get(uri).resolve(fqn.toPath()).toString(),
                    Text.encode(id));
        }

        /**
         * {@inheritDoc}
         *
         * @throws IOException {@inheritDoc}
         */
        @Override
        public void close()
                throws IOException {
            LOG.debug("{} closed.", service.url.toURI());
        }

        /**
         * Create the directory at {@code target} path and return the path.
         *
         * @param target path of the directory to create.
         * @return the path of the directory created.
         * @throws FileAlreadyExistsException
         * @throws IOException
         * @throws SecurityException
         * @throws UnsupportedOperationException
         */
        @NotNull
        private Path create(
                @NotNull final Path target)
                throws
                FileAlreadyExistsException,
                IOException,
                SecurityException,
                UnsupportedOperationException {
            return Files.createDirectories(target);
        }

        /**
         * Create the file at the {code target} path and return the path.
         *
         * @param target path of the file to create.
         * @param zset   file content in ZEST notation.
         * @return the path of the file to created.
         * @throws FileAlreadyExistsException
         * @throws IOException
         */
        @NotNull
        private Path create(
                @NotNull final Path target,
                @NotNull final ZSet zset)
                throws
                FileAlreadyExistsException,
                IOException {
            final java.io.File file = new java.io.File(((Path) target).toUri());
            if (!file.exists()) {
                final String payload = zset.toJSON().toString();
                try (final FileWriter fileWriter = new FileWriter(file)) {
                    fileWriter.append(payload);
                    fileWriter.flush();
                }
                return target;
            }
            throw new FileAlreadyExistsException(file.toString());
        }

        private void create(
                @NotNull final String op_id,
                @NotNull final Deque<ZSet> stack) {
            final ZSet zset = stack.getLast();
            try {
                Path directory;
                directory = aim(zset.fqn);
                if (!isExisting(directory)) {
                    directory = create(directory);
                    final BasicFileAttributes bfa = Files.readAttributes(directory, BasicFileAttributes.class);
                    final ZSet ack = ZSet.fqn(zset.fqn).put(TAG_ACK, Codec.DEFAULT.encode(bfa).set()).set();
                    onAck(op_id, ack, HttpStatus.SC_CREATED);
                }
                if (isExisting(directory)) {
                    Path target = create(aim(zset.fqn, zset.id), zset);
                    final BasicFileAttributes bfa = Files.readAttributes(target, BasicFileAttributes.class);
                    final ZSet ack = ZSet.fqn(zset.fqn).put(TAG_ACK, Codec.DEFAULT.encode(bfa).set()).set(zset.id);
                    onAck(op_id, ack, HttpStatus.SC_CREATED);
                } else {
                    throw new FileNotFoundException(directory.toString());
                }
            } catch (final URISyntaxException e) {
                onNak(op_id, e, HttpStatus.SC_BAD_REQUEST);
            } catch (final FileAlreadyExistsException e) {
                onNak(op_id, e, HttpStatus.SC_CONFLICT);
            } catch (final FileNotFoundException e) {
                onNak(op_id, e, HttpStatus.SC_NOT_FOUND);
            } catch (final IOException e) {
                onNak(op_id, e, HttpStatus.SC_SERVICE_UNAVAILABLE);
            } catch (final SecurityException e) {
                onNak(op_id, e, HttpStatus.SC_FORBIDDEN);
            } catch (final UnsupportedOperationException e) {
                onNak(op_id, e, HttpStatus.SC_NOT_IMPLEMENTED);
            }
        }


        @NotNull
        private Path delete(
                @NotNull final Path target)
                throws
                IOException,
                SecurityException {
            Files.deleteIfExists(target);
            return target;
        }

        private void delete(
                @NotNull final String op_id,
                @NotNull final Deque<ZSet> stack) {
            final ZSet zset = stack.getLast();
            try {
                Path target = aim(zset.fqn, zset.id);
                if (isExisting(target)) {
                    BasicFileAttributes bfa;
                    bfa = Files.readAttributes(target, BasicFileAttributes.class);
                    ZSet ack;
                    ack = ZSet.fqn(zset.fqn).put(TAG_ACK, Codec.DEFAULT.encode(bfa).set()).set(zset.id);
                    target = delete(target).getParent();
                    onAck(op_id, ack, HttpStatus.SC_OK);
                    while (Files.exists(target) && target.toFile().listFiles().length == 0) {
                        bfa = Files.readAttributes(target, BasicFileAttributes.class);
                        target = delete(target).getParent();
                        ack = ZSet.fqn(FQN.of(target.toString()))
                                .put(TAG_ACK, Codec.DEFAULT.encode(bfa).set())
                                .set();
                        onAck(op_id, ack, HttpStatus.SC_OK);
                    }
                } else {
                    throw new FileNotFoundException(target.toString());
                }
            } catch (URISyntaxException e) {
                onNak(op_id, e, HttpStatus.SC_BAD_REQUEST);
            } catch (FileNotFoundException e) {
                onNak(op_id, e, HttpStatus.SC_NOT_FOUND);
            } catch (IOException e) {
                onNak(op_id, e, HttpStatus.SC_SERVICE_UNAVAILABLE);
            } catch (SecurityException e) {
                onNak(op_id, e, HttpStatus.SC_FORBIDDEN);
            } catch (UnsupportedOperationException e) {
                onNak(op_id, e, HttpStatus.SC_NOT_IMPLEMENTED);
            }
        }


        /**
         * {@inheritDoc}
         *
         * @return {@inheritDoc}
         */
        @NotNull
        @Override
        public Service getService() {
            return service;
        }

        /**
         * Return {@code true} if the {@code target} exists.
         *
         * @param target   to check if exists.
         * @param <Target> Path for File service.
         * @return {@code true} if the {@code target} exists.
         * @throws SQLException
         */
        private <Target> boolean isExisting(
                @NotNull final Target target) throws SecurityException {
            final Path path = (Path) target;
            return Files.exists(path);
        }

        @NotNull
        private <Target> ZSet read(
                @NotNull final Target target)
                throws
                IOException,
                JsonIOException,
                JsonSyntaxException,
                SecurityException {
            final java.io.File file = new java.io.File(((Path) target).toUri());
            try (final FileReader fileReader = new FileReader(file)) {
                return Codec.DEFAULT.gson.fromJson(fileReader, ZSet.class);
            }
        }


        private void read(
                @NotNull final String op_id,
                @NotNull final Deque<ZSet> stack) {
            final ZSet zset = stack.getLast();
            try {
                Path target = zset.isIdentifiable() ? aim(zset.fqn, zset.id) : aim(zset.fqn);
                if (Files.isDirectory(target)) {
                    // todo
                } else {
                    final ZSet ack = read(target);
                    onAck(op_id, ack, HttpStatus.SC_OK);
                }
            } catch (URISyntaxException e) {
                onNak(op_id, e, HttpStatus.SC_BAD_REQUEST);
            } catch (FileNotFoundException | NoSuchFileException e) {
                onNak(op_id, e, HttpStatus.SC_NOT_FOUND);
            } catch (IOException e) {
                onNak(op_id, e, HttpStatus.SC_SERVICE_UNAVAILABLE);
            } catch (JsonIOException e) {
                onNak(op_id, e, HttpStatus.SC_SERVICE_UNAVAILABLE);
            } catch (JsonSyntaxException e) {
                onNak(op_id, e, HttpStatus.SC_UNSUPPORTED_MEDIA_TYPE);
            } catch (SecurityException e) {
                onNak(op_id, e, HttpStatus.SC_FORBIDDEN);
            } catch (UnsupportedOperationException e) {
                onNak(op_id, e, HttpStatus.SC_NOT_IMPLEMENTED);
            }
        }

        @NotNull
        private <Target> Target update(
                @NotNull final Target target,
                @NotNull final ZSet zSet)
                throws IOException {
            final java.io.File file = new java.io.File(((Path) target).toUri());
            if (file.exists()) {
                final String payload = Codec.DEFAULT.gson.toJson(zSet);
                try (final FileWriter fileWriter = new FileWriter(file)) {
                    fileWriter.append(payload);
                    fileWriter.flush();
                }
                return target;
            }
            throw new FileNotFoundException(file.toString());
        }

        private void update(
                @NotNull final String op_id,
                @NotNull final Deque<ZSet> stack) {
            final ZSet zset = stack.getLast();
            try {
                Path target = aim(zset.fqn, zset.id);
                if (isExisting(target)) {
                    target = update(target, zset);
                    final BasicFileAttributes bfa = Files.readAttributes(target, BasicFileAttributes.class);
                    final ZSet ack = ZSet.fqn(zset.fqn).put(TAG_ACK, Codec.DEFAULT.encode(bfa).set()).set(zset.id);
                    onAck(op_id, ack, HttpStatus.SC_ACCEPTED);
                } else {
                    throw new FileNotFoundException(target.toString());
                }
            } catch (final URISyntaxException e) {
                onNak(op_id, e, HttpStatus.SC_BAD_REQUEST);
            } catch (final FileAlreadyExistsException e) {
                onNak(op_id, e, HttpStatus.SC_CONFLICT);
            } catch (final FileNotFoundException e) {
                onNak(op_id, e, HttpStatus.SC_NOT_FOUND);
            } catch (final IOException e) {
                onNak(op_id, e, HttpStatus.SC_SERVICE_UNAVAILABLE);
            } catch (final SecurityException e) {
                onNak(op_id, e, HttpStatus.SC_FORBIDDEN);
            } catch (final UnsupportedOperationException e) {
                onNak(op_id, e, HttpStatus.SC_NOT_IMPLEMENTED);
            }
        }


        /**
         * {@inheritDoc}
         *
         * @param op_id {@inheritDoc}
         * @param zset  {@inheritDoc}
         * @param op    {@inheritDoc}
         * @return {@inheritDoc}
         */
        @NotNull
        @Override
        public Service.Out write(
                @NotNull final String op_id,
                @NotNull final ZSet zset,
                @NotNull final Op op) {
            onBeg(op_id);
            try {
                push(zset);
                switch (op) {
                    case C:
                        create(op_id, reset());
                        break;
                    case R:
                        read(op_id, reset());
                        break;
                    case U:
                        update(op_id, reset());
                        break;
                    case D:
                        delete(op_id, reset());
                        break;
                }
            } catch (ExecutionError | ExecutionException | UncheckedExecutionException e) {
                onNak(op_id, e, HttpStatus.SC_METHOD_FAILURE);
            }
            onEnd(op_id);
            return this;
        }

    } //~ Out

}
