package zest.io.aws;

import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import org.jetbrains.annotations.NotNull;
import software.amazon.awssdk.auth.credentials.*;
import zest.io.Service;
import zest.util.Text;
import zest.util.URL;

import java.util.Map;

public class CredentialsProvider {

    @NotNull
    public static AwsCredentialsProvider of(
            @NotNull final URL url) {
        final Map<String, String> queryMap = url.getQueryAsMap();
        final String aws_access_key_id = queryMap.get(Service.Arg.aws_access_key_id.name());
        if (Text.isFat(aws_access_key_id)) {
            final String aws_secret_access_key = queryMap.get(Service.Arg.aws_secret_access_key.name());
            if (Text.isFat(aws_secret_access_key)) {
                return StaticCredentialsProvider.create(
                        AwsBasicCredentials.create(aws_access_key_id, aws_secret_access_key));
            }
            throw new IllegalArgumentException(String.format(
                    "url = %s misses 'aws_secret_access_key' in query part", url));
        }
        if (Text.isFat(queryMap.get(Service.Arg.aws_secret_access_key.name()))) {
            throw new IllegalArgumentException(String.format(
                    "url = %s misses 'aws_secret_access_key' in query part", url));
        }
        return DefaultCredentialsProvider.create();
    }

    @NotNull
    public static AWSCredentialsProvider AWS(
            @NotNull final AwsCredentialsProvider awsCredentialsProvider) {
        final AwsCredentials awsCredentials = awsCredentialsProvider.resolveCredentials();
        return new AWSStaticCredentialsProvider(
                new BasicAWSCredentials(awsCredentials.accessKeyId(), awsCredentials.secretAccessKey()));
    }


}
