package zest.io;

import org.jetbrains.annotations.NotNull;
import zest.data.ZSet;

public interface Subscriber {

    void onBeg(
            @NotNull final String op_id);

    void onEnd(
            @NotNull final String op_id);

    void onAck(
            @NotNull final String op_id,
            @NotNull final ZSet zset,
            @NotNull final int status);

    void onNak(
            @NotNull final String op_id,
            @NotNull final Throwable throwable,
            @NotNull final int status);


}
