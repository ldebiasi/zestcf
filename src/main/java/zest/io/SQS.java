package zest.io;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.RemovalListener;
import com.google.common.util.concurrent.ExecutionError;
import com.google.common.util.concurrent.UncheckedExecutionException;
import com.google.gson.JsonSyntaxException;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import software.amazon.awssdk.auth.credentials.AwsCredentialsProvider;
import software.amazon.awssdk.awscore.exception.AwsServiceException;
import software.amazon.awssdk.core.exception.SdkClientException;
import software.amazon.awssdk.core.exception.SdkException;
import software.amazon.awssdk.http.urlconnection.UrlConnectionHttpClient;
import software.amazon.awssdk.services.sqs.SqsClient;
import software.amazon.awssdk.services.sqs.model.UnsupportedOperationException;
import software.amazon.awssdk.services.sqs.model.*;
import zest.data.Codec;
import zest.data.FQN;
import zest.data.Op;
import zest.data.ZSet;
import zest.io.aws.CredentialsProvider;
import zest.util.HttpStatus;
import zest.util.URL;

import java.io.IOException;
import java.net.URI;
import java.util.Deque;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;


public class SQS extends Service {

    protected final static transient String TAG_MESSAGE_ID = "message_id";

    protected final static transient String TAG_QUEUE_URL = "queue_url";

    protected final static transient String TAG_RECEIPT_HANDLE = "receipt_handle";

    protected final static transient String TAG_SEQUENCE_NUMBER = "sequence_number";

    protected final static transient String TAG_SEQUENCE_SIZE = "sequence_size";

    protected SQS(
            @NotNull final URL url) {
        super(url);
    }

    @NotNull
    private static String getQueueUrl(
            @NotNull final SqsClient sqsClient,
            @NotNull final FQN fqn)
            throws
            SdkClientException,
            AwsServiceException {
        final String queueName = fqn.toAWSName();
        final GetQueueUrlRequest req = GetQueueUrlRequest.builder().queueName(queueName).build();
        return sqsClient.getQueueUrl(req).queueUrl();
    }


    /**
     * {@inheritDoc}
     *
     * @param subscriber to notify the operations on this new input channel.
     * @return {@inheritDoc}
     */
    @NotNull
    @Override
    public In in(
            @NotNull final Subscriber subscriber) {
        return new In(this, subscriber);
    }

    /**
     * {@inheritDoc}
     *
     * @param subscriber to notify the operations on this new output channel.
     * @return {@inheritDoc}
     */
    @NotNull
    @Override
    public Out out(
            @NotNull final Subscriber subscriber) {
        return new Out(this, subscriber);
    }

    public static class In extends Service.In {

        /**
         * Logger.
         */
        protected final static transient Logger LOG = LoggerFactory.getLogger(In.class);

        /**
         * {@link #sqsClient} is shared for in channels having the same URL.
         */
        protected final static transient Cache<String, SqsClient> SQS_CLIENT_CACHE = CacheBuilder
                .newBuilder()
                .removalListener((RemovalListener<String, SqsClient>) notification -> {
                    final SqsClient sqsClient = notification.getValue();
                    sqsClient.close();
                })
                .build();

        protected final transient Codec codec = Codec.DEFAULT;

        /**
         * Executor service.
         */
        private final transient ScheduledExecutorService executorService = Executors.newScheduledThreadPool(1);

        /**
         * Polling period.
         */
        private final long period;

        /**
         * The AWS SQS client.
         */
        @NotNull
        protected final SqsClient sqsClient;

        /**
         * {@inheritDoc}
         *
         * @param service    on this channel.
         * @param subscriber to notify the events read from this channel.
         * @throws IllegalStateException if this channel can't start.
         */
        public In(
                @NotNull final Service service,
                @NotNull final Subscriber subscriber) {
            super(service, subscriber);
            try {
                final AwsCredentialsProvider awsCredentialsProvider = CredentialsProvider.of(service.url);
                sqsClient = SQS_CLIENT_CACHE.get(service.url.toURI(), () -> SqsClient
                        .builder()
                        .httpClientBuilder(UrlConnectionHttpClient.builder())
                        .credentialsProvider(awsCredentialsProvider)
                        .endpointOverride(URI.create(service.url.getOuterSSP()))
                        .build());
                period = Long.parseUnsignedLong(service.url
                        .getQueryAsMap()
                        .getOrDefault(Arg.period.name(), Arg.period.byDefault));
                LOG.debug("{} open.", service.url.toURI());
            } catch (ExecutionException | IllegalArgumentException e) {
                throw new IllegalStateException(String.format("url = %s can't start", service.url.toURI()), e);
            }
        }

        /**
         * {@inheritDoc}
         * <p></p>
         * Stop the polling {@link #executorService}.
         *
         * @throws IOException {@inheritDoc}
         */
        @Override
        public void close() throws IOException {
            try {
                executorService.shutdown();
            } catch (SecurityException e) {
                throw new IOException(e);
            } finally {
                LOG.debug("{} closed.", service.url.toURI());
            }
        }

        /**
         * {@inheritDoc}
         *
         * @return {@inheritDoc}
         */
        @Override
        public long getPeriod() {
            return period;
        }

        /**
         * {@inheritDoc}
         *
         * @return {@inheritDoc}
         */
        @NotNull
        @Override
        public SQS getService() {
            return (SQS) service;
        }

        @Override
        @NotNull
        public In read(
                @NotNull final ZSet zset)
                throws IOException {
            try {
                final Op op = Op.R;
                final String queueUrl = getQueueUrl(sqsClient, zset.fqn);
                final ReceiveMessageRequest req = ReceiveMessageRequest.builder().queueUrl(queueUrl).build();
                executorService.scheduleAtFixedRate(() -> {
                    final String op_id = op.id(service, zset.fqn, zset.id).toURI();
                    onBeg(op_id);
                    try {
                        final ReceiveMessageResponse res = sqsClient.receiveMessage(req);
                        final List<Message> messageList = res.messages();
                        {
                            final ZSet ack = ZSet.fqn(zset.fqn)
                                    .put(TAG_ACK, codec.encode(res.responseMetadata()).set())
                                    .put(TAG_QUEUE_URL, codec.encode(queueUrl).set())
                                    .put(TAG_SEQUENCE_SIZE, codec.encode(messageList.size()).set())
                                    .set();
                            onAck(op_id, ack, HttpStatus.SC_OK);
                        }
                        int sequence_number = 0;
                        for (final Message message : messageList) {
                            final String body = message.body();
                            final ZSet load = codec.gson.fromJson(body, ZSet.class);
                            final ZSet ack = ZSet.fqn(load.fqn)
                                    .put(TAG_MESSAGE_ID, codec.encode(message.messageId()).set())
                                    .put(TAG_SEQUENCE_NUMBER, codec.encode(sequence_number++).set())
                                    .put(TAG_RECEIPT_HANDLE, codec.encode(message.receiptHandle()).set())
                                    .put(load)
                                    .set(load.id);
                            onAck(op_id, ack, HttpStatus.SC_OK);
                        }
                    } catch (OverLimitException e) {
                        onNak(op_id, e, HttpStatus.SC_FORBIDDEN);
                    } catch (AwsServiceException e) {
                        onNak(op_id, e, HttpStatus.SC_SERVICE_UNAVAILABLE);
                    } catch (SdkClientException e) {
                        onNak(op_id, e, HttpStatus.SC_INTERNAL_SERVER_ERROR);
                    }
                    onEnd(op_id);
                }, 0, period, TimeUnit.SECONDS);
            } catch (SdkClientException e) {
                throw new IOException(e);
            }
            return this;
        }

    } //~ In

    /**
     * {@inheritDoc}
     */
    public static class Out extends Service.Out {

        /**
         * Logger.
         */
        protected final static transient Logger LOG = LoggerFactory.getLogger(Out.class);

        /**
         * {@link #sqsClient} is shared for out channels having the same URL.
         */
        protected final static transient Cache<String, SqsClient> SQS_CLIENT_CACHE = CacheBuilder
                .newBuilder()
                .removalListener((RemovalListener<String, SqsClient>) notification -> {
                    final SqsClient sqsClient = notification.getValue();
                    sqsClient.close();
                })
                .build();

        protected final transient Codec codec = Codec.DEFAULT;

        /**
         * The AWS SQS client.
         */
        @NotNull
        protected final SqsClient sqsClient;

        /**
         * {@inheritDoc}
         *
         * @param service    on this channel.
         * @param subscriber to notify the operations written to this channel.
         * @throws IllegalStateException if this channel can't start.
         */
        public Out(
                @NotNull final Service service,
                @NotNull final Subscriber subscriber)
                throws IllegalStateException {
            super(service, subscriber);
            try {
                final AwsCredentialsProvider awsCredentialsProvider = CredentialsProvider.of(service.url);
                sqsClient = SQS_CLIENT_CACHE.get(service.url.toURI(), () -> SqsClient
                        .builder()
                        .httpClientBuilder(UrlConnectionHttpClient.builder())
                        .credentialsProvider(awsCredentialsProvider)
                        .endpointOverride(URI.create(service.url.getOuterSSP()))
                        .build());
                LOG.debug("{} open.", service.url.toURI());
            } catch (ExecutionException | IllegalArgumentException e) {
                throw new IllegalStateException(String.format("url = %s can't start", service.url.toURI()), e);
            }
        }

        /**
         * {@inheritDoc}
         *
         * @throws IOException
         */
        @Override
        public void close()
                throws IOException {
            LOG.debug("{} closed.", service.url.toURI());
        }

        private void create(
                @NotNull final String op_id,
                @NotNull final Deque<ZSet> stack) {
            // todo add predicate processor
            final ZSet zset = stack.removeLast();
            try {
                String queueUrl;
                try {
                    queueUrl = getQueueUrl(zset.fqn);
                } catch (QueueDoesNotExistException e) {
                    queueUrl = creatQueue(op_id, zset.fqn);
                }
                createMessage(op_id, zset, queueUrl);
            } catch (QueueDeletedRecentlyException e) {
                onNak(op_id, e, HttpStatus.SC_CONFLICT);
            } catch (QueueNameExistsException e) {
                onNak(op_id, e, HttpStatus.SC_FORBIDDEN);
            } catch (AwsServiceException e) {
                onNak(op_id, e, HttpStatus.SC_SERVICE_UNAVAILABLE);
            } catch (SdkException e) {
                onNak(op_id, e, HttpStatus.SC_INTERNAL_SERVER_ERROR);
            }
        }

        private boolean createMessage(
                @NotNull final String op_id,
                @NotNull final ZSet zset,
                @NotNull final String queueUrl) {
            final String body = zset.toJSON().toString();
            final SendMessageRequest req = SendMessageRequest.builder().queueUrl(queueUrl).messageBody(body).build();
            try {
                final SendMessageResponse res = sqsClient.sendMessage(req);
                final ZSet ack = ZSet.fqn(zset.fqn)
                        .put(TAG_MESSAGE_ID, codec.encode(res.messageId()).set())
                        .put(TAG_SEQUENCE_NUMBER, codec.encode(res.sequenceNumber()).set())
                        .put(TAG_ACK, codec.encode(res.responseMetadata()).set())
                        .set(zset.id);
                onAck(op_id, ack, HttpStatus.SC_CREATED);
            } catch (InvalidMessageContentsException e) {
                onNak(op_id, e, HttpStatus.SC_BAD_REQUEST);
            } catch (UnsupportedOperationException e) {
                onNak(op_id, e, HttpStatus.SC_METHOD_NOT_ALLOWED);
            } catch (AwsServiceException e) {
                onNak(op_id, e, HttpStatus.SC_SERVICE_UNAVAILABLE);
            } catch (SdkClientException e) {
                onNak(op_id, e, HttpStatus.SC_INTERNAL_SERVER_ERROR);
            }
            return false;
        }

        @NotNull
        private String creatQueue(
                @NotNull final String op_id,
                @NotNull final FQN fqn)
                throws
                AwsServiceException,
                SdkClientException {
            final String queueName = fqn.toAWSName();
            final CreateQueueRequest req = CreateQueueRequest.builder().queueName(queueName).build();
            final CreateQueueResponse res = sqsClient.createQueue(req);
            final ZSet ack = ZSet.fqn(fqn)
                    .put(TAG_QUEUE_URL, codec.encode(res.queueUrl()).set())
                    .put(TAG_ACK, codec.encode(res.responseMetadata()).set())
                    .set();
            onAck(op_id, ack, HttpStatus.SC_CREATED);
            return res.queueUrl();
        }

        private void delete(
                @NotNull final String op_id,
                @NotNull final Deque<ZSet> stack) {
            final ZSet zset = stack.removeLast();
            try {
                final String queueUrl = getQueueUrl(zset.fqn);
                if (zset.isIdentifiable()) {
                    if (zset.isMember(TAG_RECEIPT_HANDLE)) {
                        final String receiptHandle = (String) codec.decode(zset.getElement(TAG_RECEIPT_HANDLE));
                        final DeleteMessageRequest req = DeleteMessageRequest.builder()
                                .queueUrl(queueUrl)
                                .receiptHandle(receiptHandle)
                                .build();
                        try {
                            final DeleteMessageResponse res = sqsClient.deleteMessage(req);
                            final ZSet ack = ZSet.fqn(zset.fqn)
                                    .put(TAG_ACK, codec.encode(res.responseMetadata()).set())
                                    .set(zset.id);
                            onAck(op_id, ack, HttpStatus.SC_OK);
                        } catch (InvalidIdFormatException e) {
                            onNak(op_id, e, HttpStatus.SC_UNPROCESSABLE_ENTITY);
                        } catch (NoSuchElementException | ReceiptHandleIsInvalidException e) {
                            onNak(op_id, e, HttpStatus.SC_BAD_REQUEST);
                        } catch (AwsServiceException e) {
                            onNak(op_id, e, HttpStatus.SC_SERVICE_UNAVAILABLE);
                        } catch (SdkClientException e) {
                            onNak(op_id, e, HttpStatus.SC_INTERNAL_SERVER_ERROR);
                        }
                    }
                } else {
                    final DeleteQueueRequest req = DeleteQueueRequest.builder().queueUrl(queueUrl).build();
                    try {
                        final DeleteQueueResponse res = sqsClient.deleteQueue(req);
                        final ZSet ack = ZSet.fqn(zset.fqn)
                                .put(TAG_ACK, codec.encode(res.responseMetadata()).set())
                                .set(zset.id);
                        onAck(op_id, ack, HttpStatus.SC_OK);
                    } catch (AwsServiceException e) {
                        onNak(op_id, e, HttpStatus.SC_SERVICE_UNAVAILABLE);
                    } catch (SdkClientException e) {
                        onNak(op_id, e, HttpStatus.SC_INTERNAL_SERVER_ERROR);
                    }
                }
            } catch (QueueDoesNotExistException e) {
                onNak(op_id, e, HttpStatus.SC_NOT_FOUND);
            }
        }

        /**
         * {@inheritDoc}
         *
         * @return the {@link Op} set supported by this channel.
         */
        @NotNull
        @Override
        public SQS getService() {
            return (SQS) service;
        }

        /**
         * @param fqn
         * @return
         * @throws QueueDoesNotExistException
         * @throws SdkClientException
         * @throws SqsException
         * @throws AwsServiceException
         */
        @NotNull
        private String getQueueUrl(
                @NotNull final FQN fqn)
                throws QueueDoesNotExistException, SdkClientException, SqsException, AwsServiceException {
            final String queueName = fqn.toAWSName();
            final GetQueueUrlRequest req = GetQueueUrlRequest.builder().queueName(queueName).build();
            return sqsClient.getQueueUrl(req).queueUrl();
        }

        private void read(
                @NotNull final String op_id,
                @NotNull final Deque<ZSet> stack) {
            final ZSet zset = stack.removeLast();
            try {
                final String queueUrl = getQueueUrl(zset.fqn);
                final ReceiveMessageRequest req = ReceiveMessageRequest.builder().queueUrl(queueUrl).build();
                try {
                    final ReceiveMessageResponse res = sqsClient.receiveMessage(req);
                    final List<Message> messageList = res.messages();
                    {
                        final ZSet ack = ZSet.fqn(zset.fqn)
                                .put(TAG_ACK, codec.encode(res.responseMetadata()).set())
                                .put(TAG_QUEUE_URL, codec.encode(queueUrl).set())
                                .put(TAG_SEQUENCE_SIZE, codec.encode(Integer.valueOf(messageList.size())).set())
                                .set();
                        onAck(op_id, ack, HttpStatus.SC_OK);
                    }
                    int sequence_number = 0;
                    for (final Message message : messageList) {
                        final String body = message.body();
                        final ZSet load = codec.gson.fromJson(body, ZSet.class);
                        final ZSet ack = ZSet.fqn(load.fqn)
                                .put(load)
                                .put(TAG_MESSAGE_ID, codec.encode(message.messageId()).set())
                                .put(TAG_SEQUENCE_NUMBER, codec.encode(sequence_number++).set())
                                .put(TAG_RECEIPT_HANDLE, codec.encode(message.receiptHandle()).set())
                                .set(load.id);
                        onAck(op_id, ack, HttpStatus.SC_OK);
                    }
                } catch (JsonSyntaxException e) {
                    onNak(op_id, e, HttpStatus.SC_UNPROCESSABLE_ENTITY);
                } catch (OverLimitException e) {
                    onNak(op_id, e, HttpStatus.SC_INSUFFICIENT_SPACE_ON_RESOURCE);
                } catch (AwsServiceException e) {
                    onNak(op_id, e, HttpStatus.SC_SERVICE_UNAVAILABLE);
                } catch (SdkClientException e) {
                    onNak(op_id, e, HttpStatus.SC_INTERNAL_SERVER_ERROR);
                }
            } catch (QueueDoesNotExistException e) {
                onNak(op_id, e, HttpStatus.SC_NOT_FOUND);
            }
        }

        private void update(
                @NotNull final String op_id,
                @NotNull final Deque<ZSet> stack) {
            final ZSet zset = stack.removeLast();
            try {
                final String queueUrl = getQueueUrl(zset.fqn);
                createMessage(op_id, zset, queueUrl);
            } catch (QueueDoesNotExistException e) {
                onNak(op_id, e, HttpStatus.SC_NOT_FOUND);
            }
        }

        /**
         * {@inheritDoc}
         *
         * @param op_id {@inheritDoc}
         * @param zset  {@inheritDoc}
         * @param op    {@inheritDoc}
         * @return {@inheritDoc}
         */
        @NotNull
        @Override
        public Service.Out write(
                @NotNull final String op_id,
                @NotNull final ZSet zset,
                @NotNull final Op op) {
            onBeg(op_id);
            try {
                push(zset);
                switch (op) {
                    case C:
                        create(op_id, reset());
                        break;
                    case R:
                        read(op_id, reset());
                        break;
                    case U:
                        update(op_id, reset());
                        break;
                    case D:
                        delete(op_id, reset());
                        break;
                }
            } catch (ExecutionError | ExecutionException | UncheckedExecutionException e) {
                onNak(op_id, e, HttpStatus.SC_METHOD_FAILURE);
            }
            onEnd(op_id);
            return this;
        }

    } //~ Out


}
