package zest.io;

import com.amazonaws.services.kinesis.producer.KinesisProducer;
import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.RemovalListener;
import com.google.common.util.concurrent.ExecutionError;
import com.google.common.util.concurrent.UncheckedExecutionException;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import software.amazon.awssdk.auth.credentials.AwsCredentialsProvider;
import software.amazon.awssdk.core.SdkBytes;
import software.amazon.awssdk.http.Protocol;
import software.amazon.awssdk.http.async.SdkAsyncHttpClient;
import software.amazon.awssdk.http.nio.netty.NettyNioAsyncHttpClient;
import software.amazon.awssdk.regions.Region;
import software.amazon.awssdk.services.kinesis.KinesisAsyncClient;
import software.amazon.awssdk.services.kinesis.KinesisAsyncClientBuilder;
import software.amazon.awssdk.services.kinesis.model.*;
import zest.data.Codec;
import zest.data.Element;
import zest.data.Op;
import zest.data.ZSet;
import zest.io.aws.CredentialsProvider;
import zest.util.HttpStatus;
import zest.util.Text;
import zest.util.URL;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.Deque;
import java.util.concurrent.*;

public class Kinesis extends Service {

    /**
     * Sequence number returned by AWS Kinesis for the message created.
     */
    protected final static transient String TAG_SEQUENCE_NUMBER = "sequence_number";

    /**
     * The real Kinesis stream name the channel is connected with.
     */
    protected final static transient String TAG_STREAM_NAME = "stream_name";

    /**
     * {@inheritDoc}
     *
     * @param url {@inheritDoc}
     */
    public Kinesis(
            @NotNull final URL url) {
        super(url);
    }

    /**
     * {@inheritDoc}
     *
     * @param subscriber to notify the operations on this new output channel.
     * @return {@inheritDoc}
     */
    @Override
    @NotNull
    public In in(
            @NotNull final Subscriber subscriber) {
        return new In(this, subscriber);
    }

    /**
     * {@inheritDoc}
     *
     * @param subscriber to notify the operations on this new input channel.
     * @return {@inheritDoc}
     */
    @Override
    @NotNull
    public Out out(
            @NotNull final Subscriber subscriber) {
        return new Out(this, subscriber);
    }

    /**
     * {@inheritDoc}
     */
    public static class In extends Service.In {

        /**
         * Logger.
         */
        protected final static transient Logger LOG = LoggerFactory.getLogger(In.class);

        /**
         * {@link #kinesisClient} is shared for in channels having the same URL.
         */
        protected final static transient Cache<String, KinesisAsyncClient> KINESIS_CLIENT_CACHE = CacheBuilder
                .newBuilder()
                .removalListener((RemovalListener<String, KinesisAsyncClient>) notification -> {
                    final KinesisAsyncClient kinesisClient = notification.getValue();
                    kinesisClient.close();
                })
                .build();

        /**
         * Codec used to serialize and deserialize {@link ZSet} instances to and from this channel.
         */
        protected final transient Codec codec = Codec.DEFAULT;

        /**
         * Executor service.
         */
        private final transient ScheduledExecutorService executorService = Executors.newScheduledThreadPool(1);

        /**
         * The Kinesis client is asynchronous and based on AWS SDK 2.
         */
        protected final transient KinesisAsyncClient kinesisClient;

        /**
         * Polling period.
         */
        private final long period;

        /**
         * {@inheritDoc}
         *
         * @param service    on this channel.
         * @param subscriber to notify the events read from this channel.
         * @throws IllegalStateException if this channel can't start.
         */
        public In(
                @NotNull final Service service,
                @NotNull final Subscriber subscriber)
                throws
                IllegalStateException {
            super(service, subscriber);
            try {
                final URL url = URL.at(service.url.getOuterSSP());
                final java.net.URI endpoint = new java.net.URI(url.scheme, null, url.host, url.port, null, null, null);
                final AwsCredentialsProvider awsCredentialsProvider = CredentialsProvider.of(service.url);
                final String aws_region = url.getQueryAsMap().get(Arg.aws_region.name());
                final SdkAsyncHttpClient httpClient = NettyNioAsyncHttpClient.builder().protocol(Protocol.HTTP1_1).build();
                kinesisClient = KINESIS_CLIENT_CACHE.get(service.url.toURI(), () -> {
                    final KinesisAsyncClientBuilder kinesisAsyncClientBuilder = KinesisAsyncClient
                            .builder()
                            .httpClient(httpClient)
                            .credentialsProvider(awsCredentialsProvider)
                            .endpointOverride(endpoint);
                    if (Text.isFat(aws_region)) {
                        kinesisAsyncClientBuilder.region(Region.of(aws_region));
                    }
                    return kinesisAsyncClientBuilder.build();
                });
                period = Long.parseUnsignedLong(service.url
                        .getQueryAsMap()
                        .getOrDefault(Arg.period.name(), Arg.period.byDefault));
                LOG.debug("{} open.", service.url.toURI());
            } catch (ExecutionException | IllegalArgumentException | java.net.URISyntaxException e) {
                throw new IllegalStateException(String.format("url = %s can't start", service.url.toURI()), e);
            }
        }

        /**
         * {@inheritDoc}
         * <p></p>
         * Stop the polling {@link #executorService}.
         *
         * @throws IOException {@inheritDoc}
         */
        @Override
        public void close() throws IOException {
            try {
                executorService.shutdown();
            } catch (Exception e) {
                throw new IOException(e);
            } finally {
                LOG.debug("{} closed.", service.url.toURI());
            }
        }

        /**
         * {@inheritDoc}
         *
         * @return {@inheritDoc}
         */
        @Override
        public long getPeriod() {
            return period;
        }

        /**
         * {@inheritDoc}
         *
         * @return {@inheritDoc}
         */
        @NotNull
        @Override
        public Kinesis getService() {
            return (Kinesis) service;
        }

        /**
         * https://docs.aws.amazon.com/streams/latest/dev/kcl2-standard-consumer-java-example.html
         *
         * @param zset operand.
         * @return {@inheritDoc}
         * @throws IOException
         */
        @Override
        @NotNull
        public In read(
                @NotNull final ZSet zset)
                throws IOException {
            final Op op = Op.R;
            final String stream = zset.fqn.toAWSName();
            executorService.scheduleAtFixedRate(() -> {
                final String op_id = op.id(service, zset.fqn, zset.id).toURI();
                onBeg(op_id);
                try {
                    final ListShardsRequest listShardsReq = ListShardsRequest.builder().streamName(stream).build();
                    final ListShardsResponse listShardsRes = kinesisClient.listShards(listShardsReq).get();
                    for (final Shard shard : listShardsRes.shards()) {
                        final GetShardIteratorRequest shardIteratorReq = GetShardIteratorRequest.builder()
                                .streamName(stream)
                                .shardId(shard.shardId())
                                .shardIteratorType(ShardIteratorType.TRIM_HORIZON) // todo: configurable
                                .build();
                        String nextShardIter = kinesisClient.getShardIterator(shardIteratorReq).get().shardIterator();
                        while (nextShardIter != null) {
                            final GetRecordsRequest req = GetRecordsRequest.builder()
                                    .shardIterator(nextShardIter)
                                    .build();
                            final GetRecordsResponse res = kinesisClient.getRecords(req).get();
                            final Element metadata = codec.encode(res.responseMetadata()).set();
                            for (final Record record : res.records()) {
                                final String body = new String(record.data().asByteArray());
                                final ZSet load = codec.decode(body);
                                final ZSet ack = ZSet.fqn(load.fqn)
                                        .put(load)
                                        .put(TAG_ACK, metadata)
                                        .set(load.id);
                                onAck(op_id, ack, HttpStatus.SC_OK);
                            }
                            nextShardIter = res.millisBehindLatest() > 0 ? res.nextShardIterator() : null;
                        }
                    }
                } catch (CompletionException e) {
                    onNak(op_id, e, HttpStatus.SC_SERVICE_UNAVAILABLE);
                } catch (ExecutionException | InterruptedException e) {
                    onNak(op_id, e, HttpStatus.SC_METHOD_FAILURE);
                }
                onEnd(op_id);
            }, 0, period, TimeUnit.SECONDS);
            return this;
        }

    } //~ In

    /**
     * {@inheritDoc}
     */
    public static class Out extends Service.Out {

        /**
         * Logger.
         */
        protected final static transient Logger LOG = LoggerFactory.getLogger(Out.class);

        /**
         * {@link #kinesisClient} is shared for out channels having the same URL.
         */
        protected final static transient Cache<String, KinesisAsyncClient> KINESIS_CLIENT_CACHE = CacheBuilder
                .newBuilder()
                .removalListener((RemovalListener<String, KinesisAsyncClient>) notification -> {
                    final KinesisAsyncClient kinesisClient = notification.getValue();
                    kinesisClient.close();
                })
                .build();

        /**
         * Codec used to wrap datum in {@link Element}.
         */
        protected final transient Codec codec = Codec.DEFAULT;

        /**
         * {@link #kinesisClient} is shared for in channels having the same URL.
         */
        protected final transient KinesisAsyncClient kinesisClient;

        protected final Integer shards;

        /**
         * Return a new output channel for this {@code service} with at least one {@code subscriber}.
         *
         * @param service    of this output channel.
         * @param subscriber of operations sent to this channel.
         * @throws IllegalStateException is the service can't start.
         */
        public Out(
                @NotNull final Service service,
                @NotNull final Subscriber subscriber)
                throws IllegalStateException {
            super(service, subscriber);
            try {
                final URL url = URL.at(service.url.getOuterSSP());
                final java.net.URI endpoint = new java.net.URI(url.scheme, null, url.host, url.port, null, null, null);
                final AwsCredentialsProvider awsCredentialsProvider = CredentialsProvider.of(service.url);
                final String aws_region = url.getQueryAsMap().get(Arg.aws_region.name());
                final SdkAsyncHttpClient httpClient = NettyNioAsyncHttpClient.builder().protocol(Protocol.HTTP1_1).build();
                shards = Integer.valueOf(url.getQueryAsMap().getOrDefault(Arg.aws_kinesis_shards.name(), Arg.aws_kinesis_shards.byDefault));
                kinesisClient = KINESIS_CLIENT_CACHE.get(service.url.toURI(), () -> {
                    final KinesisAsyncClientBuilder kinesisAsyncClientBuilder = KinesisAsyncClient
                            .builder()
                            .httpClient(httpClient)
                            .credentialsProvider(awsCredentialsProvider)
                            .endpointOverride(endpoint);
                    if (Text.isFat(aws_region)) {
                        kinesisAsyncClientBuilder.region(Region.of(aws_region));
                    }
                    return kinesisAsyncClientBuilder.build();
                });
                LOG.debug("{} open.", service.url.toURI());
            } catch (ExecutionException | IllegalArgumentException | java.net.URISyntaxException e) {
                throw new IllegalStateException(String.format("url = %s can't start", service.url.toURI()), e);
            }
        }

        /**
         * {@inheritDoc}
         *
         * @throws IOException {@inheritDoc}
         */
        @Override
        public void close() throws IOException {
            LOG.debug("{} closed.", service.url.toURI());
        }

        private void create(
                @NotNull final String op_id,
                @NotNull final Deque<ZSet> stack)
                throws
                ExecutionException,
                InterruptedException {
            // todo add predicate processor
            final ZSet zset = stack.removeLast();
            final String stream = zset.fqn.toAWSName();
            try {
                try {
                    getStream(stream);
                } catch (IOException e) {
                    createStream(op_id, zset, stream);
                }
                createRecord(op_id, zset, stream);
            } catch (CompletionException e) {
                onNak(op_id, e, HttpStatus.SC_SERVICE_UNAVAILABLE);
            }
        }

        private void createRecord(
                final String op_id,
                final ZSet zset,
                final String stream)
                throws
                CompletionException,
                ExecutionException,
                InterruptedException {
            final String body = zset.toJSON().toString();
            final SdkBytes bytes = SdkBytes.fromString(body, Charset.defaultCharset());
            final PutRecordRequest req = PutRecordRequest.builder()
                    .partitionKey(zset.id)
                    .streamName(stream)
                    .data(bytes)
                    .build();
            final PutRecordResponse res = kinesisClient.putRecord(req).get();
            final ZSet ack = ZSet.fqn(zset.fqn)
                    .put(TAG_ACK, codec.encode(res.responseMetadata()).set())
                    .put(TAG_SEQUENCE_NUMBER, codec.encode(res.sequenceNumber()).set())
                    .set(zset.id);
            onAck(op_id, ack, HttpStatus.SC_CREATED);
        }

        @NotNull
        private void createStream(
                @NotNull final String op_id,
                @NotNull final ZSet zset,
                @NotNull final String stream)
                throws
                CompletionException,
                ExecutionException,
                InterruptedException {
            final CreateStreamRequest req = CreateStreamRequest.builder()
                    .shardCount(shards)
                    .streamName(stream)
                    .build();
            final CompletableFuture<CreateStreamResponse> promise = kinesisClient.createStream(req);
            final CreateStreamResponse res = promise.get();
            final ZSet ack = ZSet.fqn(zset.fqn)
                    .put(TAG_ACK, codec.encode(res.responseMetadata()).set())
                    .put(TAG_STREAM_NAME, codec.encode(stream).set())
                    .set(zset.id);
            onAck(op_id, ack, HttpStatus.SC_CREATED);
        }


        private void delete(
                @NotNull final String op_id,
                @NotNull final Deque<ZSet> stack)
                throws
                CompletionException,
                ExecutionException,
                InterruptedException {
            final ZSet zset = stack.removeLast();
            onBeg(op_id);
            final String stream = zset.fqn.toAWSName();
            final DeleteStreamRequest req = DeleteStreamRequest.builder().streamName(stream).build();
            final DeleteStreamResponse res = kinesisClient.deleteStream(req).get();
            final ZSet ack = ZSet.fqn(zset.fqn)
                    .put(TAG_STREAM_NAME, codec.encode(stream).set())
                    .put(TAG_ACK, codec.encode(res.responseMetadata()).set())
                    .set();
            onAck(op_id, ack, HttpStatus.SC_OK);
        }

        /**
         * {@inheritDoc}
         *
         * @return {@inheritDoc}
         */
        @Override
        @NotNull
        public Kinesis getService() {
            return (Kinesis) service;
        }

        /**
         * @param stream
         * @return the {@code stream} if exists.
         * @see <h href="https://www.callicoder.com/java-8-completablefuture-tutorial/">Completable Future</h>
         */
        @NotNull
        public String getStream(
                @NotNull final String stream)
                throws
                CompletionException,
                ExecutionException,
                InterruptedException,
                IOException {
            final ListStreamsRequest req = ListStreamsRequest.builder().build();
            final CompletableFuture<ListStreamsResponse> promise = kinesisClient.listStreams(req);
            final ListStreamsResponse res = promise.get();
            if (res.streamNames().contains(stream)) {
                return stream;
            }
            throw new IOException(String.format("stream = %s not found", stream));
        }

        private void read(
                @NotNull final String op_id,
                @NotNull final Deque<ZSet> stack)
                throws ExecutionException,
                InterruptedException {
            final ZSet zset = stack.removeLast();
            final String stream = zset.fqn.toAWSName();
            try {
                final ListShardsRequest listShardsReq = ListShardsRequest.builder()
                        .streamName(stream)
                        .build();
                final ListShardsResponse listShardsRes = kinesisClient.listShards(listShardsReq).get();
                for (final Shard shard : listShardsRes.shards()) {
                    final GetShardIteratorRequest shardIteratorReq = GetShardIteratorRequest.builder()
                            .streamName(stream)
                            .shardId(shard.shardId())
                            .shardIteratorType(ShardIteratorType.TRIM_HORIZON) // todo: configurable
                            .build();
                    String nextShardIter = kinesisClient.getShardIterator(shardIteratorReq).get().shardIterator();
                    while (nextShardIter != null) {
                        final GetRecordsRequest req = GetRecordsRequest.builder()
                                .shardIterator(nextShardIter)
                                .build();
                        final GetRecordsResponse res = kinesisClient.getRecords(req).get();
                        final Element metadata = codec.encode(res.responseMetadata()).set();
                        for (final Record record : res.records()) {
                            final String body = new String(record.data().asByteArray());
                            final ZSet load = codec.decode(body);
                            final ZSet ack = ZSet.fqn(load.fqn)
                                    .put(load)
                                    .put(TAG_ACK, metadata)
                                    .set(load.id);
                            onAck(op_id, ack, HttpStatus.SC_OK);
                        }
                        nextShardIter = res.millisBehindLatest() > 0 ? res.nextShardIterator() : null;
                    }
                }
            } catch (CompletionException e) {
                onNak(op_id, e, HttpStatus.SC_SERVICE_UNAVAILABLE);
            }
        }

        private void update(
                @NotNull final String op_id,
                @NotNull final Deque<ZSet> stack)
                throws
                ExecutionException,
                InterruptedException {
            final ZSet zset = stack.removeLast();
            onBeg(op_id);
            final String stream = zset.fqn.toAWSName();
            createRecord(op_id, zset, stream);
        }

        /**
         * {@inheritDoc}
         *
         * @param op_id {@inheritDoc}
         * @param zset  {@inheritDoc}
         * @param op    {@inheritDoc}
         * @return {@inheritDoc}
         */
        @NotNull
        @Override
        public Out write(
                @NotNull final String op_id,
                @NotNull final ZSet zset,
                @NotNull final Op op) {
            onBeg(op_id);
            try {
                push(zset);
                switch (op) {
                    case C:
                        create(op_id, reset());
                        break;
                    case R:
                        read(op_id, reset());
                        break;
                    case U:
                        update(op_id, reset());
                        break;
                    case D:
                        delete(op_id, reset());
                        break;
                }
            } catch (ExecutionError | ExecutionException | InterruptedException | UncheckedExecutionException e) {
                onNak(op_id, e, HttpStatus.SC_METHOD_FAILURE);
            }
            onEnd(op_id);
            return this;
        }

    } //~ Out


}
