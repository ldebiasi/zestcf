package zest.nio;

import com.mongodb.client.model.Filters;
import com.mongodb.client.result.DeleteResult;
import com.mongodb.client.result.UpdateResult;
import com.mongodb.reactivestreams.client.*;
import org.bson.Document;
import org.bson.conversions.Bson;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.reactivestreams.Publisher;
import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;
import zest.data.Codec;
import zest.data.Condition;
import zest.data.Element;
import zest.data.ZSet;
import zest.util.URL;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.Flow;
import java.util.concurrent.SubmissionPublisher;

/**
 * Provide the service for MongoDB.
 */
public class Mongo extends Service {

    public static final transient String TAG__ID = "_id";

    /**
     * Create a new MongoDb service connected to database at {@code url} address.
     *
     * @param url address of the MongoDb server.
     */
    public Mongo(
            @NotNull final URL url) {
        super(url);
    }


    protected static @Nullable Bson filterFrom(
            @NotNull final Codec codec,
            @NotNull final ZSet operand
    ) {
        Bson filter = operand.isIdentifiable() ? new Document(TAG__ID, operand.id) : new Document();
        for (final String property : operand.getProperties()) {
            final Element element = operand.getElement(property);
            if (element.isConditional()) {
                final Condition condition = element.condition;
                if (condition instanceof Condition.And) {
                    filter = Filters.and(filter, filterFrom(codec, property, element.type, condition.next));
                } else if (condition instanceof Condition.Or) {
                    filter = Filters.or(filter, filterFrom(codec, property, element.type, condition.next));
                } else {
                    throw new UnsupportedOperationException(
                            String.format("(condition.class = %s) not supported", condition));
                }
            }
        }
        return filter;
    }

    protected static <T> @NotNull Bson filterFrom(
            final @NotNull String property,
            final @NotNull T parameter,
            final @NotNull Condition.Op op)
            throws UnsupportedOperationException {
        switch (op) {
            case BW:
                final String exp = parameter.toString();
                final String regex = exp.startsWith("^") ? exp : String.format("^%s", parameter.toString());
                return Filters.regex(property, regex);
            case MP:
                return Filters.regex(property, parameter.toString());
            case GT:
                return Filters.gt(property, parameter);
            case GTE:
                return Filters.gte(property, parameter);
            case EQ:
                return Filters.eq(property, parameter);
            case LTE:
                return Filters.lte(property, parameter);
            case LT:
                return Filters.lt(property, parameter);
        }
        throw new UnsupportedOperationException(String.format("(op = %s) not supported", op));
    }

    private static <T> @NotNull Bson filterFrom(
            final @NotNull Codec codec,
            final @NotNull String property,
            final @NotNull Class<T> type,
            final @NotNull Condition condition
    )
            throws UnsupportedOperationException {
        final T parameter = codec.gson.fromJson(condition.arg, type);
        Bson filter = filterFrom(property, parameter, condition.op);
        if (!condition.positive) {
            filter = Filters.not(filter);
        }
        if (condition.hasNext()) {
            if (condition instanceof Condition.And) {
                filter = Filters.and(filter, filterFrom(codec, property, type, condition.next));
            } else if (condition instanceof Condition.Or) {
                filter = Filters.or(filter, filterFrom(codec, property, type, condition.next));
            } else {
                throw new UnsupportedOperationException(
                        String.format("(condition.class = %s) not supported", condition));
            }
        }
        return filter;
    }

    /**
     * {@inheritDoc}
     *
     * @return {@inheritDoc}
     */
    @Override
    @NotNull
    public Out out() {
        return new Out(this);
    }

    /**
     * Implement the output channel for MongoDB
     */
    public class Out extends Service.Out {

        /**
         * Codec used to serialize and deserialize {@link ZSet} instances to and from this channel.
         */
        protected final transient Codec codec = Codec.DEFAULT;

        /**
         * Thread safe.
         */
        protected final transient MongoClient mongoClient;

        /**
         * Return a new out channel for MongoDB.
         *
         * @param service MongoDB service.
         */
        protected Out(
                @NotNull final Mongo service) {
            super(service);
            final String url = service.url.port == URL.NO_PORT
                    ? String.format("mongodb://%s", service.url.host)
                    : String.format("mongodb://%s:%d", service.url.host, service.url.port);
            mongoClient = MongoClients.create(url);
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public void close() {
            mongoClient.close();
        }


        /**
         * {@inheritDoc}
         *
         * @return {@inheritDoc}
         */
        @Override
        @NotNull
        public Mongo getService() {
            return (Mongo) service;
        }

        /**
         * {@inheritDoc}
         *
         * @param operand    representing the resource to create.
         * @param subscriber of the operation.
         * @return {@inheritDoc}
         */
        @Override
        @NotNull
        public Out create(
                @NotNull final ZSet operand,
                @NotNull final Flow.Subscriber<ZSet> subscriber) {
            return (Out) super.create(operand, subscriber);
        }

        /**
         * {@inheritDoc}
         *
         * @param operand    should be {@link ZSet#isIdentifiable()} false.
         * @param subscriber of the operation.
         */
        @Override
        protected void create_container(
                @NotNull final ZSet operand,
                @NotNull final Flow.Subscriber<? super ZSet> subscriber) {
            try (final SubmissionPublisher<ZSet> publisher = new SubmissionPublisher<>()) {
                publisher.subscribe(subscriber);
                if (operand.fqn.getSize() > 1) {
                    final String dbName = operand.fqn.getRoot();
                    final MongoDatabase db = mongoClient.getDatabase(dbName);
                    final String collectionName = operand.fqn.getName(1);
                    final Publisher<Success> creation = db.createCollection(collectionName);
                    final CountDownLatch latch = new CountDownLatch(1);
                    creation.subscribe(new Subscriber<>() {

                        private volatile Subscription subscription;

                        @Override
                        public void onSubscribe(Subscription subscription) {
                            this.subscription = subscription;
                            this.subscription.request(1);
                        }

                        @Override
                        public void onNext(Success success) {
                            final zest.nio.meta.Mongo meta = new zest.nio.meta.Mongo(success);
                            final ZSet result = ZSet.fqn(operand.fqn).put(TAG_META, codec.encode(meta).set()).set();
                            publisher.submit(result);
                            this.subscription.request(1);
                        }

                        @Override
                        public void onError(final Throwable t) {
                            publisher.closeExceptionally(t);
                            latch.countDown();
                        }

                        @Override
                        public void onComplete() {
                            publisher.close();
                            latch.countDown();
                        }
                    });
                    try {
                        latch.await();
                    } catch (InterruptedException e) {
                        publisher.closeExceptionally(e);
                    }
                } else {
                    final Exception e = new IllegalArgumentException(String.format(
                            "(operand.fqn = %s) doesn't express <db_name>/<collection_name>",
                            operand.fqn));
                    publisher.closeExceptionally(e);
                }
            }
        }

        /**
         * {@inheritDoc}
         *
         * @param operand    should be {@link ZSet#isIdentifiable()} true.
         * @param subscriber of the operation.
         */
        @Override
        protected void create_resource(
                @NotNull final ZSet operand,
                @NotNull final Flow.Subscriber<ZSet> subscriber) {
            try (final SubmissionPublisher<ZSet> publisher = new SubmissionPublisher<>()) {
                publisher.subscribe(subscriber);
                if (operand.fqn.getSize() > 1) {
                    final String dbName = operand.fqn.getRoot();
                    final MongoDatabase db = mongoClient.getDatabase(dbName);
                    final String collectionName = operand.fqn.getName(1);
                    final MongoCollection<Document> collection = db.getCollection(collectionName);
                    final Document doc = Document.parse(operand.toJSON().toString());
                    final Publisher<Success> insertion = collection.insertOne(doc);
                    final CountDownLatch latch = new CountDownLatch(1);
                    insertion.subscribe(new Subscriber<Success>() {

                        private volatile Subscription subscription;

                        @Override
                        public void onSubscribe(final Subscription subscription) {
                            this.subscription = subscription;
                            this.subscription.request(1);
                        }

                        @Override
                        public void onNext(final Success success) {
                            final zest.nio.meta.Mongo meta = new zest.nio.meta.Mongo(success);
                            final ZSet result = ZSet.fqn(operand.fqn).put(TAG_META, codec.encode(meta).set()).set();
                            publisher.submit(result);
                            this.subscription.request(1);
                        }

                        @Override
                        public void onError(final Throwable t) {
                            publisher.closeExceptionally(t);
                            latch.countDown();
                        }

                        @Override
                        public void onComplete() {
                            publisher.close();
                            latch.countDown();
                        }
                    });
                    try {
                        latch.await();
                    } catch (final InterruptedException e) {
                        publisher.closeExceptionally(e);
                    }
                } else {
                    final Exception e = new IllegalArgumentException(
                            String.format("(operand.fqn = %s) doesn't express <db_name>/<collection_name>", operand.fqn));
                    publisher.closeExceptionally(e);
                }
            }
        }

        /**
         * {@inheritDoc}
         *
         * @param operand    representing the resource to delete.
         * @param subscriber of the operation.
         * @return
         */
        @Override
        public @NotNull Out delete(
                @NotNull final ZSet operand,
                @NotNull final Flow.Subscriber<ZSet> subscriber) {
            return (Out) super.delete(operand, subscriber);
        }

        /**
         * {@inheritDoc}
         *
         * @param operand    should be {@link ZSet#isIdentifiable()} false.
         * @param subscriber of the operation.
         */
        @Override
        protected void delete_container(
                @NotNull final ZSet operand,
                @NotNull final Flow.Subscriber<ZSet> subscriber) {
            try (final SubmissionPublisher<ZSet> publisher = new SubmissionPublisher<>()) {
                publisher.subscribe(subscriber);
                if (operand.fqn.getSize() > 1) {
                    final String dbName = operand.fqn.getRoot();
                    final MongoDatabase db = mongoClient.getDatabase(dbName);
                    final String collectionName = operand.fqn.getName(1);
                    final MongoCollection<Document> collection = db.getCollection(collectionName);
                    final Bson filter = filterFrom(codec, operand);
                    final Publisher<DeleteResult> deletion = collection.deleteMany(filter);
                    final CountDownLatch latch = new CountDownLatch(1);
                    deletion.subscribe(new Subscriber<>() {

                        private volatile Subscription subscription;

                        @Override
                        public void onSubscribe(Subscription subscription) {
                            this.subscription = subscription;
                            this.subscription.request(1);
                        }

                        @Override
                        public void onNext(DeleteResult deleteResult) {
                            final zest.nio.meta.Mongo meta = new zest.nio.meta.Mongo(deleteResult);
                            final ZSet result = ZSet.fqn(operand.fqn)
                                    .put(TAG_META, codec.encode(meta).set())
                                    .set();
                            publisher.submit(result);
                            this.subscription.request(1);
                        }

                        @Override
                        public void onError(final Throwable t) {
                            publisher.closeExceptionally(t);
                            latch.countDown();
                        }

                        @Override
                        public void onComplete() {
                            publisher.close();
                            latch.countDown();
                        }
                    });
                    try {
                        latch.await();
                    } catch (InterruptedException e) {
                        publisher.closeExceptionally(e);
                    }
                } else {
                    final Exception e = new IllegalArgumentException(String.format(
                            "(operand.fqn = %s) doesn't express <db_name>/<collection_name>",
                            operand.fqn));
                    publisher.closeExceptionally(e);
                }
            }
        }

        /**
         * {@inheritDoc}
         * <p></p>
         * Mongo updates with same algorithm for {@link ZSet#isIdentifiable()} or not.
         *
         * @param operand    should be {@link ZSet#isIdentifiable()} true.
         * @param subscriber of the operation.
         */
        @Override
        protected void delete_resource(
                @NotNull final ZSet operand,
                @NotNull final Flow.Subscriber<ZSet> subscriber) {
            delete_container(operand, subscriber);
        }

        /**
         * {@inheritDoc}
         *
         * @param operand    representing the resource to delete.
         * @param subscriber of the operation.
         * @return {@inheritDoc}
         */
        @NotNull
        @Override
        public Out read(
                @NotNull final ZSet operand,
                @NotNull final Flow.Subscriber<ZSet> subscriber) {
            return (Out) super.read(operand, subscriber);
        }

        /**
         * {@inheritDoc}
         *
         * @param operand    should be {@link ZSet#isIdentifiable()} false.
         * @param subscriber of the operation.
         */
        @Override
        protected void read_container(
                @NotNull final ZSet operand,
                @NotNull final Flow.Subscriber<ZSet> subscriber) {
            try (final SubmissionPublisher<ZSet> publisher = new SubmissionPublisher<>()) {
                publisher.subscribe(subscriber);
                if (operand.fqn.getSize() > 1) {
                    final String dbName = operand.fqn.getRoot();
                    final MongoDatabase db = mongoClient.getDatabase(dbName);
                    final String collectionName = operand.fqn.getName(1);
                    final MongoCollection<Document> collection = db.getCollection(collectionName);
                    final Bson filter = filterFrom(codec, operand);
                    final FindPublisher<Document> finder = collection.find(filter);
                    final CountDownLatch latch = new CountDownLatch(1);
                    finder.subscribe(new Subscriber<>() {

                        private volatile Subscription subscription;

                        @Override
                        public void onSubscribe(Subscription subscription) {
                            this.subscription = subscription;
                            this.subscription.request(1);
                        }

                        @Override
                        public void onNext(Document document) {
                            // todo

                            this.subscription.request(1);
                        }

                        @Override
                        public void onError(final Throwable t) {
                            publisher.closeExceptionally(t);
                            latch.countDown();
                        }

                        @Override
                        public void onComplete() {
                            publisher.close();
                            latch.countDown();
                        }
                    });
                    try {
                        latch.await();
                    } catch (InterruptedException e) {
                        publisher.closeExceptionally(e);
                    }
                } else {
                    final Exception e = new IllegalArgumentException(String.format(
                            "(operand.fqn = %s) doesn't express <db_name>/<collection_name>",
                            operand.fqn));
                    publisher.closeExceptionally(e);
                }
            }
        }

        /**
         * {@inheritDoc}
         * <p></p>
         * Mongo updates with same algorithm for {@link ZSet#isIdentifiable()} or not.
         *
         * @param operand    should be {@link ZSet#isIdentifiable()} true.
         * @param subscriber of the operation.
         */
        @Override
        protected void read_resource(
                @NotNull final ZSet operand,
                @NotNull final Flow.Subscriber<ZSet> subscriber) {
            read_container(operand, subscriber);
        }

        @Override
        @NotNull
        public Out update(
                @NotNull final ZSet operand,
                @NotNull final Flow.Subscriber<ZSet> subscriber) {
            return (Out) super.update(operand, subscriber);
        }

        /**
         * {@inheritDoc}
         *
         * @param operand    should be {@link ZSet#isIdentifiable()} false.
         * @param subscriber of the operation.
         */
        @Override
        protected void update_container(
                @NotNull final ZSet operand,
                @NotNull final Flow.Subscriber<ZSet> subscriber) {
            try (final SubmissionPublisher<ZSet> publisher = new SubmissionPublisher<>()) {
                publisher.subscribe(subscriber);
                if (operand.fqn.getSize() > 1) {
                    final String dbName = operand.fqn.getRoot();
                    final MongoDatabase db = mongoClient.getDatabase(dbName);
                    final String collectionName = operand.fqn.getName(1);
                    final MongoCollection<Document> collection = db.getCollection(collectionName);
                    final Bson filter = filterFrom(codec, operand);
                    final Document doc = Document.parse(operand.toJSON().toString());
                    final Publisher<UpdateResult> updater = collection.updateMany(filter, doc);
                    final CountDownLatch latch = new CountDownLatch(1);
                    updater.subscribe(new Subscriber<>() {

                        private volatile Subscription subscription;

                        @Override
                        public void onSubscribe(Subscription s) {
                            this.subscription = subscription;
                            this.subscription.request(1);
                        }

                        @Override
                        public void onNext(UpdateResult updateResult) {
                            final zest.nio.meta.Mongo meta = new zest.nio.meta.Mongo(updateResult);
                            final ZSet result = ZSet.fqn(operand.fqn).put(TAG_META, codec.encode(meta).set()).set();
                            publisher.submit(result);
                            this.subscription.request(1);
                        }

                        @Override
                        public void onError(Throwable t) {
                            publisher.closeExceptionally(t);
                            latch.countDown();
                        }

                        @Override
                        public void onComplete() {
                            publisher.close();
                            latch.countDown();
                        }
                    });
                } else {
                    final Exception e = new IllegalArgumentException(String.format(
                            "(operand.fqn = %s) doesn't express <db_name>/<collection_name>",
                            operand.fqn));
                    publisher.closeExceptionally(e);
                }
            }
        }

        /**
         * {@inheritDoc}
         * <p></p>
         * Mongo updates with same algorithm for {@link ZSet#isIdentifiable()} or not.
         *
         * @param operand    should be {@link ZSet#isIdentifiable()} true.
         * @param subscriber of the operation.
         */
        @Override
        protected void update_resource(
                @NotNull final ZSet operand,
                @NotNull final Flow.Subscriber<ZSet> subscriber) {
            update_container(operand, subscriber);
        }

    } //~ Out

}
