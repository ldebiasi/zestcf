package zest.nio;

import org.jetbrains.annotations.NotNull;
import zest.data.Codec;
import zest.data.FQN;
import zest.data.ZSet;
import zest.util.URL;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.concurrent.Flow;
import java.util.concurrent.SubmissionPublisher;
import java.util.stream.Stream;

/**
 * Provide the service based on the Operating System's File.
 */
public class OSF extends Service {

    /**
     * Create a new service connected to the data service at {@code url} address.
     *
     * @param url address of the data service connected to this service.
     */
    protected OSF(
            @NotNull final URL url) {
        super(url);
    }

    /**
     * {@inheritDoc}
     *
     * @return {@inheritDoc}
     */
    @Override
    @NotNull
    public Out out() {
        return new Out(this);
    }

    /**
     * Implement the output channel for the OS file System.
     */
    public class Out extends Service.Out {

        /**
         * Codec used to serialize and deserialize {@link ZSet} instances to and from this channel.
         */
        protected final transient Codec codec = Codec.DEFAULT;

        /**
         * Return a new out channel for the OS file system.
         *
         * @param service Local File System service.
         */
        protected Out(
                @NotNull final OSF service) {
            super(service);
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public void close() {
            // Do nothing.
        }

        /**
         * {@inheritDoc}
         *
         * @param operand    representing the resource to create.
         * @param subscriber of the operation.
         * @return {@inheritDoc}
         */
        @Override
        @NotNull
        public Out create(
                @NotNull final ZSet operand,
                @NotNull final Flow.Subscriber<ZSet> subscriber) {
            return (Out) super.create(operand, subscriber);
        }

        /**
         * {@inheritDoc}
         *
         * @param operand    should be {@link ZSet#isIdentifiable()} false.
         * @param subscriber of the operation.
         */
        @Override
        @NotNull
        protected void create_container(
                @NotNull final ZSet operand,
                @NotNull final Flow.Subscriber<? super ZSet> subscriber) {
            try (final SubmissionPublisher<ZSet> publisher = publisherFactory.create()) {
                publisher.subscribe(subscriber);
                final Path dir = service.url.toPath().resolve(operand.fqn.toPath());
                try {
                    Files.createDirectories(dir);
                    final zest.nio.meta.OSF meta = new zest.nio.meta.OSF(Files.readAttributes(dir, BasicFileAttributes.class));
                    final ZSet result = ZSet.fqn(operand.fqn).put(TAG_META, codec.encode(meta).set()).set(operand.id);
                    publisher.submit(result);
                } catch (IOException e) {
                    publisher.closeExceptionally(e);
                }
            }
        }

        /**
         * {@inheritDoc}
         *
         * @param operand    should be {@link ZSet#isIdentifiable()} true.
         * @param subscriber of the operation.
         */
        @Override
        protected void create_resource(
                @NotNull final ZSet operand,
                @NotNull final Flow.Subscriber<ZSet> subscriber) {
            try (final SubmissionPublisher<ZSet> publisher = new SubmissionPublisher<>()) {
                publisher.subscribe(subscriber);
                final Path path = service.url.toPath().resolve(operand.fqn.toPath(operand.id));
                final File file = path.toFile();
                final String content = operand.toJSON().toString();
                try (final FileWriter fileWriter = new FileWriter(file)) {
                    fileWriter.write(content);
                    final zest.nio.meta.OSF meta = new zest.nio.meta.OSF(Files.readAttributes(path, BasicFileAttributes.class));
                    final ZSet result = ZSet.fqn(operand.fqn).put(TAG_META, codec.encode(meta).set()).set(operand.id);
                    publisher.submit(result);
                } catch (IOException e) {
                    publisher.closeExceptionally(e);
                }
            }
        }

        /**
         * {@inheritDoc}
         *
         * @param operand    representing the resource to create.
         * @param subscriber of the operation.
         * @return {@inheritDoc}
         */
        @Override
        @NotNull
        public Out delete(
                @NotNull final ZSet operand,
                @NotNull final Flow.Subscriber<ZSet> subscriber) {
            return (Out) super.delete(operand, subscriber);
        }


        /**
         * {@inheritDoc}
         *
         * @param operand    should be {@link ZSet#isIdentifiable()} false.
         * @param subscriber of the operation.
         */
        protected void delete_container(
                @NotNull final ZSet operand,
                @NotNull final Flow.Subscriber<ZSet> subscriber) {
            try (final SubmissionPublisher<ZSet> publisher = new SubmissionPublisher<>()) {
                publisher.subscribe(subscriber);
                final Path dir = service.url.toPath().resolve(operand.fqn.toPath());
                try {
                    if (Files.exists(dir)) {
                        final zest.nio.meta.OSF meta = new zest.nio.meta.OSF(Files.readAttributes(dir, BasicFileAttributes.class));
                        final ZSet result = ZSet.fqn(operand.fqn).put(TAG_META, codec.encode(meta).set()).set(operand.id);
                        Files.delete(dir);
                        publisher.submit(result);
                    }
                } catch (IOException e) {
                    publisher.closeExceptionally(e);
                }
            }
        }

        /**
         * {@inheritDoc}
         *
         * @param operand    should be {@link ZSet#isIdentifiable()} true.
         * @param subscriber of the operation.
         */
        protected void delete_resource(
                @NotNull final ZSet operand,
                @NotNull final Flow.Subscriber<ZSet> subscriber) {
            try (final SubmissionPublisher<ZSet> publisher = new SubmissionPublisher<>()) {
                publisher.subscribe(subscriber);
                final Path path = service.url.toPath().resolve(operand.fqn.toPath(operand.id));
                try {
                    if (Files.exists(path)) {
                        final zest.nio.meta.OSF meta = new zest.nio.meta.OSF(Files.readAttributes(path, BasicFileAttributes.class));
                        final ZSet result = ZSet.fqn(operand.fqn).put(TAG_META, codec.encode(meta).set()).set(operand.id);
                        Files.delete(path);
                        publisher.submit(result);
                    }
                } catch (IOException e) {
                    publisher.closeExceptionally(e);
                }
            }
        }


        /**
         * {@inheritDoc}
         *
         * @return {@inheritDoc}
         */
        @Override
        @NotNull
        public OSF getService() {
            return (OSF) service;
        }

        /**
         * {@inheritDoc}
         *
         * @param operand    representing the resource to create.
         * @param subscriber of the operation.
         * @return {@inheritDoc}
         */
        @Override
        @NotNull
        public Out read(
                @NotNull final ZSet operand,
                @NotNull final Flow.Subscriber<ZSet> subscriber) {
            return (Out) super.read(operand, subscriber);
        }

        /**
         * {@inheritDoc}
         *
         * @param operand    should be {@link ZSet#isIdentifiable()} false.
         * @param subscriber of the operation.
         */
        @Override
        protected void read_container(
                @NotNull final ZSet operand,
                @NotNull final Flow.Subscriber<ZSet> subscriber) {
            try (final SubmissionPublisher<ZSet> publisher = new SubmissionPublisher<>()) {
                publisher.subscribe(subscriber);
                final Path dir = service.url.toPath().resolve(operand.fqn.toPath());
                try (final Stream<Path> pathStream = Files.list(dir)) {
                    pathStream.forEachOrdered(path -> {
                        final File file = path.toFile();
                        try {
                            final zest.nio.meta.OSF meta = new zest.nio.meta.OSF(Files.readAttributes(path, BasicFileAttributes.class));
                            final ZSet result = ZSet.fqn(FQN.of(path))
                                    .put(TAG_META, codec.encode(meta).set())
                                    .set(file.getName());
                            publisher.submit(result);
                        } catch (IOException e) {
                            publisher.closeExceptionally(e);
                        }
                    });
                } catch (IOException e) {
                    publisher.closeExceptionally(e);
                }
            }
        }

        /**
         * {@inheritDoc}
         *
         * @param operand    should be {@link ZSet#isIdentifiable()} true.
         * @param subscriber of the operation.
         */
        @Override
        protected void read_resource(
                @NotNull ZSet operand,
                @NotNull Flow.Subscriber<ZSet> subscriber) {
            try (final SubmissionPublisher<ZSet> publisher = new SubmissionPublisher<>()) {
                publisher.subscribe(subscriber);
                final Path path = service.url.toPath().resolve(operand.fqn.toPath());
                final File file = path.toFile();
                try (final FileReader fileReader = new FileReader(file)) {
                    final ZSet load = codec.decode(fileReader);
                    final zest.nio.meta.OSF meta = new zest.nio.meta.OSF(Files.readAttributes(path, BasicFileAttributes.class));
                    final ZSet result = ZSet.fqn(load.fqn)
                            .put(TAG_META, codec.encode(meta).set())
                            .put(load)
                            .set(load.id);
                    publisher.submit(result);
                } catch (IOException e) {
                    publisher.closeExceptionally(e);
                }
            }
        }

        /**
         * {@inheritDoc}
         *
         * @param operand    representing the resource to create.
         * @param subscriber of the operation.
         * @return {@inheritDoc}
         */
        @Override
        @NotNull
        public Out update(
                @NotNull final ZSet operand,
                @NotNull final Flow.Subscriber<ZSet> subscriber) {
            return (Out) super.update(operand, subscriber);
        }

        /**
         * {@inheritDoc}
         * <p></p>
         * Publish an {@link UnsupportedOperationException}!
         *
         * @param operand    should be {@link ZSet#isIdentifiable()} false.
         * @param subscriber of the operation.
         */
        @Override
        protected void update_container(
                @NotNull final ZSet operand,
                @NotNull final Flow.Subscriber<ZSet> subscriber) {
            try (final SubmissionPublisher<ZSet> publisher = new SubmissionPublisher<>()) {
                publisher.subscribe(subscriber);
                final URL url = service.url.resolve(operand.fqn.toPath(operand.id).toAbsolutePath());
                final Exception e = new UnsupportedOperationException(String.format("%s can't update.", url));
                publisher.closeExceptionally(e);
            }
        }

        @Override
        protected void update_resource(
                @NotNull final ZSet operand,
                @NotNull final Flow.Subscriber<ZSet> subscriber) {
            try (final SubmissionPublisher<ZSet> publisher = new SubmissionPublisher<>()) {
                publisher.subscribe(subscriber);
                final Path path = service.url.toPath().resolve(operand.fqn.toPath(operand.id));
                final File file = path.toFile();
                if (file.exists()) {
                    final String content = operand.toJSON().toString();
                    try (final FileWriter fileWriter = new FileWriter(file)) {
                        fileWriter.write(content);
                        final zest.nio.meta.OSF meta = new zest.nio.meta.OSF(Files.readAttributes(path, BasicFileAttributes.class));
                        final ZSet result = ZSet.fqn(operand.fqn).put(TAG_META, codec.encode(meta).set()).set(operand.id);
                        publisher.submit(result);
                    } catch (IOException e) {
                        publisher.closeExceptionally(e);
                    }
                } else {
                    final URL url = service.url.resolve(operand.fqn.toPath(operand.id).toAbsolutePath());
                    final Exception e = new FileNotFoundException(String.format("%s not found.", url));
                    publisher.closeExceptionally(e);
                }
            }
        }

    } //~

 }
