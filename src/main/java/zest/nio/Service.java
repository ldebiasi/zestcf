package zest.nio;

import org.jetbrains.annotations.NotNull;
import zest.data.ZSet;
import zest.util.URL;

import java.io.Closeable;
import java.util.concurrent.Flow;
import java.util.concurrent.SubmissionPublisher;

/**
 * The abstraction of the data service provided by the ZEST Computing Fabric
 * <p></p>
 * Thread safe.
 */
public abstract class Service {

    /**
     * Encapsulate in a {@link ZSet} result an {@link zest.data.Element} describing
     * the metadata
     */
    public final static transient String TAG_META = "meta";

    /**
     * Address of the data service connected to this service.
     */
    public final URL url;

    /**
     * Create a new service connected to the data service at {@code url} address.
     *
     * @param url address of the data service connected to this service.
     */
    protected Service(
            @NotNull final URL url) {
        this.url = url;
    }

    /**
     * Return the service connected to the data service at the {@code url} service.
     * <p></p>
     * Appliances are cached using {@link zest.io.Service#url} as key.
     *
     * @param url url address of the data service connected to this service.
     * @return the service connected to the data service at the {@code url} service.
     * @throws IllegalArgumentException if the {@code url} doesn't address any service or isn't able to start it.
     */
    @NotNull
    public static Service at(
            @NotNull final URL url)
            throws
            IllegalArgumentException {
        switch (Scheme.valueOf(url.getScheme())) {
            case file:
                return new OSF(url);
//                    case jdbc:
//                        return new JDBC(url);
//                    case jms:
//                        return new JMS(url);
//                    case kafka:
//                        return new Kafka(url);
//                    case kinesis:
//                        return new Kinesis(url);
            case mongo:
                return new Mongo(url);
//                    case sqs:
//                        return new SQS(url);
        }
        throw new IllegalArgumentException(String.format("(uri = %s) doesn't address a service", url));

    }

    /**
     * Return a new output channel of this service.
     *
     * @return a new output channel of this service.
     */
    @NotNull
    public abstract Out out();


    /**
     * The base class for I/O channels.
     * <p></p>
     * Thread safe.
     *
     * @see <a href="https://github.com/reactive-streams/reactive-streams-jvm#specification">Reactive Streams JMS</a>
     */
    public static abstract class Channel implements Closeable {

        /**
         * Reference to the service of this channel.
         */
        protected final Service service;

        /**
         * Factory of {@link SubmissionPublisher} instances used by channel's operative methods.
         */
        protected final SubmissionPublisherFactory publisherFactory;

        /**
         * Return a new channel for this {@code service}.
         *
         * @param service of this channel.
         */
        public Channel(
                @NotNull final Service service) {
            this(service, new SubmissionPublisherFactory() {
            });
        }

        /**
         * Return a new channel for this {@code service}.
         *
         * @param service          of this channel.
         * @param publisherFactory used to create {@link SubmissionPublisher} notifying to the channel operative methods.
         */
        public Channel(
                @NotNull final Service service,
                @NotNull final SubmissionPublisherFactory publisherFactory) {
            this.service = service;
            this.publisherFactory = publisherFactory;
        }

        /**
         * Return the {@link zest.io.Service} of this channel.
         *
         * @return the {@link zest.io.Service} of this channel.
         */
        @NotNull
        public abstract Service getService();


    } //~ Channel

    /**
     * Provide an input channel to observe resourced on this {@link Service}.
     * <p></p>
     * Thread safe.
     */
    public static abstract class In extends Channel {

        /**
         * Return a new channel for this {@code service} with at least one {@code subscriber}.
         *
         * @param service of this channel.
         */
        public In(
                @NotNull final Service service) {
            super(service);
        }

    } //~ In

    /**
     * Provide an output channel to create, read (or query), delete and update resources for this {@link Service}.
     * <p></p>
     * Thread safe.
     */
    public static abstract class Out extends Channel {

        /**
         * Return a new output channel for the {@code service}.
         *
         * @param service this new output channel belongs to.
         */
        protected Out(
                @NotNull final Service service) {
            super(service);
        }

        /**
         * Create the resource represented by {@code operand} on the {@link #service}.
         *
         * @param operand    representing the resource to create.
         * @param subscriber of the operation.
         * @return this.
         */
        @NotNull
        public Out create(
                @NotNull final ZSet operand,
                @NotNull final Flow.Subscriber<ZSet> subscriber) {
            if (operand.isIdentifiable()) {
                create_resource(operand, subscriber);
            } else {
                create_container(operand, subscriber);
            }
            return this;
        }

        /**
         * The container is identified by {@link ZSet#fqn} only.
         *
         * @param operand    should be {@link ZSet#isIdentifiable()} false.
         * @param subscriber of the operation.
         */
        protected abstract void create_container(
                @NotNull final ZSet operand,
                @NotNull final Flow.Subscriber<? super ZSet> subscriber);

        /**
         * The resource is identifies by {@link ZSet#fqn} and {@link ZSet#id}.
         *
         * @param operand    should be {@link ZSet#isIdentifiable()} true.
         * @param subscriber of the operation.
         */
        protected abstract void create_resource(
                @NotNull final ZSet operand,
                @NotNull final Flow.Subscriber<ZSet> subscriber);

        /**
         * Delete the resource represented by {@code operand} on the {@link #service}.
         *
         * @param operand    representing the resource to delete.
         * @param subscriber of the operation.
         * @return this.
         */
        @NotNull
        public Out delete(
                @NotNull final ZSet operand,
                @NotNull final Flow.Subscriber<ZSet> subscriber) {
            if (operand.isIdentifiable()) {
                delete_resource(operand, subscriber);
            } else {
                delete_container(operand, subscriber);
            }
            return this;
        }

        /**
         * The container is identified by {@link ZSet#fqn} only.
         *
         * @param operand    should be {@link ZSet#isIdentifiable()} false.
         * @param subscriber of the operation.
         */
        protected void delete_container(
                @NotNull final ZSet operand,
                @NotNull final Flow.Subscriber<ZSet> subscriber) {
        }

        /**
         * The resource is identifies by {@link ZSet#fqn} and {@link ZSet#id}.
         *
         * @param operand    should be {@link ZSet#isIdentifiable()} true.
         * @param subscriber of the operation.
         */
        protected void delete_resource(
                @NotNull final ZSet operand,
                @NotNull final Flow.Subscriber<ZSet> subscriber) {
        }

        /**
         * Read the resource represented by {@code operand} on the {@link #service}.
         *
         * @param operand    representing the resource to delete.
         * @param subscriber of the operation.
         * @return this.
         */
        @NotNull
        public Out read(
                @NotNull final ZSet operand,
                @NotNull final Flow.Subscriber<ZSet> subscriber) {
            if (operand.isIdentifiable()) {
                read_resource(operand, subscriber);
            } else {
                read_container(operand, subscriber);
            }
            return this;
        }

        /**
         * The container is identified by {@link ZSet#fqn} only.
         *
         * @param operand    should be {@link ZSet#isIdentifiable()} false.
         * @param subscriber of the operation.
         */
        protected abstract void read_container(
                @NotNull final ZSet operand,
                @NotNull final Flow.Subscriber<ZSet> subscriber);


        /**
         * The resource is identifies by {@link ZSet#fqn} and {@link ZSet#id}.
         *
         * @param operand    should be {@link ZSet#isIdentifiable()} true.
         * @param subscriber of the operation.
         */
        protected abstract void read_resource(
                @NotNull final ZSet operand,
                @NotNull final Flow.Subscriber<ZSet> subscriber);

        /**
         * Update the resource represented by {@code operand} on the {@link #service}.
         *
         * @param operand    representing the resource to delete.
         * @param subscriber of the operation.
         * @return this.
         */
        @NotNull
        public Out update(
                @NotNull final ZSet operand,
                @NotNull final Flow.Subscriber<ZSet> subscriber) {
            if (operand.isIdentifiable()) {
                update_resource(operand, subscriber);
            } else {
                update_container(operand, subscriber);
            }
            return this;
        }

        /**
         * The container is identified by {@link ZSet#fqn} only.
         *
         * @param operand    should be {@link ZSet#isIdentifiable()} false.
         * @param subscriber of the operation.
         */
        protected abstract void update_container(
                @NotNull final ZSet operand,
                @NotNull final Flow.Subscriber<ZSet> subscriber);


        /**
         * The resource is identifies by {@link ZSet#fqn} and {@link ZSet#id}.
         *
         * @param operand    should be {@link ZSet#isIdentifiable()} true.
         * @param subscriber of the operation.
         */
        protected abstract void update_resource(
                @NotNull final ZSet operand,
                @NotNull final Flow.Subscriber<ZSet> subscriber);


    } //~ Out

    /**
     * Provide a factory to create new {@link SubmissionPublisher} instances.
     * <p></p>
     * Used internally by channel operative methods to publish results of the operations.
     */
    public interface SubmissionPublisherFactory {

        default SubmissionPublisher<ZSet> create() {
            return new SubmissionPublisher<>();
        }

    }

    /**
     * URL schemas supported by services extending this {@link Service} class.
     */
    public enum Scheme {
        //        cassandra,
        corda,
        dynamo,
        es,
        file,
        //        ftp,
        hyper,
        http,
        https,
        ipfs,
        jdbc,
        jms,
        kafka,
        kinesis,
        minio,
        mongo,
        neo4j,
        s3,
        //        sftp,
//        solr,
        sqs,
        ws,
        wss;

        /**
         * Return {@code true} if this [A+M] release provide the {@link zest.io.Service} for the scheme of {@code url}.
         *
         * @param url address of the service to provide with {@link zest.io.Service}.
         * @return {@code true} if this [A+M] release provide the {@link zest.io.Service} for the scheme of {@code url}.
         */
        public static boolean isAvailable(
                @NotNull final URL url) {
            final String scheme = url.getScheme().toLowerCase();
            for (Scheme available : values()) {
                if (scheme.equals(available.name())) {
                    return true;
                }
            }
            return false;
        }

    } //~ Scheme

}
