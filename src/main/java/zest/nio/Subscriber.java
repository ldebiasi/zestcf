package zest.nio;

import com.google.common.base.Throwables;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import zest.data.ZSet;

import java.util.UUID;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.Flow;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;

/**
 * Implement a default <a href="https://www.reactive-streams.org/">Reactive Streams</a> subscriber.
 * Subscribed operation is synchronizable with {@link #await()} and {@link #await(long, TimeUnit)}.
 * Condition if the operation completed successfully with {@link #isFailed()} and get the error with {@link #getError()}.
 */
public abstract class Subscriber implements Flow.Subscriber<ZSet> {

    /**
     * Logger
     */
    public static final Logger LOG = LoggerFactory.getLogger(Subscriber.class);

    /**
     * Synchronize the completion of the operation this subscribes.
     */
    protected final CountDownLatch completionLatch = new CountDownLatch(1);

    /**
     * Max number of results requested by default.
     */
    protected final long requests;

    /**
     * Hold here the last {@link Throwable} catched on {@link #onError(Throwable)} method.
     */
    protected final AtomicReference<Throwable> catcher = new AtomicReference();

    /**
     * Operation identifier is set in {@link #onSubscribe(Flow.Subscription)}.
     */
    @Nullable
    protected volatile UUID op_id;

    /**
     * Reactive stream subscription is set in {@link #onSubscribe(Flow.Subscription)}
     */
    @Nullable
    protected volatile Flow.Subscription subscription;

    /**
     * Return a new Subscription, by default it {@code requests} results per time.
     * <p></p>
     * Call {@link Flow.Subscription#request(long)} for {@link #subscription} in
     * {@link #onNext(ZSet)} method to adjust requests.
     *
     * @param requests Max number of results requested by default.
     */
    public Subscriber(final long requests) {
        this.requests = requests;
    }

    /**
     * return a new Subscription requesting one result per time.
     * <p></p>
     * Call {@link Flow.Subscription#request(long)} for {@link #subscription} in
     * {@link #onNext(ZSet)} method to adjust requests.
     */
    public Subscriber() {
        this(1);
    }

    /**
     * Causes the current thread to wait until {@link #completionLatch} has counted down to zero.
     *
     * @throws InterruptedException if the current thread is interrupted while waiting.
     */
    public void await()
            throws InterruptedException {
        completionLatch.await();
    }

    /**
     * Causes the current thread to wait until {@link #completionLatch} has counted down to zero,
     * unless the thread is interrupted, or the specified waiting time elapses.
     *
     * @param timeout  expressed in {@link TimeUnit}.
     * @param timeUnit used to express {@timeout}.
     * @return {@true}  if the count reached zero
     * and {@code false} if the waiting time elapsed before the count reached zero.
     * @throws InterruptedException if the current thread is interrupted while waiting.
     */
    public boolean await(
            final long timeout,
            @NotNull TimeUnit timeUnit)
            throws InterruptedException {
        return completionLatch.await(timeout, timeUnit);
    }

    /**
     * Cancel this subscription to the subscribed operation.
     */
    public synchronized void cancel() {
        if (subscription != null) {
            subscription.cancel();
        }
    }

    /**
     * Return the last error caught by {@link #onError(Throwable)}, {@code null} if the operation run(s) smoothly.
     *
     * @return the last error caught by {@link #onError(Throwable)}, {@code null} if the operation run(s) smoothly.
     */
    @Nullable
    public Throwable getError() {
        return catcher.get();
    }

    /**
     * Return {@code true} id the subscribed operation called {@link #onError(Throwable)}.
     *
     * @return {@code true} id the subscribed operation called {@link #onError(Throwable)}.
     */
    public boolean isFailed() {
        return catcher.get() != null;
    }

    /**
     * {@inheritDoc}
     * <p></p>
     * Called first when the operation begins.
     * Default implementation requests one result per time.
     * If this method is overridden {@link #subscription} must be set in the method or
     *
     * @param subscription {@inheritDoc}.
     */
    @Override
    public void onSubscribe(
            @NotNull final Flow.Subscription subscription) {
        this.subscription = subscription;
        op_id = UUID.randomUUID();
        LOG.debug("BEG {}.", op_id);
        subscription.request(requests);
    }

    /**
     * {@inheritDoc}
     * <p></p>
     * Only called after {@link #onSubscribe(Flow.Subscription)} call.
     *
     * @param result operation's result.
     */
    @Override
    abstract public void onNext(
            @NotNull final ZSet result);

    /**
     * {@inheritDoc}
     * <p></p>
     * Called if the subscribed operation results in an error.
     * Only called after {@link #onSubscribe(Flow.Subscription)} call.
     * Call {@link AtomicReference#set(Object) for {@link #catcher}} if this method is overridden.
     *
     * @param throwable describes the error.
     */
    @Override
    public void onError(
            @NotNull final Throwable throwable) {
        catcher.set(throwable);
        completionLatch.countDown();
        LOG.error("ERR {}: {}!", op_id, Throwables.getStackTraceAsString(throwable));
    }

    /**
     * {@inheritDoc}
     * <p></p>
     * Called if the subscribed operation is completed without errors.
     * Called last.
     */
    @Override
    public void onComplete() {
        LOG.debug("END {}.", op_id);
        completionLatch.countDown();
    }

    /**
     * Ask the operation to results at most {@link #requests} results before this method has to be called again.
     */
    public void next() {
        next(requests);
    }

    /**
     * Ask the operation to results at most {@code requests} results before this method has to be called again.
     * Call me on {@link #onNext(ZSet)} implementation.
     *
     * @param requests of results.
     */
    public synchronized void next(final long requests) {
        if (subscription != null) {
            subscription.request(requests);
        }
    }

}
