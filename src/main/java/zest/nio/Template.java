package zest.nio;

import com.mongodb.reactivestreams.client.MongoClient;
import com.mongodb.reactivestreams.client.MongoClients;
import org.jetbrains.annotations.NotNull;
import zest.data.Codec;
import zest.data.ZSet;
import zest.util.URL;

import java.util.concurrent.Flow;

/**
 * Provide the service for MongoDB.
 */
public class Template extends Service {

    /**
     * Create a new MongoDb service connected to database at {@code url} address.
     *
     * @param url address of the MongoDb server.
     */
    public Template(
            @NotNull final URL url) {
        super(url);
    }


    /**
     * {@inheritDoc}
     *
     * @return {@inheritDoc}
     */
    @Override
    @NotNull
    public Out out() {
        return new Out(this);
    }

    /**
     * Implement the output channel for MongoDB
     */
    public class Out extends Service.Out {

        /**
         * Codec used to serialize and deserialize {@link ZSet} instances to and from this channel.
         */
        protected final transient Codec codec = Codec.DEFAULT;

        /**
         * Thread safe.
         */
        protected final transient MongoClient mongoClient;

        /**
         * Return a new out channel for MongoDB.
         *
         * @param service MongoDB service.
         */
        protected Out(
                @NotNull final Template service) {
            super(service);
            final String url = service.url.port == URL.NO_PORT
                    ? String.format("mongodb://%s", service.url.host)
                    : String.format("mongodb://%s:%d", service.url.host, service.url.port);
            mongoClient = MongoClients.create(url);
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public void close() {
            mongoClient.close();
        }


        /**
         * {@inheritDoc}
         *
         * @return {@inheritDoc}
         */
        @Override
        @NotNull
        public Template getService() {
            return (Template) service;
        }

        /**
         * {@inheritDoc}
         *
         * @param operand    representing the resource to create.
         * @param subscriber of the operation.
         * @return {@inheritDoc}
         */
        @Override
        @NotNull
        public Out create(
                @NotNull final ZSet operand,
                @NotNull final Flow.Subscriber<ZSet> subscriber) {
            return (Out) super.create(operand, subscriber);
        }

        /**
         * {@inheritDoc}
         *
         * @param operand    should be {@link ZSet#isIdentifiable()} false.
         * @param subscriber of the operation.
         */
        @Override
        protected void create_container(
                @NotNull final ZSet operand,
                @NotNull final Flow.Subscriber<? super ZSet> subscriber) {

        }

        /**
         * {@inheritDoc}
         *
         * @param operand    should be {@link ZSet#isIdentifiable()} true.
         * @param subscriber of the operation.
         */
        @Override
        protected void create_resource(
                @NotNull final ZSet operand,
                @NotNull final Flow.Subscriber<ZSet> subscriber) {

        }

        /**
         * {@inheritDoc}
         *
         * @param operand    representing the resource to delete.
         * @param subscriber of the operation.
         * @return
         */
        @Override
        @NotNull
        public Out delete(
                @NotNull final ZSet operand,
                @NotNull final Flow.Subscriber<ZSet> subscriber) {
            return (Out) super.delete(operand, subscriber);
        }

        /**
         * {@inheritDoc}
         *
         * @param operand    should be {@link ZSet#isIdentifiable()} false.
         * @param subscriber of the operation.
         */
        @Override
        protected void delete_container(
                @NotNull final ZSet operand,
                @NotNull final Flow.Subscriber<ZSet> subscriber) {
            super.delete_container(operand, subscriber);
        }

        /**
         * {@inheritDoc}
         * @param operand    should be {@link ZSet#isIdentifiable()} true.
         * @param subscriber of the operation.
         */
        @Override
        protected void delete_resource(
                @NotNull final ZSet operand,
                @NotNull final Flow.Subscriber<ZSet> subscriber) {
            super.delete_resource(operand, subscriber);
        }

        /**
         * {@inheritDoc}
         *
         * @param operand    representing the resource to delete.
         * @param subscriber of the operation.
         * @return {@inheritDoc}
         */
        @NotNull
        @Override
        public Out read(
                @NotNull final ZSet operand,
                @NotNull final Flow.Subscriber<ZSet> subscriber) {
            return (Out) super.read(operand, subscriber);
        }

        /**
         * {@inheritDoc}
         *
         * @param operand    should be {@link ZSet#isIdentifiable()} false.
         * @param subscriber of the operation.
         */
        @Override
        protected void read_container(
                @NotNull final ZSet operand,
                @NotNull final Flow.Subscriber<ZSet> subscriber) {

        }

        /**
         * {@inheritDoc}
         *
         * @param operand    should be {@link ZSet#isIdentifiable()} true.
         * @param subscriber of the operation.
         */
        @Override
        protected void read_resource(
                @NotNull final ZSet operand,
                @NotNull final Flow.Subscriber<ZSet> subscriber) {

        }

        @Override
        @NotNull
        public Out update(
                @NotNull final ZSet operand,
                @NotNull final Flow.Subscriber<ZSet> subscriber) {
            return (Out) super.update(operand, subscriber);
        }

        /**
         * {@inheritDoc}
         *
         * @param operand    should be {@link ZSet#isIdentifiable()} false.
         * @param subscriber of the operation.
         */
        @Override
        protected void update_container(
                @NotNull final ZSet operand,
                @NotNull final Flow.Subscriber<ZSet> subscriber) {

        }

        /**
         * {@inheritDoc}
         *
         * @param operand    should be {@link ZSet#isIdentifiable()} true.
         * @param subscriber of the operation.
         */
        @Override
        protected void update_resource(
                @NotNull final ZSet operand,
                @NotNull final Flow.Subscriber<ZSet> subscriber) {

        }

    } //~ Out

}
