package zest.nio.meta;

/**
 * Tag classes used to describe the metadata describing the result of operative methods.
 */
public interface Metadata {

} //~ Metadata
