package zest.nio.meta;

import org.jetbrains.annotations.NotNull;

import java.nio.file.attribute.BasicFileAttributes;

public class OSF implements Metadata {

    public final String creation_time;

    public final Boolean is_directory;

    public final Boolean is_other;

    public final Boolean is_regular_file;

    public final Boolean is_symbolic_Link;

    public final String last_access_time;

    public final String last_modified_time;

    public final Long size;

    public OSF(
            @NotNull final BasicFileAttributes bfa) {
        creation_time = bfa.creationTime().toInstant().toString();
        is_directory = bfa.isDirectory();
        is_other = bfa.isOther();
        is_regular_file = bfa.isRegularFile();
        last_access_time = bfa.lastAccessTime().toInstant().toString();
        last_modified_time = bfa.lastModifiedTime().toInstant().toString();
        is_symbolic_Link = bfa.isSymbolicLink();
        size = bfa.size();
    }

} //~ Metadata
