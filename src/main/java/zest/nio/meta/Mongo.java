package zest.nio.meta;

import com.mongodb.client.result.DeleteResult;
import com.mongodb.client.result.UpdateResult;
import com.mongodb.reactivestreams.client.Success;
import org.jetbrains.annotations.NotNull;

public class Mongo implements Metadata {

    public final long count;

    private final boolean isAcknowledged;

    public Mongo(
            final @NotNull DeleteResult deleteResult) {
        count = deleteResult.getDeletedCount();
        isAcknowledged = deleteResult.wasAcknowledged();
    }

    public Mongo(
            final @NotNull Success ignored) {
        count = 1;
        isAcknowledged = true;
    }

    public Mongo(
            final @NotNull UpdateResult updateResult) {
        count = updateResult.getModifiedCount();
        isAcknowledged = updateResult.wasAcknowledged();
    }

}
