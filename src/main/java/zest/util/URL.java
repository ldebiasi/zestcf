package zest.util;

import com.google.common.base.Joiner;
import com.google.common.base.MoreObjects;
import com.google.common.base.Splitter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * <a href="https://en.wikipedia.org/wiki/URL">Uniform Resource Locator</a>.
 * <p></p>
 * Immutable.
 */
public class URL implements URI {

    public final static transient int NO_PORT = -1;

    protected final static transient Splitter PATH_SPLITTER = Splitter.on('/').trimResults().omitEmptyStrings();

    protected final static transient Joiner.MapJoiner QUERY_JOINER = Joiner.on(';').withKeyValueSeparator('=');

    protected final static transient Splitter.MapSplitter QUERY_SPLITTER = Splitter.on(';').withKeyValueSeparator('=');

    @NotNull
    public final String scheme;

    @Nullable
    public final String user;

    /**
     * Password is omitted by URI and textual expression obtained by this class,
     * is ignored in {@link #equals(Object)} and {@link #hashCode()} methods.
     */
    @Nullable
    public final String password;

    @Nullable
    public final String host;

    public final int port;

    @NotNull
    public final String path;

    @Nullable
    public final String query;

    @Nullable
    public final String fragment;

    /**
     * @param scheme   it is lowercase normalized.
     * @param user
     * @param password
     * @param host     it is lowercase normalized.
     * @param port
     * @param path
     * @param query    '&' separators are chanhed in more readable ';'.
     * @param fragment
     */
    public URL(
            @NotNull final String scheme,
            @Nullable final String user,
            @Nullable final String password,
            @Nullable final String host,
            final int port,
            @NotNull final String path,
            @Nullable final String query,
            @Nullable final String fragment)
            throws
            IllegalArgumentException {
        this.scheme = scheme.toLowerCase();
        this.user = user;
        this.password = password;
        this.host = Text.isFat(host) ? host.toLowerCase() : host;
        this.port = port;
        if (Text.isFat(path)) {
            if (path.startsWith("//")) {
                throw new IllegalArgumentException(String.format("(path = %s) can't start with '//'", path));
            }
        }
        this.path = path;
        this.query = Text.isFat(query) ? query.replace('&', ';') : query;
        this.fragment = fragment;
    }

    public URL(
            @NotNull final String scheme,
            @Nullable final String user,
            @Nullable final String password,
            @Nullable final String host,
            final int port,
            @NotNull final String path,
            @Nullable final Map<String, String> queryMap,
            @Nullable final String fragment) {
        this(scheme, user, password, host, port, path, queryMap != null ? QUERY_JOINER.join(queryMap) : null, fragment);
    }


    /**
     * Return a new URL for the {@code uri}.
     *
     * @param uri to return as URL.
     * @return a new URL for the {@code uri}.
     * @throws IllegalArgumentException if {@code url} hasn't a schema and path, path can be empty but not {@code null}.
     */
    @NotNull
    public static URL at(
            @NotNull final String uri)
            throws
            IllegalArgumentException {
        final String sas = URI.schemeAndSubsOf(uri);
        final String user = userOf(uri);
        final String password = passwordOf(uri);
        final String host = hostOf(uri);
        final int port = portOf(uri);
        final String path = pathOf(uri);
        final String query = queryOf(uri);
        final String fragment = fragmentOf(uri);
        return new URL(sas, user, password, host, port, path, query, fragment);
    }

    /**
     * Return a new URL for the {@code uri}.
     *
     * @param uri to return as URL
     * @return a new URL for the {@code uri}.
     * @throws IllegalArgumentException if {@code url} hasn't a schema and path, path can be empty but not {@code null}.
     */
    @NotNull
    public static URL at(
            @NotNull final java.net.URI uri) throws
            IllegalArgumentException {
        return at(uri.toString());
    }

    /**
     * Return the authority part of the {@code uri},
     * from the charachter follwing the first '//' to the first occurrence
     * of '/' or '?' or '#' or the end of {@code uri},
     * or {@code null} {@code uri} hasn't the authority party.
     *
     * @param uri Uniform Resource Identifier
     * @return Return the authority part of the {@code uri} or {@code null}
     * @throws IllegalArgumentException if {@code uri} hasn't scheme part.
     */
    @Nullable
    static String authorityOf(
            @NotNull final String uri)
            throws
            IllegalArgumentException {
        if (uri.contains("//")) {
            final String ssp = URI.innerSSPOf(uri);
            if (Text.isFat(ssp)) {
                final StringBuilder authority = new StringBuilder();
                for (int end = 0; end < ssp.length(); end++) {
                    final char c = ssp.charAt(end);
                    if (c == '/' || c == '?' || c == '#') {
                        break;
                    }
                    authority.append(c);
                }
                // Remove eventual ':' left over undefined port.
                while (authority.length() > 0 && authority.charAt(authority.length() - 1) == ':') {
                    authority.deleteCharAt(authority.length() - 1);
                }
                return authority.toString();
            }
        }
        return null;
    }

    /**
     * Return the fragment part from the character after the first '#' to the end of {@code uri}.
     *
     * @param uri Uniform Resource Identifier.
     * @return the fragment part from the character after the first '#' to the end of {@code uri},
     * or {@code null} is {@code uri} hasn't fragment part.
     * @throws IllegalArgumentException if {@code uri} hasn't a scheme part.
     */
    @Nullable
    static String fragmentOf(
            @NotNull final String uri)
            throws
            IllegalArgumentException {
        final String ssp = URI.outerSSPOf(uri);
        final int start = ssp.indexOf('#');
        if (start > 0) {
            return ssp.substring(start + 1);
        }
        return null;
    }

    /**
     * Return the host part of the {@code uri},
     * from the character following the first occurrence of '//' or '@' to next '/'
     * or the ':' before next '/' separating host part from port part,
     * or {@code null} if {@code uri} hasn't host part.
     *
     * @param uri Uniform Resource Identifier.
     * @return Return the host part of the {@code uri},
     * or {@code null} if {@code uri} hasn't host part.
     * @throws IllegalArgumentException if {@code uri} hasn't scheme part.
     */
    @Nullable
    static String hostOf(
            @NotNull final String uri)
            throws
            IllegalArgumentException {
        final String authority = authorityOf(uri);
        if (Text.isFat(authority)) {
            int start = authority.indexOf('@');
            start = start > -1 ? start + 1 : 0; // Skip '@'
            final StringBuilder host = new StringBuilder();
            for (int index = start; index < authority.length(); index++) {
                final char c = authority.charAt(index);
                if (c == ':' || c == '/') {
                    break;
                }
                host.append(c);
            }
            return host.toString();
        }
        return null;
    }


    /**
     * Return the password from the userinfo part,
     * from the character following the first occurrence of ':' to next '@',
     * or {@code null} is {@code uri} hasn't password in the userinfo part.
     *
     * @param uri Uniform Resource Identifier.
     * @return Return the password from the userinfo part,
     * or {@code null} is {@code uri} hasn't password in the userinfo part.
     * @throws IllegalArgumentException if {@code uri} hasn't a scheme part.
     */
    @Nullable
    static String passwordOf(
            @NotNull final String uri)
            throws
            IllegalArgumentException {
        final String userinfo = userinfoOf(uri);
        if (Text.isFat(userinfo)) {
            final int start = userinfo.indexOf(':');
            if (start > -1) {
                return userinfo.substring(start + 1); // Skip ':'
            }
        }
        return null;
    }

    /**
     * Return the path component of {@code url},
     * from the next '/' after '//' for absolute paths
     * to the end of {@code url} or nect occurrence of '?' or '#',
     * or return an empty string if the path is empty.
     *
     * @param uri Uniform Resource Identifier.
     * @return Return the path component of {@code url},
     * or return an empty string if the path is empty.
     * @throws IllegalArgumentException If {@code url} hasn't a schema part.
     */
    @NotNull
    static String pathOf(
            @NotNull final String uri)
            throws
            IllegalArgumentException {
        final String ssp = URI.innerSSPOf(uri);
        final String authority = authorityOf(uri);
        int start = Text.isFat(authority) ? authority.length() : 0;
        final StringBuilder path = new StringBuilder();
        for (int index = start; index < ssp.length(); index++) {
            final char c = ssp.charAt(index);
            if (c == '?' || c == '#') {
                break;
            }
            path.append(c);
        }
        return path.toString();
    }

    /**
     * Return the port part of {@code uri},
     * the trailing decimal digits prefixed with ':',
     * or {@link #NO_PORT} is the port is not specified in {@code uri}.
     *
     * @param uri Uniform Resource Identifier.
     * @return Return the port part of {@code uri}, or {@link #NO_PORT} is the port is not specified in {@code uri}.
     * @throws IllegalArgumentException if {@code uri} hasn't scheme part.
     */
    static int portOf(
            @NotNull final String uri)
            throws
            IllegalArgumentException {
        final String authority = authorityOf(uri);
        if (Text.isFat(authority)) {
            final StringBuilder port = new StringBuilder();
            final int stop = authority.lastIndexOf(':');
            if (stop >= 0) {
                for (int start = authority.length() - 1; start > stop; start--) {
                    final char c = authority.charAt(start);
                    if (Character.isDigit(c) || c == ':') {
                        port.insert(0, c);
                    }
                }
                while (port.length() > 0 && port.charAt(0) == ':') {
                    port.deleteCharAt(0);
                }
                if (port.length() > 0)
                    return Integer.valueOf(port.toString());
            }
        }

        return NO_PORT;
    }

    /**
     * Return the query part of {@code uri}
     * from the character after the first '?' to the end of {@code uri} or to the first '#'.
     *
     * @param uri Uniform Resource Identifier.
     * @return the fragment part of {@code uri} or {@code null} if {@code uri} hasn't query part.
     * @throws IllegalArgumentException if {@code uri} hasn't a scheme part.
     */
    @Nullable
    static String queryOf(
            @NotNull final String uri)
            throws
            IllegalArgumentException {
        final String ssp = URI.innerSSPOf(uri);
        final int start = ssp.indexOf('?');
        if (start > 0) {
            final int end = ssp.indexOf('#');
            return ssp.substring(start + 1, end > 0 ? end : ssp.length());
        }
        return null;
    }

    /**
     * Return the userinfo part of {@code uri},
     * from the character after the first occurrence of '//' to next '@',
     * or {@code null} if {@code uri} hasn't userinfo part.
     *
     * @param uri Uniform Resource Identifier.
     * @return the userinfo part of {@code uri} or {@code null} if {@code} hasn't userinfo part.
     * @throws IllegalArgumentException if {@code uri} hasn't a scheme part.
     */
    @Nullable
    static String userinfoOf(
            @NotNull final String uri)
            throws
            IllegalArgumentException {
        final String authority = authorityOf(uri);
        if (Text.isFat(authority)) {
            final int end = authority.indexOf('@');
            if (end >= 0) {
                return authority.substring(0, end);
            }
        }
        return null;
    }

    /**
     * Return the user component of userinfo part,
     * from the first character after '//' to the first occurrence of ':' or '@',
     * or {@code null} is {@code uri} hasn't userinfo part.
     *
     * @param uri Uniform Resource Identifier.
     * @return Return the user component of userinfo part, or {@code null} is {@code uri} hasn't userinfo part.
     * @throws IllegalArgumentException if {@code uri} hasn't a scheme part.
     */
    @Nullable
    static String userOf(
            @NotNull final String uri)
            throws
            IllegalArgumentException {
        final String userinfo = userinfoOf(uri);
        if (Text.isFat(userinfo)) {
            final int end = userinfo.indexOf(':');
            if (end > -1) {
                return userinfo.substring(0, end);
            }

            return userinfo;
        }
        return null;
    }


    /**
     * {@inheritDoc}
     *
     * @param that {@inheritDoc}
     * @return {@inheritDoc}
     */
    @Override
    public int compareTo(
            @NotNull final URI that) {
        if (that instanceof URL) {
            return compareTo(that);
        }
        return toURI().compareTo(that.toURI());
    }

    /**
     * Specialized {@link #compareTo(URI)} method for URL.
     *
     * @param that See {@link #compareTo(URI)}.
     * @return See {@link #compareTo(URI)}.
     * @see URL.Comparator
     */
    public int compareTo(
            @NotNull final URL that) {
        return new Comparator().compare(this, that);
    }

    /**
     * {@inheritDoc}
     * <p></p>
     * <a href="https://tools.ietf.org/html/rfc3986#section-6.1">RFC 3986 Uniform Resource Identifier (URI): Generic Syntax: 6.1 Equivalence</a>
     * defines query and fragment parts are not used to evaluate equivalence because two instances of the same URL
     * still addresses the same resource even if the query part and the fragments differ.
     * <p></p>
     * If the query part and the fragment matters to discriminate URL instances,
     * use {@link #toString()} or {@link #toURI()} methods.
     *
     * @param that {@inheritDoc}
     * @return {@inheritDoc}
     */
    @Override
    public boolean equals(
            @NotNull final Object that) {
        if (this == that) return true;
        if (!(that instanceof URL)) return false;
        URL url = (URL) that;
        return port == url.port &&
                scheme.equals(url.scheme) &&
                Objects.equals(user, url.user) &&
                Objects.equals(host, url.host) &&
                path.equals(url.path);
    }

    /**
     * Return a mutable map of the components of the query part.
     * <p></p>
     * The map preserves the order of the components.
     *
     * @return Return a mutable map of the components of the query part.
     */
    @NotNull
    public Map<String, String> getQueryAsMap() {
        final Map<String, String> map = new LinkedHashMap<>();
        if (Text.isFat(query)) {
            map.putAll(QUERY_SPLITTER.split(query));
        }
        return map;
    }

    /**
     * {@inheritDoc}
     *
     * @return {@inheritDoc}
     */
    @Override
    public int hashCode() {
        return Objects.hash(scheme, user, host, port, path, query, fragment);
    }

    /**
     * Return a new URL instance resolving the {@code path} against this {@link #path}.
     * Used to combine the path of {@link zest.data.ZSet#fqn}/{@link zest.data.ZSet#id} with this URL.
     *
     * @param path to resolve against this {@link #path}.
     * @return a new URL instance resolving the {@code path} against this {@link #path}.
     */
    @NotNull
    public URL resolve(
            @NotNull final Path path) {
        final Path resolved = toPath().resolve(path);
        return new URL(scheme, user, password, host, port, resolved.toAbsolutePath().toString(), query, fragment);
    }


    /**
     * Return the normalized path of this URL.
     *
     * @return the normalized path of this URL. If the path is empty this method returns '.'.
     */
    @NotNull
    public Path toPath() {
        if (Text.isFat(path)) {
            final List<String> pathList = PATH_SPLITTER.splitToList(path);
            if (pathList.size() > 1) {
                final List<String> subList = pathList.subList(1, pathList.size());
                return Paths.get(pathList.get(0), subList.toArray(new String[subList.size()])).normalize();
            }
            return Paths.get(pathList.get(0)).normalize();
        }
        return Paths.get(".");
    }

    /**
     * {@inheritDoc}
     * <p></p>
     * The {@link #password} is not expressed in the returned text.
     *
     * @return {@inheritDoc}
     */
    @NotNull
    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("scheme", scheme)
                .add("user", user)
                .add("host", host)
                .add("port", port)
                .add("path", path)
                .add("query", query)
                .add("fragment", fragment)
                .toString();
    }

    /**
     * {@inheritDoc}
     * <p></p>
     * The {@link #password} is not expressed in the returned URI.
     *
     * @return {@inheritDoc}
     */
    @NotNull
    @Override
    public String toURI() {
        final StringBuilder uri = new StringBuilder(scheme).append(':');
        if (Text.isFat(host)) {
            uri.append("//");
            if (Text.isFat(user)) {
                uri.append(user).append('@');
            }
            uri.append(host);
            if (port > NO_PORT) {
                uri.append(':').append(port);
            }
        }
        uri.append(path);
        if (Text.isFat(query)) {
            uri.append('?').append(query);
        }
        if (Text.isFat(fragment)) {
            uri.append('#').append(fragment);
        }
        return uri.toString();
    }

    /**
     * Specialized comparator for {@link URL} objects.
     */
    public static class Comparator implements java.util.Comparator<URL> {

        /**
         * {@inheritDoc}
         * <p></p>
         * Use {@link URL#toPath()} to compare normalized paths.
         * The {@link #password} is not used  to compare URL instances.
         *
         * @param url1 {@inheritDoc}
         * @param url2 {@inheritDoc}
         * @return {@inheritDoc}
         */
        @Override
        public int compare(
                @NotNull final URL url1,
                @NotNull final URL url2) {
            final int schemeDelta = url1.scheme.compareTo(url2.scheme);
            if (schemeDelta == 0) {
                final int userinfoDelta = Text.isFat(url1.user) && Text.isFat(url2.user)
                        ? url1.user.compareTo(url2.user)
                        : Text.isFat(url1.user) ? 1 : Text.isFat(url2.user) ? -1 : 0;
                if (userinfoDelta == 0) {
                    final int hostDelta = Text.isFat(url1.host) && Text.isFat(url2.host)
                            ? url1.host.compareTo(url2.host)
                            : Text.isFat(url1.host) ? 1 : Text.isFat(url2.host) ? -1 : 0;
                    if (hostDelta == 0) {
                        final int portDelta = url1.port - url2.port;
                        if (portDelta == 0) {
                            final int pathDelta = url1.toPath().compareTo(url2.toPath());
                            if (pathDelta == 0) {
                                final int queryDelta = Text.isFat(url1.query) && Text.isFat(url2.query)
                                        ? url1.query.compareTo(url2.query)
                                        : Text.isFat(url1.query) ? 1 : Text.isFat(url2.query) ? -1 : 0;
                                if (queryDelta == 0) {
                                    return Text.isFat(url1.fragment) && Text.isFat(url2.fragment)
                                            ? url1.fragment.compareTo(url2.fragment)
                                            : Text.isFat(url1.fragment) ? 1 : Text.isFat(url2.fragment) ? -1 : 0;
                                }
                                return queryDelta;
                            }
                            return pathDelta;
                        }
                        return portDelta;
                    }
                    return hostDelta;
                }
                return userinfoDelta;
            }
            return schemeDelta;
        }

    } //~ Comparator

}

