package zest.util;

import com.google.common.base.Joiner;
import com.google.common.base.Splitter;
import com.google.common.collect.ImmutableSet;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Iterator;
import java.util.Set;

/**
 * Utility static methods to handle texts.
 * <p></p>
 * Thread safe.
 *
 * @author <a href="mailto:luca,nicola.debiasi@gmail.com">Luca Nicola Debiasi</a>
 */
public class Text {

    private final static String HEX_TOKEN = "hex_";

    private final static Splitter HEX_TOKEN_SPLITTER = Splitter.on(HEX_TOKEN).omitEmptyStrings();

    @NotNull
    public static String behead(
            @Nullable final String text,
            @NotNull final Set<String> prefixSet) {
        for (final String prefix : prefixSet) {
            if (text.startsWith(prefix)) {
                return text.substring(prefix.length());
            }
        }
        return Text.isFat(text) ? text : "";
    }

    @NotNull
    public static String behead(
            @Nullable final String text,
            @NotNull final String prefix) {
        return behead(text, ImmutableSet.of(prefix));
    }

    /**
     * Return decoded text from {code text} where not [0-9A-Za-Z]
     * are encoded in hex form prefixed with {@link Text#HEX_TOKEN}.
     * <p></p>
     * Used to decode names in a form suitable for web services and legal for URI.
     *
     * @param text to decode.
     * @return decoded text from {code text} where not [0-9A-Za-Z]
     * are encoded in hex form prefixed with {@link Text#HEX_TOKEN}..
     */
    @NotNull
    public static String decode(
            @NotNull final String text) {
        final StringBuilder decode = new StringBuilder();
        final Iterator<String> iterator = HEX_TOKEN_SPLITTER.split(text).iterator();
        if (iterator.hasNext()) {
            decode.append(iterator.next());  // Head doesn't begin with an hex expression.
            while (iterator.hasNext()) {
                final String segment = iterator.next();
                final String hex = segment.substring(0, 2);
                final String ascii = hexToAscii(hex);
                decode.append(ascii);
                decode.append(segment.substring(2));
            }
            return decode.toString();
        }
        return text;
    }

    /**
     * Return encoded text where all characters not [0-9A-Za-Z] are encoded in hex form prefixed with '0x'.
     * <p></p>
     * Used to encode names in a form suitable for web services and legal for URI.
     *
     * @param text to encode.
     * @return encoded text where all characters not [0-9A-Za-Z] are encoded in hex form prefixed with '0x'..
     * @see <a href="https://docs.aws.amazon.com/amazondynamodb/latest/developerguide/HowItWorks.NamingRulesDataTypes.html">AWS Naming Rules</a>
     */
    @NotNull
    public static String encode(
            @NotNull final String text) {
        final StringBuilder encode = new StringBuilder();
        for (int i = 0; i < text.length(); i++) {
            final char c = text.charAt(i);
            if (Character.isAlphabetic(c)
                    || Character.isDigit(c)
                    || c == '.'
                    || c == '-'
                    || c == '_'
            ) {
                encode.append(c);
            } else {
                encode.append(String.format("%s%02x", HEX_TOKEN, (int) c));
            }
        }
        return encode.toString();
    }


    /**
     * Return the ASCII representation of {@code hex} interpreted as hexadecimal expression.
     *
     * @param hex hexadecimal expression of text.
     * @return The ASCII representation of {@code hex} interpreted as hexadecimal expression.
     * @see <a = href="https://www.baeldung.com/java-convert-hex-to-ascii">Covert Hex to ASCII</a>
     */
    @NotNull
    public static String hexToAscii(
            @NotNull final String hex) {
        final StringBuilder ascii = new StringBuilder();
        for (int i = 0; i < hex.length(); i += 2) {
            String digit = hex.substring(i, i + 2);
            ascii.append((char) Integer.parseInt(digit, 16));
        }
        return ascii.toString();
    }

    /**
     * Return {@code true} if {@code obj} represents a not empty text not is {code null}.
     *
     * @param obj representing text.
     * @return {@code true} if {@code obj} represents a not empty text not is {code null}.
     */
    public static boolean isFat(
            @Nullable final Object obj) {
        if (!isLong(obj)) {
            return false;
        }
        final CharSequence text = (CharSequence) obj;
        int strLen = text.length();
        for (int i = 0; i < strLen; i++) {
            if (!Character.isWhitespace(text.charAt(i))) {
                return true;
            }
        }
        return false;
    }

    /**
     * Return {@code true} if {@code obj} is not an empty text.
     *
     * @param obj representing text.
     * @return Return {@code true} if {@code obj} is not an empty text.
     */
    public static boolean isLong(
            @Nullable final Object obj) {
        if (obj != null && obj instanceof CharSequence) {
            return ((CharSequence) obj).length() > 0;
        }
        return false;
    }

    /**
     * Return {@code true} if {code text} is {@code null} or empty.
     *
     * @param text to chek if {@code null} or empty.
     * @return {@code true} if {code text} is {@code null} or empty.
     */
    public static boolean isNil(
            @Nullable final CharSequence text) {
        return !isFat(text);
    }

    /**
     * Return {@code text} without any char matching white-spaces.
     *
     * @param text to shrink.
     * @return {@code text} without any char matching {@code charMatcher}.
     */
    @NotNull
    public static String shrinkWhitespaces(
            @NotNull final String text) {
        final StringBuilder dry = new StringBuilder();
        for (int i = 0; i < text.length(); i++) {
            final char c = text.charAt(i);
            if (!Character.isWhitespace(c)) {
                dry.append(c);
            }
        }
        return dry.toString();
    }

    @NotNull
    public static String replaceAndTrim(
            @NotNull final CharSequence text,
            final char splitter,
            final char joiner) {
        return replaceAndTrim(
                text,
                Splitter.on(splitter).trimResults().omitEmptyStrings(),
                Joiner.on(joiner).skipNulls());
    }

    @NotNull
    public static String replaceAndTrim(
            @NotNull final CharSequence text,
            @NotNull final String splitter,
            @NotNull final String joiner) {
        return replaceAndTrim(
                text,
                Splitter.on(splitter).trimResults().omitEmptyStrings(),
                Joiner.on(joiner).skipNulls());
    }

    @NotNull
    public static String replaceAndTrim(
            @NotNull final CharSequence text,
            @NotNull final Splitter splitter,
            @NotNull final Joiner joiner) {
        return joiner.join(splitter.split(text));
    }

}
