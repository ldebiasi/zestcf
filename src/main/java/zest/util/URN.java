package zest.util;

import com.google.common.base.Joiner;
import com.google.common.base.MoreObjects;
import com.google.common.base.Splitter;
import com.google.gson.annotations.SerializedName;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Comparator;
import java.util.Iterator;
import java.util.Objects;

/**
 * Represent and handles correctly the <a href="https://tools.ietf.org/html/rfc8141">Uniform Resource Name</a>
 * <p></p>
 * Immutable.
 */
public class URN implements URI {

    public final static transient Comparator COMPARATOR = new Comparator();

    public final static transient Joiner N_JOINER = Joiner.on(':').skipNulls();

    public final static transient Splitter N_SPITTER = Splitter.on(':').limit(3).trimResults();

    /**
     * Q component joiner.
     */
    public final static transient Joiner.MapJoiner Q_JOINER = Joiner.on(';').withKeyValueSeparator('=');

    /**
     * Q component splitter.
     */
    public final static transient Splitter.MapSplitter Q_SPLITTER = Splitter.on(';').withKeyValueSeparator('=');


    public final static transient String TAG_NID = "nid";

    public final static transient String TAG_NSS = "nss";

    public final static transient String TAG_F_COMPONENTS = "f-component";

    public final static transient String TAG_R_COMPONENTS = "r-component";

    public final static transient String TAG_Q_COMPONENTS = "q-component";


    /**
     * Scheme.
     */
    protected final static String SCHEME = "urn";


    /**
     * Name-space Identifier.
     */
    @NotNull
    @SerializedName(TAG_NID)
    public final String nid;

    /**
     * Name-Space Specific part.
     */
    @NotNull
    @SerializedName(TAG_NSS)
    public final String nss;

    /**
     * R-component.
     */
    @Nullable
    @SerializedName(TAG_R_COMPONENTS)
    public final String r_component;

    /**
     * Q-component.
     */
    @Nullable
    @SerializedName(TAG_Q_COMPONENTS)
    public final String q_component;

    /**
     * Fragment.
     */
    @Nullable
    @SerializedName(TAG_F_COMPONENTS)
    public final String f_component;

    /**
     * See <a href="https://tools.ietf.org/html/rfc8141">Uniform Resource Name</a>
     *
     * @param nid         Name-space identifier.
     * @param nss         Name-Space Specific part.
     * @param r_component R-component.
     * @param q_component Q-component.
     * @param f_component Fragment.
     */
    public URN(
            @NotNull final String nid,
            @NotNull final String nss,
            @Nullable final String r_component,
            @Nullable final String q_component,
            @Nullable final String f_component) {
        this.nid = nid.toLowerCase();
        this.nss = nss;
        this.r_component = r_component;
        this.q_component = q_component;
        this.f_component = f_component;
    }

    @NotNull
    public static URN of(
            @NotNull String uri)
            throws IllegalArgumentException {
        final int f_start = uri.indexOf('#');
        String f_component = null;
        if (f_start > -1) {
            f_component = uri.substring(f_start + 1);
            uri = uri.substring(0, f_start);
        }
        String q_component = null;
        final int q_start = uri.indexOf("?=");
        if (q_start > -1) {
            q_component = uri.substring(q_start + 1);
            uri = uri.substring(0, q_start);
        }
        String r_component = null;
        final int r_start = uri.indexOf("?+");
        if (r_start > -1) {
            r_component = uri.substring(r_start + 1);
            uri = uri.substring(0, r_start);
        }
        final Iterator<String> iter = N_SPITTER.split(uri).iterator();
        if (iter.hasNext()) {
            if (SCHEME.equalsIgnoreCase(iter.next())) {
                if (iter.hasNext()) {
                    final String nid = iter.next();
                    if (iter.hasNext()) {
                        final String nss = iter.next();
                        return new URN(nid, nss, r_component, q_component, f_component);
                    }
                    throw new IllegalArgumentException(String.format("uri = %s hasn't uri:%s:<NSS> part", uri, nid));
                }
                throw new IllegalArgumentException(String.format("uri = %s hasn't uri:<NID> part", uri));
            }
            throw new IllegalArgumentException(String.format("uri = %s hasn't uri: scheme", uri));
        }
        throw new IllegalArgumentException(String.format("uri = %s illegal format", uri));
    }

    /**
     * {@inheritDoc}
     *
     * @param that {@inheritDoc}
     * @return {@inheritDoc}
     */
    @Override
    public int compareTo(
            @NotNull final URI that) {
        if (that instanceof URN) {
            return compareTo((URN) that);
        }
        return toURI().compareTo(that.toURI());
    }

    /**
     * Specialized {@link #compareTo(URI)} method for URN.
     *
     * @param that See {@link #compareTo(URI)}.
     * @return See {@link #compareTo(URI)}.
     * @see URN.Comparator
     */
    public int compareTo(
            @NotNull final URN that) {
        return new Comparator().compare(this, that);
    }

    /**
     * {@inheritDoc}
     *
     * @param that {@inheritDoc}
     * @return {@inheritDoc}
     */
    @Override
    public boolean equals(Object that) {
        if (this == that) return true;
        if (!(that instanceof URN)) return false;
        URN urn = (URN) that;
        return nid.equals(urn.nid) &&
                nss.equals(urn.nss) &&
                Objects.equals(r_component, urn.r_component) &&
                Objects.equals(q_component, urn.q_component) &&
                Objects.equals(f_component, urn.f_component);
    }

    /**
     * {@inheritDoc}
     *
     * @return {@inheritDoc}
     */
    @NotNull
    @Override
    public String getScheme() {
        return SCHEME;
    }

    /**
     * {@inheritDoc}
     *
     * @return {@inheritDoc}
     */
    @Override
    public int hashCode() {
        return Objects.hash(nid, nss, r_component, q_component, f_component);
    }

    /**
     * {@inheritDoc}
     *
     * @return {@inheritDoc}
     */
    @NotNull
    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .omitNullValues()
                .add(TAG_NID, nid)
                .add(TAG_NSS, nss)
                .add(TAG_R_COMPONENTS, r_component)
                .add(TAG_Q_COMPONENTS, q_component)
                .add(TAG_F_COMPONENTS, f_component)
                .toString();
    }

    /**
     * {@inheritDoc}
     *
     * @return {@inheritDoc}
     */
    @NotNull
    @Override
    public String toURI() {
        final StringBuilder uri = new StringBuilder(SCHEME).append(':');
        uri.append(nid).append(':');
        uri.append(nss);
        if (Text.isFat(r_component)) {
            uri.append("?+").append(r_component);
        }
        if (Text.isFat(q_component)) {
            uri.append("?=").append(q_component);
        }
        if (Text.isFat(f_component)) {
            uri.append("#").append(f_component);
        }
        return uri.toString();
    }

    /**
     * Specialized comparator for {@link URN} objects.
     */
    public static class Comparator implements java.util.Comparator<URN> {

        /**
         * {@inheritDoc}
         *
         * @param urn1 {@inheritDoc}
         * @param urn2 {@inheritDoc}
         * @return {@inheritDoc}
         */
        @Override
        public int compare(
                @NotNull final URN urn1,
                @NotNull final URN urn2) {
            final int nidDelta = urn1.nid.compareTo(urn2.nid);
            if (nidDelta == 0) {
                return urn1.nss.compareTo(urn2.nss);
            }
            return nidDelta;
        }

    } //~ Comparator

}
