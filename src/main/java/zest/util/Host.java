package zest.util;

import org.jetbrains.annotations.NotNull;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.InetAddress;
import java.net.URL;
import java.net.UnknownHostException;
import java.util.Scanner;
import java.util.UUID;

public class Host {

    public static final String AWS_CHECK_IP_SERVICE = "http://checkip.amazonaws.com";

    @NotNull
    public static String name() throws UnknownHostException {
        try {
            return InetAddress.getLocalHost().getHostName();
        } catch (UnknownHostException e) {
            try (final Scanner scanner = new Scanner(Runtime.getRuntime()
                    .exec("hostname")
                    .getInputStream())
                    .useDelimiter("\\A")) {
                if (scanner.hasNext()) {
                    return scanner.next();
                }
            } catch (IOException e1) {
                final String os = System.getProperty("os.name").toLowerCase();
                if (os.contains("win")) {
                    final String hostName = System.getenv("COMPUTERNAME");
                    if (Text.isFat(hostName)) {
                        return hostName;
                    }
                } else if (os.contains("nix") || os.contains("nux") || os.contains("mac os x")) {
                    final String hostName = System.getenv("HOSTNAME");
                    if (Text.isFat(hostName)) {
                        return hostName;
                    }
                }
            }
        }
        throw new UnknownHostException();
    }

    @NotNull
    public static String privateIP() throws UnknownHostException {
        return InetAddress.getLocalHost().getHostAddress();
    }

    @NotNull
    public static String publicIP() throws UnknownHostException {
        try (final BufferedReader bufferedReader = new BufferedReader(
                new InputStreamReader(
                        new URL(AWS_CHECK_IP_SERVICE).openStream()))) {
            return bufferedReader.readLine();
        } catch (@NotNull final IOException e) {
            return privateIP();
        }
    }
}
