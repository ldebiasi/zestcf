package zest.util;

import org.jetbrains.annotations.NotNull;

/**
 * Represents and handles correctly the
 * <a href="https://tools.ietf.org/html/rfc3986">Uniform Resource Identifier</a>.
 * <p></p>
 * Immutable.
 */
public interface URI extends Comparable<URI> {

    /**
     * Return the scheme part:
     * from the beginning of {@code uri} to the first ':' excluded.
     *
     * @param uri Uniform Resource Identifier.
     * @return Return the scheme part.
     * @throws IllegalArgumentException if {@code uri} hasn't a scheme part.
     */
    @NotNull
    static String schemeOf(
            @NotNull final String uri)
            throws
            IllegalArgumentException {
        final StringBuilder scheme = new StringBuilder();
        if (Character.isLetter(uri.charAt(0))) {
            for (int i = 0; i < uri.length(); i++) {
                final char c = uri.charAt(i);
                if (Character.isLetterOrDigit(c) || c == '+' || c == '-' || c == '.') {
                    scheme.append(c);
                } else if (c == ':') {
                    return scheme.toString();
                }
            }
        }
        throw new IllegalArgumentException(String.format("(uri = %s) hasn't scheme", uri));
    }

    /**
     * Return the scheme part with its sub-scheme components:
     * for URL this is from the beginning of URI to authority or path;
     * for URN this from the beginning to query or fragment with the assumption NID and NSS parts are
     * sub-schemes of {@code urn:} scheme.
     *
     * @param uri Uniform Resource Identifier.
     * @return Return the scheme part.
     * @throws IllegalArgumentException if {@code uri} hasn't a scheme part
     */
    @NotNull
    static String schemeAndSubsOf(
            @NotNull final String uri)
            throws IllegalArgumentException {
        final StringBuilder schemes = new StringBuilder(schemeOf(uri));
        try {
            while (schemes.length() < uri.length()) {
                schemes.append(':').append(schemeOf(uri.substring(schemes.length())));
            }
        } catch (IllegalArgumentException expected) {
            if (schemes.charAt(schemes.length() - 1) == ':') {
                schemes.deleteCharAt(schemes.length() - 1);
            }
        }
        return schemes.toString();
    }

    /**
     * Return the scheme specific part excluded sub-scheme components,
     *
     * @param uri Uniform Resource Identifier.
     * @return Return the inner scheme specific part excluded sub-scheme components.
     * @throws IllegalArgumentException if {@code uri} hasn't a scheme part.
     */
    @NotNull
    static String innerSSPOf(
            @NotNull final String uri
    ) throws IllegalArgumentException {
        final String scheme = schemeAndSubsOf(uri);
        int start = scheme.length() + 1; // Consider ':' after scheme.
        if (uri.substring(start).startsWith("//")) {
            start += 2;
        }
        return uri.substring(start);
    }

    /**
     * Return the scheme specific part included sub-scheme components,
     * from the first character after the first ':' to the end of URI.
     *
     * @param uri Uniform Resource Identifier.
     * @return Return the outer scheme=specific part, included sub-scheme components.
     * @throws IllegalArgumentException if {@code uri} hasn't a scheme part.
     */
    @NotNull
    static String outerSSPOf(
            @NotNull final String uri
    ) throws IllegalArgumentException {
        final String scheme = schemeOf(uri);
        int start = scheme.length() + 1; // Consider ':' after scheme.
        if (uri.substring(start).startsWith("//")) {
            start += 2;
        }
        return uri.substring(start);
    }

    /**
     * Return the scheme specific part excluded sub-scheme components,
     *
     * @return Return the inner scheme specific part excluded sub-scheme components.
     * @throws IllegalArgumentException if {@code uri} hasn't a scheme part.
     */
    @NotNull
    default String getInnerSSP() throws IllegalArgumentException {
        return innerSSPOf(toURI());
    }

    /**
     * Return the scheme specific part included sub-scheme components,
     * from the first character after the first ':' to the end of URI.
     *
     * @return Return the outer scheme=specific part, included sub-scheme components.
     * @throws IllegalArgumentException if {@code uri} hasn't a scheme part.
     */
    @NotNull
    default String getOuterSSP() throws IllegalArgumentException {
        return outerSSPOf(toURI());
    }

    /**
     * Return the scheme part:
     * from the beginning of URI to the first ':' excluded.
     *
     * @return Return the scheme part.
     * @throws IllegalArgumentException if URI hasn't a scheme part, this shouldn't happen never.
     */
    @NotNull
    default String getScheme()
            throws IllegalArgumentException {
        return schemeOf(toURI());
    }

    /**
     * Return the scheme part with its sub-scheme components:
     * for URL from the beginning of URI to authority or path;
     * for URN from the beginning to query or fragment with the assumption NID and NSS parts are
     * sub-schemes of {@code urn:}.
     *
     * @return Return the scheme part.
     * @throws IllegalArgumentException if URI hasn't a scheme part, this shouldn't happen never.
     */
    @NotNull
    default String getSchemeAndSubs()
            throws IllegalArgumentException {
        return schemeAndSubsOf(toURI());
    }

    /**
     * Return a legal URI expression.
     *
     * @return a legal URI expression
     */
    @NotNull
    String toURI();

}
