package zest.util;

/**
 * Extend the {@link org.apache.http.HttpStatus} interface to include new status code or
 * specific status codes for this project.
 */
public class HttpStatus implements org.apache.http.HttpStatus {

    /**
     * {@code 226 IM Used} (HTTP/1.1 - RFC 3229)
     */
    public static final int SC_IM_USED = 226;

}
