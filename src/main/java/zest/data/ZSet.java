package zest.data;

import com.google.common.base.Joiner;
import com.google.common.base.MoreObjects;
import com.google.common.collect.Maps;
import com.google.common.collect.Queues;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import com.google.gson.annotations.SerializedName;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import zest.util.Text;

import java.util.*;
import java.util.function.Function;
import java.util.function.ToDoubleFunction;
import java.util.function.ToIntFunction;
import java.util.function.ToLongFunction;

public class ZSet implements Comparable<ZSet> {

    public final static transient Comparator COMPARATOR = new ZSet.Comparator();

    protected final static transient Joiner FQN_JOINER = Joiner.on('.');

    public final static transient String TAG_FQN = "fqn";

    public final static transient String TAG_ID = "id";

    public final static transient String TAG_MAP = "map";

    /**
     * It's correct to consider FQN the table of a relational database.
     */
    @NotNull
    @SerializedName(TAG_FQN)
    public final FQN fqn;

    @NotNull
    @SerializedName(TAG_ID)
    public final String id;

    @NotNull
    @SerializedName(TAG_MAP)
    public final Map<String, Element> map;

    protected ZSet(
            @NotNull final FQN fqn,
            @NotNull final String id,
            @NotNull final Map<String, Element> map) {
        this.fqn = fqn;
        this.id = id;
        this.map = map;
    }

    @NotNull
    public static Builder fqn(
            @NotNull final FQN fqn) {
        return new Builder(fqn);
    }

    /**
     * {@inheritDoc}
     *
     * @param that {@inheritDoc}
     * @return {@inheritDoc}
     */
    @Override
    public int compareTo(
            @NotNull final ZSet that) {
        return new Comparator().compare(this, that);
    }

    /**
     * {@inheritDoc}
     *
     * @param that {@inheritDoc}
     * @return {@inheritDoc}
     */
    @Override
    public boolean equals(Object that) {
        if (this == that) return true;
        if (!(that instanceof ZSet)) return false;
        ZSet zset = (ZSet) that;
        return fqn.equals(zset.fqn) &&
                id.equals(zset.id) &&
                map.equals(zset.map);
    }

    public Element getElement(@NotNull final String property) throws NoSuchElementException {
        if (isMember(property)) {
            return map.get(property);
        }
        throw new NoSuchElementException(String.format("(property = %s) isn't member of this %s", property, this));
    }

    /**
     * Return the set of properties (or property's name) on this zset.
     *
     * @return the set of properties (or property's name) on this zset.
     */
    @NotNull
    public Set<String> getProperties() {
        return map.keySet();
    }

    /**
     * {@inheritDoc}
     *
     * @return {@inheritDoc}
     */
    @Override
    public int hashCode() {
        return Objects.hash(fqn, id, map);
    }

    /**
     * Return {@code true} if {@link ZSet#id} has a value not blacnk or empty.
     *
     * @return {@code true} if {@link ZSet#id} has a value not blacnk or empty.
     */
    public boolean isIdentifiable() {
        return Text.isFat(id);
    }

    public boolean isMember(
            @NotNull final String property) {
        return map.get(property) != null;
    }

    @NotNull
    public JsonObject toJSON() {
        return (JsonObject) Codec.DEFAULT.gson.toJsonTree(this);
    }

    /**
     * {@inheritDoc}
     *
     * @return {@inheritDoc}
     */
    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .omitNullValues()
                .add(TAG_FQN, fqn)
                .add(TAG_ID, id)
                .add(TAG_MAP, map)
                .toString();
    }

    public static class Builder {

        @NotNull
        protected final FQN fqn;

        @Nullable
        protected volatile Map<String, Element> map = Maps.newTreeMap();

        protected Builder(
                @NotNull final FQN fqn) {
            this.fqn = fqn;
        }

        /**
         * Flatten the {@code zset} to a single layer of {@link Element} wraps of {@link JsonPrimitive}
         * and add it to this {@link #map}.
         *
         * @param zset to flatten.
         * @return this.
         */
        @NotNull
        public Builder flatten(
                final @NotNull ZSet zset) {
            final Deque<String> stack = Queues.newArrayDeque();
            for (final String property : zset.getProperties()) {
                stack.addLast(property);
                flatten(stack, zset.getElement(property).toJSON());
                stack.removeLast();
            }
            return this;
        }

        /**
         * Flatten the {@code jsonElement} to a single layer of {@link Element} wraps of {@link JsonPrimitive}
         * and add them to this {@link #map}.
         *
         * @param stack       of member name's components joined by {@link #FQN_JOINER} to build the property name
         *                    of leaf {@link Element}.
         * @param jsonElement to flatten.
         * @see #flatten(ZSet)
         */
        @NotNull
        protected void flatten(
                @NotNull final Deque<String> stack,
                @NotNull final JsonElement jsonElement) {
            if (jsonElement.isJsonArray()) {
                final JsonArray jsonArray = jsonElement.getAsJsonArray();
                for (int i = 0; i < jsonArray.size(); i++) {
                    stack.addLast(String.valueOf(i));
                    flatten(stack, jsonArray.get(i));
                    stack.removeLast();
                }
            } else if (jsonElement.isJsonObject()) {
                final JsonObject jsonObject = jsonElement.getAsJsonObject();
                for (final Map.Entry<String, JsonElement> entry : jsonObject.entrySet()) {
                    stack.addLast(entry.getKey());
                    flatten(stack, entry.getValue());
                    stack.removeLast();
                }
            } else {
                final JsonPrimitive jsonPrimitive = jsonElement.getAsJsonPrimitive();
                if (jsonPrimitive.isBoolean()) {
                    map.put(FQN_JOINER.join(stack), Element.from(Boolean.class, jsonElement).set());
                } else if (jsonPrimitive.isNumber()) {
                    map.put(FQN_JOINER.join(stack), Element.from(Number.class, jsonElement).set());
                } else if (jsonPrimitive.isString()) {
                    map.put(FQN_JOINER.join(stack), Element.from(String.class, jsonElement).set());
                }
            }
        }

        @NotNull
        public Builder put(
                @NotNull final ZSet zset) {
            map.putAll(zset.map);
            return this;
        }

        @NotNull
        public Builder put(
                @NotNull final String property,
                @NotNull final Element element) {
            if (!element.isNull()) {
                map.put(property, element);
            }
            return this;
        }

        @NotNull
        public ZSet set(
                @NotNull final String id) {
            return new ZSet(fqn, id, map);
        }

        public ZSet set() {
            return new ZSet(fqn, "", map);
        }


    } //~ Builder

    public static class Comparator implements java.util.Comparator<ZSet> {

        @Override
        public int compare(
                @NotNull final ZSet z1,
                @NotNull final ZSet z2) {
            final String j1 = Codec.DEFAULT.gson.toJson(z1);
            final String j2 = Codec.DEFAULT.gson.toJson(z2);
            return j1.compareTo(j2);
        }

        @Override
        public java.util.Comparator<ZSet> reversed() {
            return null;
        }

        @Override
        public java.util.Comparator<ZSet> thenComparing(java.util.Comparator<? super ZSet> other) {
            return null;
        }

        @Override
        public <U> java.util.Comparator<ZSet> thenComparing(Function<? super ZSet, ? extends U> keyExtractor, java.util.Comparator<? super U> keyComparator) {
            return null;
        }

        @Override
        public <U extends Comparable<? super U>> java.util.Comparator<ZSet> thenComparing(Function<? super ZSet, ? extends U> keyExtractor) {
            return null;
        }

        @Override
        public java.util.Comparator<ZSet> thenComparingInt(ToIntFunction<? super ZSet> keyExtractor) {
            return null;
        }

        @Override
        public java.util.Comparator<ZSet> thenComparingLong(ToLongFunction<? super ZSet> keyExtractor) {
            return null;
        }

        @Override
        public java.util.Comparator<ZSet> thenComparingDouble(ToDoubleFunction<? super ZSet> keyExtractor) {
            return null;
        }

    } //~ Comparator

}
