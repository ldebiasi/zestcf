package zest.data;

import com.google.common.base.Joiner;
import com.google.common.collect.ImmutableList;
import com.google.gson.JsonArray;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import zest.util.Text;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

/**
 * Represent a Full Qualified Name-space.
 * FQN is used to express the Full Qualified Name of the type of {@link Element} instances.
 * FQN is used to express the Full Qualified Namespace of the {@link ZSet}, where {@link ZSet#id} is unique.
 *
 * @author
 */
public class FQN implements Comparable<FQN> {

    public final static transient String SEPARATOR = "/";

    public final static transient Joiner JOINER = Joiner.on(SEPARATOR);

    /**
     * Return a new FQN.
     *
     * @param partList components of the full qualified name.
     */
    protected FQN(
            @NotNull final ImmutableList<String> partList) {
        this.partList = partList;
    }

    /**
     * Immutable list of the the FQN part, hence accessible.
     */
    public final ImmutableList<String> partList;

    /**
     * Return a new FQN.
     *
     * @param path to represent as FQN.
     * @return a new FQN.
     * @throws IllegalArgumentException if {@code path} is empty.
     */
    @NotNull
    public static FQN of(
            @NotNull final Path path) {
        final ImmutableList.Builder<String> listBuilder = new ImmutableList.Builder<>();
        if (path.getNameCount() > 0) {
            for (int i = 0; i < path.getNameCount(); i++) {
                final String part = path.getName(i).toFile().getName();
                listBuilder.add(part);
            }
            return new FQN(listBuilder.build());
        }
        throw new IllegalArgumentException("(path) can't be empty");
    }

    /**
     * Return a new FQN.
     *
     * @param root  component of FQN must not be null or empty.
     * @param parts components of the full qualified name.
     * @return a new FQN.
     * @throws IllegalArgumentException if {@code root} is blank or empty.
     */
    @NotNull
    public static FQN of(
            @NotNull final String root,
            @Nullable final String... parts) throws IllegalArgumentException {
        if (Text.isFat(root)) {
            final ImmutableList.Builder listBuilder = ImmutableList.builder().add(root);
            if (parts != null) {
                listBuilder.addAll(Arrays.asList(parts));
            }
            return new FQN(listBuilder.build());
        }
        throw new IllegalArgumentException("(root) can't be an blank or empty string");
    }

    /**
     * Return a new FQN for the {@code cls} canonical name.
     *
     * @param cls {@link Class#getCanonicalName()} used as FQN.
     * @return Return a new FQN.
     */
    @NotNull
    public static FQN of(
            @NotNull final Class cls) {
        return of(cls.getCanonicalName(), (String[]) null);
    }

    /**
     * Return a new FQN having the {@code root} component in front of {@code fqn} parts.
     *
     * @param root part to prefix {@code fqn}.
     * @param fqn  to append to {@code root}.
     * @return a new FQN having the {@code root} component in front of {@code fqn} parts.
     */
    @NotNull
    public static FQN of(
            @NotNull final String root,
            @NotNull final FQN fqn) {
        final ImmutableList.Builder<String> listBuilder = new ImmutableList.Builder<>();
        listBuilder.add(root);
        listBuilder.addAll(fqn.partList);
        return new FQN(listBuilder.build());
    }

    /**
     * Return a new FQN having {@code fqn} parent of {@code name}.
     *
     * @param fqn  parent FQN.
     * @param name to be added to {@code fqn}.
     * @return a new FQN having {@code fqn} parent of {@code name}.
     */
    @NotNull
    public static FQN of(
            @NotNull final FQN fqn,
            @NotNull final String name) {
        final ImmutableList.Builder<String> listBuilder = new ImmutableList.Builder<>();
        listBuilder.addAll(fqn.partList);
        listBuilder.add(name);
        return new FQN(listBuilder.build());
    }

    @Override
    public int compareTo(@NotNull final FQN fqn) {
        return toPath().compareTo(fqn.toPath());
    }

    /**
     * Return the last part of FQN.
     *
     * @return the last part of FQN.
     * @see #getName()
     */
    @NotNull
    public String getName() {
        return getName(0);
    }

    /**
     * Return the last part of this FQN as string with parts separated by {@link #SEPARATOR}
     *
     * @param start highest part of the FQN to consider to build the name, from 0 if < 1.
     * @return the last part of this FQN as string with parts separated by {@link #SEPARATOR}
     */
    @NotNull
    public String getName(int start) {
        return JOINER.join(partList.subList(start > 0 ? start : 0, getSize()));
    }

    /**
     * Return the parts of FQN.
     *
     * @return the parts of FQN.
     */
    @NotNull
    public List<String> getPartList() {
        return partList;
    }

    /**
     * Return the root (first) part or FQN.
     *
     * @return the root (first) part or FQN.
     */
    @NotNull
    public String getRoot() {
        return partList.get(0);
    }

    /**
     * Return the number of parts of this FQN.
     *
     * @return the number of parts of this FQN.
     */
    public int getSize() {
        return partList.size();
    }

    /**
     * Return this FQN as a JSON array pf its FQN parts.
     *
     * @return this FQN as a JSON array pf its FQN parts.
     */
    @NotNull
    public JsonArray toJSON() {
        final JsonArray jsonArray = new JsonArray();
        for (final String part : partList) {
            jsonArray.add(part);
        }
        return jsonArray;
    }

    /**
     * Return the {@link Path} expressing this FQN.
     *
     * @return the {@link Path} expressing this FQN.
     */
    @NotNull
    public Path toPath() {
        if (partList.size() > 1) {
            final ImmutableList<String> partSublist = this.partList.subList(1, this.partList.size());
            final String[] parts = partSublist.toArray(new String[partSublist.size()]);
            return Paths.get(getRoot(), parts);
        } else {
            return Paths.get(getRoot());
        }
    }

    /**
     * Return the {@link Path} made from this FQN and the file {@code name}.
     * <p></p>
     * Handy method to get a path from {@link ZSet#fqn} and {@link ZSet#id}.
     *
     * @param name file name to attribute to path expressed by this FQN.
     * @return the {@link Path} made from this FQN and the file {@code name}.
     */
    public Path toPath(
            @NotNull final String name) {
        return of(this, name).toPath();
    }

    /**
     * Return the representation of the FQN as a JSON array of its parts.
     *
     * @return the representation of the FQN as a JSON array of its parts.
     */
    @NotNull
    @Override
    public String toString() {
        return toJSON().toString();
    }

    /**
     * Return the FQN in  encoded to be compatible with AWS name conventions.
     * Dot '.' is used as part separator.
     * Parts encoded with {@link Text#encode(String)}.
     *
     * @return the FQN in  encoded to be compatible with AWS name conventions.
     */
    @NotNull
    @Deprecated
    public String toAWSName() {
        final Iterator<String> iterator = partList.iterator();
        final StringBuilder exp = new StringBuilder(Text.encode(iterator.next()));
        while (iterator.hasNext()) {
            exp.append('.').append(Text.encode(iterator.next()));
        }
        return exp.toString();
    }


}
