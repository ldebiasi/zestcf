package zest.data;

import org.jetbrains.annotations.NotNull;

/**
 * This element represents a constrain the Service has to respect.
 * The {@link #exp} expresses the condition in SQL.
 */
public enum Constraint {

    /**
     * Condition the this named element to be true for the predicate expressed by {@link },
     * {@link #logic} and {@link #negative}.
     */
    CH("CHECK"),

    /**
     * Set this element value, represented by {@link #json} as default value for operations
     * invoving {@link ZSet} instances where this named element value is missing.
     */
    DF("DEFAULT"),

    /**
     * Relate two {@link ZSet#fqn}-space.
     */
    FK("FOREIGN KEY"),

    /**
     * todo: for no-sql database, consider PK instead.
     */
    HK("HASH KEY"),

    /**
     * Index this named element.
     */
    IK("INDEX KEY"),

    /**
     * Restrict the element to be void in the {@link ZSet#fqn}-space.
     */
    NN("NOT NULL"),

    /**
     * Uniquely identify each element in the {@link ZSet#fqn}-space.
     */
    PK("PRIMARY KEY"),

    /**
     * Ensures that a field or column will only have unique values.
     * The element will not have duplicate data in the {@link ZSet#fqn}-space.
     */
    UN("UNIQUE");

    /**
     * The condition expressed in SQL.
     */
    @NotNull
    public String exp;

    /**
     * Return a new condition which is {@code exp} in SQL.
     *
     * @param exp The condition expressed in SQL.
     */
    Constraint(
            @NotNull final String exp) {
        this.exp = exp;
    }

} //~
