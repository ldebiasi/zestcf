package zest.data;

import com.google.gson.JsonElement;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public abstract class Condition {

    public final @NotNull JsonElement arg;

    public final @NotNull Op op;

    public final boolean positive;

    public final @Nullable Condition next;

    protected Condition(
            final @NotNull JsonElement arg,
            final @NotNull Op op,
            final boolean positive,
            final @Nullable Condition next
    ) {
        this.arg = arg;
        this.op = op;
        this.positive = positive;
        this.next = next;
    }

    public boolean hasNext() {
        return next != null;
    }

    public static class And extends Condition {

        public And(
                final @NotNull JsonElement arg,
                final @NotNull Op op,
                final boolean positive,
                final @Nullable Condition next
        ) {
            super(arg, op, positive, null);
        }

    } //~ And

    public static class Or extends Condition {

        public Or(
                final @NotNull JsonElement arg,
                final @NotNull Op op,
                final boolean positive,
                final @Nullable Condition next
        ) {
            super(arg, op, positive, null);
        }

    } //~ Or

    public static And and(
            final @NotNull JsonElement arg,
            final @NotNull Op op
    ) {
        return new And(arg, op, true, null);
    }

    public static Or or(
            final @NotNull JsonElement arg,
            final @NotNull Op op
    ) {
        return new Or(arg, op, true, null);
    }

    public static Condition not(
            final @NotNull And and
    ) {
        return new And(and.arg, and.op, !and.positive, and.next);
    }

    public static Condition not(
            final @NotNull Or or
    ) {
        return new And(or.arg, or.op, !or.positive, or.next);
    }

    public And and(
            final Condition condition
    ) {
        return new And(arg, op, positive, condition);
    }

    public Or or(
            final Condition condition
    ) {
        return new Or(arg, op, positive, condition);
    }

    public enum Op {

        /**
         * Less Then
         */
        LT,

        /**
         * Less or Equal then
         */
        LTE,

        /**
         * Equal to
         */
        EQ,

        /**
         * Greater Then
         */
        GT,

        /**
         * Greater or Equal then
         */
        GTE,

        /**
         * Begin with.
         */
        BW,

        /**
         * Match pattern.
         */
        MP;

    } //~ Op
}
