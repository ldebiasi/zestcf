package zest.data;

import com.google.common.collect.ImmutableMap;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import zest.io.Service;
import zest.util.Text;
import zest.util.URL;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.Instant;

/**
 * Represent the op of the operation done on the operands in the {@link Service}'s stack.
 */
public enum Op {

    /**
     * Create the resource from the last {@link ZSet} operand in {@link Service} stack.
     */
    C("CREATE"),

    /**
     * Read the resource identified by the last {@link ZSet} operand in {@link Service} stack.
     */
    R("READ"),

    /**
     * Update the resource from the last {@link ZSet} operand in {@link Service} stack.
     */
    U("UPDATE"),

    /**
     * Delete the resource identified by the last {@link ZSet} operand in {@link Service} stack.
     */
    D("DELETE");

    /**
     * OP key in query part of operation id got calling {@link #id(Service, FQN, String)}
     */
    public final static String OP = "op";

    /**
     * Thread identifier.
     */
    public final static String TID = "tid";

    /**
     * Op's expression.
     */
    @NotNull
    public final String op;

    /**
     * Return a new op having {@code op} expression.
     *
     * @param op
     */
    Op(
            @NotNull final String op) {
        this.op = op;
    }

    /**
     * Return a {@link zest.util.URL} for this operation on the resource identified by {@code fqn} and {@code id}.
     * <p></p>
     * The returned URL has the following format.
     * <pre>
     *     {@code <scheme>:<host>[:<port>]/<path>?tid=<thread_id>;op=<op>#<iso_8601_now>}
     * </pre>
     *
     * @param service processing the operation.
     * @return a {@link zest.util.URL} for this operation on {@code ZSet} operand processed by the {@code service}.
     */
    @NotNull
    public URL id(
            @NotNull final Service service) {
        return id(service, null);
    }

    /**
     * Return a {@link zest.util.URL} for this operation on the resource identified by {@code fqn} and {@code id}.
     * <p></p>
     * The returned URL has the following format.
     * <pre>
     *     {@code <scheme>:<host>[:<port>]/<path>?tid=<thread_id>;op=<op>#<iso_8601_now>}
     * </pre>
     *
     * @param service processing the operation.
     * @param fqn     path in resulting URL.
     * @return a {@link zest.util.URL} for this operation on {@code ZSet} operand processed by the {@code service}.
     */
    @NotNull
    public URL id(
            @NotNull final Service service,
            @Nullable final FQN fqn) {
        return id(service, fqn, null);
    }

    /**
     * Return a {@link zest.util.URL} for this operation on the resource identified by {@code fqn} and {@code id}.
     * <p></p>
     * The returned URL has the following format.
     * <pre>
     *     {@code <scheme>:<host>[:<port>]/<path>?tid=<thread_id>;op=<op>#<iso_8601_now>}
     * </pre>
     *
     * @param service processing the operation.
     * @param fqn     path in resulting URL.
     * @param id      last path component in resulting URL.
     * @return a {@link zest.util.URL} for this operation on {@code ZSet} operand processed by the {@code service}.
     */
    @NotNull
    public URL id(
            @NotNull final Service service,
            @Nullable final FQN fqn,
            @Nullable final String id) {
        final Long tid = Thread.currentThread().getId();
        final Path path;
        if (fqn != null) {
            if (Text.isFat(id)) {
                path = Paths.get(service.url.toPath().resolve(fqn.toPath()).toString(), id);
            } else {
                path = service.url.toPath().resolve(fqn.toPath());
            }
        } else {
            path = service.url.toPath();
        }
        return new URL(
                service.url.scheme,
                service.url.user,
                service.url.password,
                service.url.host,
                service.url.port,
                path.toString(),
                ImmutableMap.of(TID, tid.toString(), OP, name()),
                Instant.now().toString());
    }

}
