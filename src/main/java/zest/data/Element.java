package zest.data;

import com.google.common.base.MoreObjects;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import com.google.gson.annotations.SerializedName;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.math.BigDecimal;
import java.util.EnumSet;
import java.util.Objects;

/**
 * Represents the datum of {@code T} type.
 * The datum is represented as {@link #json} representation (one can think it as the value of the datum)
 * and the semantic {@link #type} of attribute a meaning to the JSON representation.
 * This class provides semantics to apply a predicate on element.
 *
 * @param <T> Element's type.
 * @see <a href="https://www.json.org/json-en.html">https://www.json.org/json-en.html</a>
 */
public class Element<T> implements Comparable<Element<T>> {

    /**
     * Tag the JSON representation of this element.
     */
    public final static transient String TAG_JSON = "json";

    /**
     * Tag the type of this element.
     */
    public final static transient String TAG_TYPE = "type";

    /**
     * The type of this element.
     */
    @NotNull
    @SerializedName(TAG_TYPE)
    public final Class<T> type;

    /**
     * The JSON representation of this element.
     */
    @NotNull
    @SerializedName(TAG_JSON)
    public final JsonElement json;

    /**
     * Conditions expressed about this element as parameter of the condition.
     */
    @Nullable
    public final transient Condition condition;

    /**
     * Rules used to limit the type of data that can this element can be,
     * to maintain the accuracy and integrity of the data inside the {@link ZSet#fqn}-space.
     */
    @Nullable
    public final transient EnumSet<Constraint> constraints;

    /**
     * Return a new element with...
     *
     * @param type        element's type;
     * @param json        JSON representation of element value;
     * @param constraints rules to respect to maintain datum integrity;
     */
    protected Element(
            @NotNull final Class<T> type,
            @NotNull final JsonElement json,
            @Nullable final Condition condition,
            @Nullable final EnumSet<Constraint> constraints
    ) {
        this.type = type;
        this.json = json;
        this.condition = condition;
        this.constraints = constraints;
    }

    /**
     * Compare two {@link JsonArray} instances.
     *
     * @param a1 operand;
     * @param a2 operand;
     * @return the size difference between the arrays,
     * else use {@link #deltaOf(JsonElement, JsonElement)} to compare
     * the arrays element by element until some element differs.
     */
    protected static int deltaOf(
            @NotNull final JsonArray a1,
            @NotNull final JsonArray a2) {
        int deltaSize = a1.size() - a2.size();
        if (deltaSize == 0) {
            for (int i = 0; i < a1.size(); i++) {
                int delta = deltaOf(a1.get(i), a2.get(i));
                if (delta != 0) {
                    return delta;
                }
            }
        }
        return deltaSize;
    }

    /**
     * Compare two {@link JsonObject} instances.
     *
     * @param e1 operand;
     * @param e2 operand;
     * @return according the following rules:
     * <ul>
     *     <li>{@link Integer#MIN_VALUE} if {code e1} is null;</li>
     *     <li>{@link Integer#MAX_VALUE} if {code e2} is null;</li>
     *     <li>Call {@link #deltaOf(JsonArray, JsonArray)} if operands are arrays;</li>
     *     <li>{@code e1} - {code e2} if operands are numbers</li>
     *     <li>else the lexicographic difference of the JSON textual representation of operands.</li>
     * </ul>
     */
    protected static int deltaOf(
            @NotNull final JsonElement e1,
            @NotNull final JsonElement e2) {
        if (e1.isJsonNull()) {
            return Integer.MIN_VALUE;
        }
        if (e2.isJsonNull()) {
            return Integer.MAX_VALUE;
        }
        if (e1.isJsonArray() || e2.isJsonArray()) {
            return deltaOf(jsonArrayOf(e1), jsonArrayOf(e2));
        }
        if (e1.isJsonPrimitive() && e2.isJsonPrimitive()) {
            final JsonPrimitive p1 = e1.getAsJsonPrimitive();
            final JsonPrimitive p2 = e2.getAsJsonPrimitive();
            if (p1.isNumber() && p2.isNumber()) {
                final BigDecimal d1 = p1.getAsBigDecimal();
                final BigDecimal d2 = p2.getAsBigDecimal();
                return d1.compareTo(d2);
            }
        }
        return e1.toString().compareTo(e2.toString());
    }

    /**
     * Compare two classes by their canonical names.
     *
     * @param type1 operand;
     * @param type2 operand;
     * @return the lexicographic difference of the canonical names of {@code type1} and {@code type2}.
     * @see Class#getCanonicalName()
     */
    protected static int deltaOf(
            @NotNull final Class type1,
            @NotNull final Class type2) {
        return type1.getCanonicalName().compareTo(type2.getCanonicalName());
    }

    /**
     * Return the {@code json} as element of {@link JsonArray}.
     *
     * @param json to wrap in a {@link JsonArray}
     * @return {@code json}  it it is an array, else create a {@link JsonArray},
     * add {@code json} element in the brand new array and return it.
     */
    @NotNull
    protected static JsonArray jsonArrayOf(
            @NotNull final JsonElement json) {
        if (!json.isJsonArray()) {
            final JsonArray array = new JsonArray();
            array.add(json);
            return array;
        }
        return json.getAsJsonArray();
    }

    /**
     * Return a new element for the value expressed by {@code json} of {@code type}.
     *
     * @param type value's type.
     * @param json value represented in JSON.
     * @param <T>  value's type.
     * @return a new element for the value expressed by {@code json} of {@code type}.
     */
    @NotNull
    public static <T> Builder<T> from(
            @NotNull final Class<T> type,
            @NotNull final JsonElement json) {
        return new Builder(type, json);
    }

    /**
     * {@inheritDoc}
     *
     * @param that element.
     * @return what {@link Comparator} returns for this and {@code that} element.
     */
    @Override
    public int compareTo(
            @NotNull final Element that) {
        return new Comparator().compare(this, that);
    }


    /**
     * Return {@code true} is {@code this} and {@code that} elements are of the same type and have the same values.
     *
     * @param that element.
     * @return {@code true} is {@code this} and {@code that} elements are of the same type and have the same values
     * expressed by their {@link #json} representation.
     * All others attributes are not considered to test if {@code this} and {@code that} are equals.
     */
    @Override
    public boolean equals(Object that) {
        if (this == that) return true;
        if (!(that instanceof Element)) return false;
        Element<?> Element = (Element<?>) that;
        return type.equals(Element.type) &&
                json.equals(Element.json);
    }

    /**
     * Return the {@link #json} as element of {@link JsonArray}.
     *
     * @return {@link #json}  it it is an array, else create a {@link JsonArray},
     * add {@code json} element in the brand new array and return it.
     */
    public JsonArray getJsonArray() {
        return jsonArrayOf(json);
    }

    /**
     * Return the size of this element if this element is an array,
     * else 1 if this element is scalar,
     * else 0 if this element is null.
     *
     * @return the size of this element if this element is an array,
     * else 1 if this element is scalar,
     * else 0 if this element is null.
     */
    public int getSize() {
        if (isArray()) {
            return json.getAsJsonArray().size();
        }
        if (isNull()) {
            return 0;
        }
        return 1;
    }

    /**
     * {@inheritDoc}
     *
     * @return the hash code for {@link #type} combined with {@link #json}, all other attribute ignored.
     */
    @Override
    public int hashCode() {
        return Objects.hash(type, json);
    }

    /**
     * Return {@code true} if this element expresses a boolean datum.
     *
     * @return {@code true} if this element expresses a boolean datum.
     */
    public boolean isBoolean() {
        if (json.isJsonPrimitive()) {
            return json.getAsJsonPrimitive().isBoolean();
        }
        return false;
    }

    /**
     * Return {@code true} if this element expresses a conditional expression.
     *
     * @return {@code true} if this element expresses a conditional expression.
     */
    public boolean isConditional() {
        return condition != null;
    }

    /**
     * Return {@code true} if this element expresses rules for the integrity of the datum.
     *
     * @return {@code true} if this element expresses rules for the integrity of the datum.
     */
    public boolean isConstraint() {
        if (constraints != null) {
            return !constraints.isEmpty();
        }
        return false;
    }

    /**
     * Return {@code true} if this element represents an array.
     *
     * @return {@code true} if this element represents an array.
     */
    public boolean isArray() {
        return json.isJsonArray();
    }

    /**
     * Return {@code true} if this element is null/void.
     *
     * @return {@code true} if this element is null/void.
     */
    public boolean isNull() {
        return json.isJsonNull();
    }

    /**
     * Return {@code true} if this element is numeric.
     *
     * @return {@code true} if this element is numeric.
     */
    public boolean isNumber() {
        if (json.isJsonPrimitive()) {
            return json.getAsJsonPrimitive().isNumber();
        }
        return false;
    }

    /**
     * Return {@code true} if this element is an object, that means not array, not scalar datum.
     *
     * @return {@code true} if this element is an object, that means not array, not scalar datum.
     */
    public boolean isObject() {
        return json.isJsonObject();
    }

    /**
     * Return {@code true} if this element is a text datum.
     *
     * @return {@code true} if this element is a text datum.
     */
    public boolean isText() {
        if (json.isJsonPrimitive()) {
            return json.getAsJsonPrimitive().isString();
        }
        return false;
    }

    /**
     * Return {@code true} is element of {@code type} type.
     *
     * @param type to check if this element is of the same type.
     * @return {@code true} is element of {@code type} type.
     */
    public boolean isType(
            @NotNull final Class type) {
        return type.isAssignableFrom(this.type);
    }

    /**
     * Return the {@link JsonObject} representation of this element.
     * todo: all attributes?
     *
     * @return the {@link JsonObject} representation of this element.
     */

    @NotNull
    public JsonObject toJSON() {
        final JsonObject json = new JsonObject();
        json.addProperty(TAG_TYPE, type.getCanonicalName());
        json.add(TAG_JSON, this.json);
        return json;
    }

    /**
     * {@inheritDoc}
     *
     * @return {@inheritDoc}
     */
    @NotNull
    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .omitNullValues()
                .add("type", type)
                .add("json", json)
                .toString();
    }

    /**
     * Mutable constructor for new elements.
     *
     * @param <T> Element's type.
     */
    public static class Builder<T> {

        /**
         * The type of this element.
         */
        @NotNull
        protected final Class<T> type;

        /**
         * The JSON representation of this element.
         */
        @NotNull
        protected final JsonElement json;

        /**
         * Conditions expressed about this element as parameter of the condition.
         */
        @Nullable
        protected volatile Condition condition;


        /**
         * Rules used to limit the type of data that can this element can be,
         * to maintain the accuracy and integrity of the data inside the {@link ZSet#fqn}-space.
         */
        @NotNull
        protected final EnumSet<Constraint> constraints = EnumSet.noneOf(Constraint.class);


        /**
         * Return a new mutable constructor for this element of...
         *
         * @param type element's type;
         * @param json element's value representation in JSON.
         */
        public Builder(
                @NotNull final Class<T> type,
                @NotNull final JsonElement json) {
            this.type = type;
            this.json = json;
        }

        /**
         * Return a new immutable {@link Element} instance built from this.
         *
         * @return a new immutable {@link Element} instance built from this.
         */
        @NotNull
        public Element set() {
            return new Element(
                    type,
                    json,
                    condition,
                    constraints.isEmpty() ? null : constraints
            );
        }

        /**
         * Make the element conditional.
         *
         * @param condition conditional expression.
         * @return this.
         */
        @NotNull
        public Builder condition(
                @Nullable final Condition condition) {
            this.condition = condition;
            return this;
        }

        /**
         * Add the {@code constraint} to the set of constraints this has to respect to keep datum integrity.
         *
         * @param constraint to add.
         * @return this.
         */
        @NotNull
        public Builder constrain(
                @NotNull final Constraint constraint) {
            constraints.add(constraint);
            return this;
        }

    } //~ Builder

    /**
     * {@inheritDoc}
     * <p></p>
     * Order of comparison.
     * <ol>
     *     <li>{@link #type}</li>
     *     <li>{@link #json}</li>
     * </ol>
     * <p></p>
     * Ignored.
     * <ul>
     *     <li>{@link #constraints}</li>
     * </ul>
     */
    public static class Comparator implements java.util.Comparator<Element> {

        @Override
        public int compare(
                @NotNull final Element p1,
                @NotNull final Element p2) {
            final int deltaType = deltaOf(p1.type, p2.type);
            if (deltaType == 0) {
                return deltaOf(p1.json, p2.json);
            }
            return deltaType;
        }

    } //~ Comparator


}
