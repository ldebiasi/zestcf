package zest.data;

import com.google.gson.*;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.FileReader;
import java.io.IOException;

public class Codec {

    public final static transient String ISO_8601_FORMAT = "yyyy-MM-dd'T'HH:mm:ssX";

    public final static transient Codec DEFAULT = Codec.use(gson().create());

    @NotNull
    public static GsonBuilder gson() {
        return new GsonBuilder()
                .setPrettyPrinting()
                .setDateFormat(ISO_8601_FORMAT)
                .setLongSerializationPolicy(LongSerializationPolicy.DEFAULT)
                .registerTypeHierarchyAdapter(
                        Class.class,
                        new TypeAdapter<Class>() {
                            @Override
                            public void write(JsonWriter out, Class type) throws IOException {
                                out.value(type.toString());
                            }

                            @Override
                            public Class read(JsonReader in) throws IOException {
                                try {
                                    final String canonicalName = in.nextString();
                                    return Class.forName(canonicalName);
                                } catch (ClassNotFoundException e) {
                                    throw new IOException(e);
                                }
                            }
                        })

                .disableHtmlEscaping();
    }

    public final Gson gson;

    protected Codec(
            Gson gson) {
        this.gson = gson;
    }

    public static Codec use(
            @NotNull final Gson gson) {
        return new Codec(gson);
    }


    /**
     * Decode the {@code element} in the object represented by the {@link Element#type}.
     *
     * @param element to decode in the object it represents.
     * @param <T>     type of object the {@code element} represents.
     * @return the object represented by {@code element}.
     */
    @NotNull
    public <T> T decode(
            @NotNull final Element<T> element) {
        return gson.fromJson(element.json, element.type);
    }

    /**
     * Decode the {@code element} in the object represented by the {@link Element#type}.
     *
     * @param element to decode in the object it represents.
     * @param type    has to be equal to
     * @param <T>     type of object the {@code element} represents.
     * @return the object represented by {@code element}.
     * @throws ClassCastException if {@code type} isn't a class of super class of type {@link Element#type}.
     */
    @NotNull
    public <T> T decode(
            @NotNull final Element<T> element,
            @NotNull final Class<T> type)
            throws ClassCastException {
        if (type.isAssignableFrom(element.type)) {
            return gson.fromJson(element.json, element.type);
        } else {
            throw new ClassCastException(String.format("%s isn't of %s type", type.getCanonicalName(), element.type));
        }
    }


    /**
     * Decode the {@code json} text supposing it describes a {@link ZSet} instance.
     *
     * @param fileReader for the {@link ZSet} instance.
     * @return the {@code json} text supposing it describes a {@link ZSet} instance.
     */
    @NotNull
    public ZSet decode(
            @NotNull final FileReader fileReader) {
        return gson.fromJson(fileReader, ZSet.class);
    }

    /**
     * Decode the {@code json} text supposing it describes a {@link ZSet} instance.
     *
     * @param json representing a {@link ZSet} instance.
     * @return the {@code json} text supposing it describes a {@link ZSet} instance.
     */
    @NotNull
    public ZSet decode(
            @NotNull final String json) {
        return gson.fromJson(json, ZSet.class);
    }

    @NotNull
    public Element.Builder encode(final double val) {
        return Element.from(Double.class, new JsonPrimitive(val));
    }

    @NotNull
    public Element.Builder encode(final boolean val) {
        return Element.from(Boolean.class, new JsonPrimitive(val));
    }

    @NotNull
    public Element.Builder encode(final char val) {
        return Element.from(Character.class, new JsonPrimitive(val));
    }

    @NotNull
    public Element.Builder encode(final float val) {
        return Element.from(Float.class, new JsonPrimitive(val));
    }

    @NotNull
    public Element.Builder encode(final int val) {
        return Element.from(Integer.class, new JsonPrimitive(val));
    }

    @NotNull
    public Element.Builder encode(final long val) {
        return Element.from(Long.class, new JsonPrimitive(val));
    }

    @NotNull
    public Element.Builder encode(
            @Nullable final Object obj) {
        if (obj != null) {
            final Class type = obj.getClass();
            final JsonElement json = gson.toJsonTree(obj, type);
            return Element.from(type, json);
        }
        return Element.from(JsonNull.class, JsonNull.INSTANCE);
    }


}
